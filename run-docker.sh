#!/bin/sh

docker-machine stop default
docker-machine start default
docker-machine env default
eval "$(docker-machine env default)"
docker run -d -p 8888:80 -v $(pwd):/var/www/html laravel-env
