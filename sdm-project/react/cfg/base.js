var path = require('path');

var port = 8000;
var srcPath = path.join(__dirname, '/../src');
var publicPath = '/assets/';

module.exports = {
  port: port,
  debug: true,
  output: {
		path: path.join(__dirname, '/../../assets'),
    filename: 'app.js',
    publicPath: publicPath
  },
	//devServer: {
	//contentBase: './src/',
	//historyApiFallback: true,
	//hot: true,
	//port: port,
		//publicPath: publicPath,
		//noInfo: false,
	//proxy: {
	//'*': 'http://192.168.99.100:8888/sdm-project/'
	//}
	//},
  resolve: {
    extensions: ['', '.js', '.jsx', '.css', '.scss', '.styl'],
    alias: {
      actions: srcPath + '/actions/',
      components: srcPath + '/components/',
      sources: srcPath + '/sources/',
      stores: srcPath + '/stores/',
      styles: srcPath + '/styles/',
			config: srcPath + '/config/' + process.env.REACT_WEBPACK_ENV,
			constants: srcPath + '/constants',
			utils: srcPath + '/utils'
    }
  },
  module: {
		//noParse: /node_modules\/quill/,
    preLoaders: [
      {
        test: /\.(js|jsx)$/,
        include: path.join(__dirname, 'src'),
        loader: 'eslint-loader'
      }
    ],
    loaders: [
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      },
      {
        test: /\.sass/,
        loader: 'style-loader!css-loader!sass-loader?outputStyle=expanded&indentedSyntax'
      },
      {
        test: /\.scss/,
        loader: 'style-loader!css-loader!sass-loader?outputStyle=expanded'
      },
      {
        test: /\.less/,
        loader: 'style-loader!css-loader!less-loader'
      },
      {
        test: /\.styl/,
        loader: 'style-loader!css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!stylus-loader'
      },
      {
        test: /\.(png|jpg|gif|woff|woff2)$/,
        loader: 'url-loader?limit=8192'
      }
    ]
  }
};
