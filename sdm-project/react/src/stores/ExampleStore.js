// import dependencies
import BaseStore from './BaseStore.js';
import ActionType from '../constants/ActionType.js';
import {fromJS} from 'immutable';

// default state
let state = fromJS({
	foo: 0
});

// define store
class ExampleStore extends BaseStore {
	// constructor
	constructor() {
		super();
		// register to Dispatcher
		this.register(this.onDispatched);
	}
	// getter
	getState() {
		return state;
	}
	// add callback to event change
	addChangeListener(callback) {
		this.on(ActionType.CHANGE, callback);
	}
	// remove callback to event change
	removeChangeListener(callback) {
		this.removeListener(ActionType.CHANGE, callback);
	}
	// trigger when action is dispatched
	onDispatched = (action) => {
		const {actionType, payload} = action;
		// determine what to do when an action is dispatched
		switch (actionType) {
			case ActionType.EXAMPLE_ADD:
				// set state
				_add(payload);
				// notify registered components
				this.emitChange();
				break;
		}
	}
}

// ***** Setter *****
function _add (data) {
	state = state.update('foo', v => v + data);
}
// ***** Setter *****

// export store
export default new ExampleStore();
