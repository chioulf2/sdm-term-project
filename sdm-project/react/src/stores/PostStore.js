// import dependencies
import BaseStore from './BaseStore.js';
import ActionType from '../constants/ActionType.js';
import {fromJS, List} from 'immutable';
import moment from 'moment';

// default state
let state = null;

// define store
class PostStore extends BaseStore {
	// constructor
	constructor() {
		super();
		// register to Dispatcher
		this.register(this.onDispatched);
	}
	// getter
	getState() {
		return state;
	}
	// add callback to event change
	addChangeListener(callback) {
		this.on(ActionType.CHANGE, callback);
	}
	// remove callback to event change
	removeChangeListener(callback) {
		this.removeListener(ActionType.CHANGE, callback);
	}
	// trigger when action is dispatched
	onDispatched = (action) => {
		const {actionType, payload} = action;
		// determine what to do when an action is dispatched
		switch (actionType) {
			case ActionType.POST_GET_DATA:
				_setData(payload);
				this.emitChange();
				break;
			case ActionType.POST_ADD_DATA:
				_addData(payload);
				this.emitChange();
				break;
			case ActionType.REPLY_ADD_DATA:
				_reply(payload);
				this.emitChange();
				break;
			case ActionType.REPLY_OF_REPLY_ADD_DATA:
				_replyReply(payload);
				this.emitChange();
				break;
			case ActionType.POST_DELETE_DATA:
				_deleteData(payload);
				this.emitChange();
				break;
			case ActionType.POST_EDIT_DATA:
				_editData(payload);
				this.emitChange();
				break;
		}
	}
}

// ***** Setter *****
function _setData (data) {
	data.sort((a, b) => moment(a.created_at).isAfter(b.created_at, 'second'));
	data.reverse();
	state = fromJS(data);
}
function _addData (data) {
	state = state.unshift(fromJS(data));
}
function _reply (data) {
	let postIndex = state.findIndex((post) => post.get('key') === data.parentkey);
	if (postIndex !== -1) {
		state = state.update(postIndex, (post) => post.set('replies', post.get('replies').push(fromJS(data))))
	}
}
function _replyReply (data) {
	let postIndex = state.findIndex(post => {
		let replyIndex = post.get('replies').findIndex(reply => reply.get('key') === data.parentkey);
		return replyIndex !== undefined && replyIndex !== -1;
	});
	if (postIndex !== undefined && postIndex !== -1) {
		state = state.update(postIndex, (post) => {
			let replyIndex = post.get('replies').findIndex(reply => reply.get('key') === data.parentkey);
			if (replyIndex !== undefined && replyIndex !== -1) {
				let replies = post.get('replies').update(replyIndex, (reply) => reply.set('replies', reply.get('replies').push(fromJS(data))));
				return post.set('replies', replies);
			} else {
				return post;
			}
		})
	}
}
function _deleteData (data) {
	let index = state.findIndex(post => post.get('key') === data);
	if (index !== -1) {
		state = state.delete(index);
	}
}
function _editData (data) {
	let index = state.findIndex(post => post.get('key') === data.key);
	if (index !== -1) {
		state = state.set(index, fromJS(data));
	}
}
// ***** Setter *****

// export store
export default new PostStore();
