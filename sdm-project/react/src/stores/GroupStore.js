// import dependencies
import BaseStore from './BaseStore.js';
import ActionType from '../constants/ActionType.js';
import {fromJS} from 'immutable';

// default state
let state = fromJS([]);

// define store
class GroupStore extends BaseStore {
	// constructor
	constructor() {
		super();
		// register to Dispatcher
		this.register(this.onDispatched);
	}
	// getter
	getState() {
		return state;
	}
	// add callback to event change
	addChangeListener(callback) {
		this.on(ActionType.CHANGE, callback);
	}
	// remove callback to event change
	removeChangeListener(callback) {
		this.removeListener(ActionType.CHANGE, callback);
	}
	// trigger when action is dispatched
	onDispatched = (action) => {
		const {actionType, payload} = action;
		// determine what to do when an action is dispatched
		switch (actionType) {
			case ActionType.GROUP_GET_DATA:
				// set state
				_setData(payload);
				// notify registered components
				this.emitChange();
				break;
			case ActionType.GROUP_ADD_DATA:
				_addData(payload);
				this.emitChange();
				break;
		}
	}
}

// ***** Setter *****
function _setData (data) {
	state = fromJS(data);
}
function _addData (data) {
	state = state.push(fromJS(data));
}
// ***** Setter *****

// export store
export default new GroupStore();
