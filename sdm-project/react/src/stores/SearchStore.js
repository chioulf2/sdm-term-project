// import dependencies
import BaseStore from './BaseStore.js';
import ActionType from '../constants/ActionType.js';
import {fromJS} from 'immutable';

// default state
let state = fromJS({
	postTag: [],
	userName: [],
	groupName: [],
	post: [],
	tagPrefix: '',
	userPrefix: '',
	groupPrefix: ''
});

// define store
class SearchStore extends BaseStore {
	// constructor
	constructor() {
		super();
		// register to Dispatcher
		this.register(this.onDispatched);
	}
	// getter
	getState() {
		return state;
	}
	// add callback to event change
	addChangeListener(callback) {
		this.on(ActionType.CHANGE, callback);
	}
	// remove callback to event change
	removeChangeListener(callback) {
		this.removeListener(ActionType.CHANGE, callback);
	}
	// trigger when action is dispatched
	onDispatched = (action) => {
		const {actionType, payload} = action;
		// determine what to do when an action is dispatched
		switch (actionType) {
			case ActionType.SEARCH_POST_TAG_COMPLETE:
				_setPostTagComplete(payload);
				this.emitChange();
				break;
			case ActionType.SEARCH_USER_NAME_COMPLETE:
				_setUserNameComplete(payload);
				this.emitChange();
				break;
			case ActionType.SEARCH_GROUP_NAME_COMPLETE:
				_setGroupNameComplete(payload);
				this.emitChange();
				break;
			case ActionType.SEARCH_POST:
				_setPostSearch(payload);
				this.emitChange();
				break;
			case ActionType.SEARCH_CHANGE_FILTER:
				_changeFilter(payload);
				this.emitChange();
				break;
			case ActionType.SEARCH_SET_PREFIX:
				_setPrefix(payload);
				this.emitChange();
				break;
		}
	}
}

// ***** Setter *****
function _setPostTagComplete(data){
	state = state.set('postTag', fromJS(data));
}
function _setUserNameComplete(data) {
	state = state.set('userName', fromJS(data));
}
function _setGroupNameComplete(data) {
	state = state.set('groupName', fromJS(data));
}
function _setPostSearch(data) {
	state = state.set('post', fromJS(data));
}
function _changeFilter(data) {
	state = state.set('filter', data);
}
function _setPrefix(data) {
	let {filter, value} = data;
	state = state.set(filter + 'Prefix', value);
}
// ***** Setter *****

// export store
export default new SearchStore();
