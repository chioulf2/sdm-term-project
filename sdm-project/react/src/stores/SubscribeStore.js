// import dependencies
import BaseStore from './BaseStore.js';
import ActionType from '../constants/ActionType.js';
import {fromJS} from 'immutable';

// default state
let state = fromJS([]);

// define store
class SubscribeStore extends BaseStore {
	// constructor
	constructor() {
		super();
		// register to Dispatcher
		this.register(this.onDispatched);
	}
	// getter
	getState() {
		return state;
	}
	// add callback to event change
	addChangeListener(callback) {
		this.on(ActionType.CHANGE, callback);
	}
	// remove callback to event change
	removeChangeListener(callback) {
		this.removeListener(ActionType.CHANGE, callback);
	}
	// trigger when action is dispatched
	onDispatched = (action) => {
		const {actionType, payload} = action;
		// determine what to do when an action is dispatched
		switch (actionType) {
			case ActionType.SUBSCRIBE_GET_DATA:
				_getData(payload);
				this.emitChange();
				break;
			case ActionType.SUBSCRIBE_ADD_DATA:
				// set state
				_addData(payload);
				// notify registered components
				this.emitChange();
				break;
			case ActionType.SUBSCRIBE_REMOVE_DATA:
				_removeData(payload);
				this.emitChange();
				break;
		}
	}
}

// ***** Setter *****
function _getData(data) {
	state = fromJS(data);
}
function _addData (data) {
	state = state.push(fromJS(data));
}
function _removeData (data) {
	let index = state.findIndex(s => s.get('classification_id') === data);
	if (index !== -1) {
		state = state.delete(index);
	}
}
// ***** Setter *****

// export store
export default new SubscribeStore();
