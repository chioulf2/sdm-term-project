// import dependencies
import BaseStore from './BaseStore.js';
import ActionType from '../constants/ActionType.js';
import {fromJS, Record} from 'immutable';

const newPostData = Record({
	body: '',
	classification_id: '0'
});

const newReplyData = Record({
	body: ''
});

const newReplyOfReplyData = Record({
	body: ''
});

const newGroupData = Record({
	name: '',
	'private': '0',
	description: ''
});

const newTagData = Record({
	user: '',
	post: ''
});

// default state
let state = fromJS({
	isSideMenuOpen: false,
	isMenuOpen: false,
	isGroupNavOpen: false,
	isPostCreateDialogOpen: false,
	isGroupCreateDialogOpen: false,
	isMemberAddDialogOpen: false,
	isPostEditDialogOpen: false,
	isPostSearchResultOpen: false,
	isPostSearchDialogOpen: false,
	isReplyOfReplyDialogOpen: false,
	currentPostClassification: '0',
	currentGroupkey: '0000000000000000',
	newPostData: new newPostData(),
	newReplyData: new newReplyData(),
	newReplyOfReplyData: new newReplyOfReplyData(),
	newGroupData: new newGroupData(),
	newTagData: new newTagData(),
	newMemberData: []
});

// define store
class AppStore extends BaseStore {
	// constructor
	constructor() {
		super();
		// register to Dispatcher
		this.register(this.onDispatched);
	}
	// getter
	getState() {
		return state;
	}
	// add callback to event change
	addChangeListener(callback) {
		this.on(ActionType.CHANGE, callback);
	}
	// remove callback to event change
	removeChangeListener(callback) {
		this.removeListener(ActionType.CHANGE, callback);
	}
	// trigger when action is dispatched
	onDispatched = (action) => {
		const {actionType, payload} = action;
		// determine what to do when an action is dispatched
		switch (actionType) {
			case ActionType.APP_SET_STATE:
				// set state
				_setState(payload);
				// notify registered components
				this.emitChange();
				break;
			case ActionType.APP_SET_NEW_POST_DATA:
				_setNewPostData(payload);
				this.emitChange();
				break;
			case ActionType.APP_RESET_NEW_POST_DATA:
				_resetNewPostData();
				this.emitChange();
				break;
			case ActionType.APP_SET_NEW_REPLY_DATA:
				_setNewReplyData(payload);
				this.emitChange();
				break;
			case ActionType.APP_SET_NEW_REPLY_OF_REPLY_DATA:
				_setNewReplyOfReplyData(payload);
				this.emitChange();
				break;
			case ActionType.APP_SET_NEW_GROUP_DATA:
				_setNewGroupData(payload);
				this.emitChange();
				break;
			case ActionType.APP_RESET_NEW_GROUP_DATA:
				_resetNewGroupData();
				this.emitChange();
				break;
			case ActionType.APP_SET_NEW_TAG_DATA:
				_setNewTagData(payload);
				this.emitChange();
				break;
			case ActionType.APP_RESET_NEW_TAG_DATA:
				_resetNewTagData();
				this.emitChange();
				break;
			case ActionType.APP_SET_NEW_MEMBER_DATA:
				_setNewMemberData(payload);
				this.emitChange();
				break;
			case ActionType.APP_RESET_NEW_MEMBER_DATA:
				_resetNewMemberData();
				this.emitChange();
				break;
		}
	}
}

// ***** Setter *****
function _setState (data) {
	let {key, value} = data;
	state = state.set(key, value);
}
function _setNewPostData (data) {
	let {key, value} = data;
	state = state.setIn(['newPostData', key], value);
}
function _resetNewPostData () {
	state = state.set('newPostData', new newPostData());
}
function _setNewReplyData (data) {
	let {key, value} = data;
	state = state.setIn(['newReplyData', key], value);
}
function _setNewReplyOfReplyData (data) {
	let {key, value} = data;
	state = state.setIn(['newReplyOfReplyData', key], value);
}
function _setNewGroupData (data) {
	let {key, value} = data;
	state = state.setIn(['newGroupData', key], value);
}
function _resetNewGroupData () {
	state = state.set('newGroupData', new newGroupData());
}
function _setNewTagData (data) {
	let {key, value} = data;
	state = state.setIn(['newTagData', key], value);
}
function _resetNewTagData () {
	state = state.set('newTagData', new newTagData());
}
function _setNewMemberData (data) {
	state = state.set('newMemberData', data);
}
function _resetNewMemberData () {
	state = state.set('newMemberData', fromJS([]));
}
// ***** Setter *****

// export store
export default new AppStore();
