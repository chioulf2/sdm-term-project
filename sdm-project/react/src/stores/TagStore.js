// import dependencies
import BaseStore from './BaseStore.js';
import ActionType from '../constants/ActionType.js';
import {fromJS, List, Map} from 'immutable';

// default state
let state = fromJS({
	user:[],
	post:[]
});

// define store
class TagStore extends BaseStore {
	// constructor
	constructor() {
		super();
		// register to Dispatcher
		this.register(this.onDispatched);
	}
	// getter
	getState() {
		return state;
	}
	// add callback to event change
	addChangeListener(callback) {
		this.on(ActionType.CHANGE, callback);
	}
	// remove callback to event change
	removeChangeListener(callback) {
		this.removeListener(ActionType.CHANGE, callback);
	}
	// trigger when action is dispatched
	onDispatched = (action) => {
		const {actionType, payload} = action;
		// determine what to do when an action is dispatched
		switch (actionType) {
			case ActionType.TAG_GET_USER:
				// set state
				_getUserData(payload);
				// notify registered components
				this.emitChange();
				break;
			case ActionType.TAG_GET_POST:
				_getPostData(payload);
				this.emitChange();
				break;
			case ActionType.TAG_ADD_USER:
				_addUserData(payload);
				this.emitChange();
				break;
			case ActionType.TAG_ADD_POST:
				_addPostData(payload);
				this.emitChange();
				break;
			case ActionType.TAG_REMOVE_POST:
				_removePostData(payload);
				this.emitChange();
				break;
		}
	}
}

// ***** Setter *****
function _getUserData (data) {
	state = state.set('user', fromJS(data));
}
function _getPostData (data) {
	state = state.set('post', fromJS(data));
}
function _addUserData(data){
	state = state.update('user', (tags) => tags.push(data));
}
function _addPostData(data){
	state = state.update('post', (tags) => tags.push(data));
}
function _removePostData(data){
	state = state.update('post', (tags) => tags.filter(tag => tag !== data));
}
// ***** Setter *****

// export store
export default new TagStore();
