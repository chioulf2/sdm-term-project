// import dependencies
import BaseStore from './BaseStore.js';
import ActionType from '../constants/ActionType.js';
import {fromJS} from 'immutable';

// default state
let state = fromJS({
	key: null,
	landing: null,
	current: {
		key: null,
		educations: [],
		experiences: []
	},
	other: [],
	alumniList: [],
	current_alumni: {
		educations: [],
		experiences: []
	}

});

// define store
class UserStore extends BaseStore {
	// constructor
	constructor() {
		super();
		// register to Dispatcher
		this.register(this.onDispatched);
	}
	// getter
	getState() {
		return state;
	}
	// add callback to event change
	addChangeListener(callback) {
		this.on(ActionType.CHANGE, callback);
	}
	// remove callback to event change
	removeChangeListener(callback) {
		this.removeListener(ActionType.CHANGE, callback);
	}
	// trigger when action is dispatched
	onDispatched = (action) => {
		const {actionType, payload} = action;
		// determine what to do when an action is dispatched
		switch (actionType) {
			case ActionType.USER_SET_CURRENT_KEY:
				// set state
				_setKey(payload);
				// notify registered components
				this.emitChange();
				break;
			case ActionType.USER_SET_CURRENT_LANDING:
				_setLanding(payload);
				this.emitChange();
				break;
			case ActionType.USER_GET_CURRENT_DATA:
				_setCurrentData(payload);
				this.emitChange();
				break;
			case ActionType.USER_UPDATE_CURRENT_DATA:
				_updateCurrentData(payload);
				this.emitChange();
				break;
			case ActionType.USER_GET_OTHER_DATA:
				_setOtherData(payload);
				this.emitChange();
				break;
			case ActionType.USER_GET_EDECATION_DATA:
				_setEducationData(payload);
				this.emitChange();
				break;
			case ActionType.USER_ADD_EDUCATION_DATA:
				_addEducationData(payload);
				this.emitChange();
				break;
			case ActionType.USER_ADD_EXPERIENCE_DATA:
				_addExperienceData(payload);
				this.emitChange();
				break;
			case ActionType.USER_UPLOAD_AVATAR:
				_setAvatar(payload);
				this.emitChange();
				break;
			case ActionType.GET_ALUMNI_DATA:
				_addAlumniData(payload);
				this.emitChange();
				break;
			case ActionType.USER_UPDATE_BIOGRAPHY:
				_updateBiographyData(payload);
				this.emitChange();
				break;
			case ActionType.SEARCH_USER_BY_TAG:
				_updateAlumniList(payload);
				this.emitChange();
				break;
		}
	}
}

// ***** Setter *****
function _setKey (data) {
	state = state.set('key', data);
}
function _setLanding (data) {
	state = state.set('landing', data);
}
function _setCurrentData(data) {
	for (let key in data) {
		state = state.setIn(['current', key], fromJS(data[key]));
	}
}
function _updateCurrentData(data) {
	for (let key in data) {
		state = state.setIn(['current', key], fromJS(data[key]));
	}
}
function _setOtherData(data) {
	state = state.set('other', fromJS(data));
	state = state.set('alumniList', fromJS(data));
}
function _setEducationData(data) {
	state = state.setIn(['current', 'educations'], fromJS(data.educations));
	state = state.setIn(['current', 'experiences'], fromJS(data.experiences));
}
function _addEducationData(data) {
	state = state.updateIn(['current', 'educations'], (educations) => educations.push(fromJS(data)));
}

function _addExperienceData(data) {
	state = state.updateIn(['current', 'experiences'], (experiences) => experiences.push(fromJS(data)));
}

function _setAvatar(data) {
	state = state.setIn(['current', 'avatar'], data);
}

function _addAlumniData(data) {
	state = state.setIn(['current_alumni', 'educations'], fromJS(data.educations));
	state = state.setIn(['current_alumni', 'experiences'], fromJS(data.experiences));
}
function _updateBiographyData(data) {
	state = state.setIn(['current', 'biography'], fromJS(data['biography']));
	console.log(state)
}
function _updateAlumniList(data){
	state = state.set('alumniList', fromJS(data));
}
// ***** Setter *****

// export store
export default new UserStore();
