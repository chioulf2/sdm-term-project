// import dependencies
import AppDispatcher from '../dispatcher/AppDispatcher.js';
import {EventEmitter} from 'events';
import ActionType from '../constants/ActionType.js';

// define store
class BaseStore extends EventEmitter {
	// dispatcher token after registering to Dispatcher
	static dispatcherToken = null
	static user_key = null
	// initialize
	initialize(user_key) {
		this.user_key = user_key;
	}
	// emit change event
	emitChange() {
		this.emit(ActionType.CHANGE);
	}
	// add callback to event change
	addChangeListener(callback) {
		this.on(ActionType.CHANGE, callback);
	}
	// remove callback to event change
	removeChangeListener(callback) {
		this.removeListener(ActionType.CHANGE, callback);
	}
	// register to Dispatcher
	register(callback) {
		this.dispatcherToken = AppDispatcher.register(callback);
	}
}

// export store
export default BaseStore;
