<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>SDM Project</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="csrf-token" content="<?php echo csrf_token() ?>">
		<link rel="icon" type="image/icon" href="/favicon.ico">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

		<script src="https://cdn.polyfill.io/v1/polyfill.min.js?features=Intl.~locale.en,Intl.~locale.zh"></script>
	</head>
	<body>
		<div id="app">Loading...</div>

		<div style='display:none'  id='user_key'><?php echo $user_key; ?></div>
		<div style='display:none'  id='landing'><?php echo $landing; ?></div>

		<script>__REACT_DEVTOOLS_GLOBAL_HOOK__ = parent.__REACT_DEVTOOLS_GLOBAL_HOOK__</script>
		<script>
			var WebFontConfig = {
				google: { families: [ 'Roboto:400,300,500:latin' ] }
			};
			(function() {
				var wf = document.createElement('script');
				wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
					'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
				wf.type = 'text/javascript';
				wf.async = 'true';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(wf, s);
			})();
		</script>
		<script type="text/javascript" src="/assets/app.js"></script>
	</body>
</html>
