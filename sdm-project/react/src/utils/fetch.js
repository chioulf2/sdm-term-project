import superagent from 'superagent';
import assign from 'object-assign';

const PREFIX = '/api';

export default function(url, option) {
	let api = PREFIX + url;
	return new Promise((resolve, reject) => {
		let request;
		if (option && option.method) {
			let method = option.method.toLowerCase();
			if (method === 'post') {
				request = superagent.post(api).send(option.body);
			} else if (method === 'put') {
				request = superagent.post(api).send(assign({_method: 'PUT'}, option.body));
			} else if (method === 'delete') {
				request = superagent.del(api);
			} else {
				request = superagent.get(api);
			}
		} else {
			request = superagent.get(api);
		}
		let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
		request.set('X-CSRF-TOKEN', token).end((err, res) => {
			if (err) {
				reject(err);
			} else {
				resolve(res);
			}
		});
	});
}

function getCookie(name) {
	var match = document.cookie.match(new RegExp(name + '=([^;]+)'));
	if (match) return match[1];
}
