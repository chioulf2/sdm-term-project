(function() {
	'use strict';
	// console
	if (console && console.debug === undefined) {
		console.debug = function() {
			console.log.apply(null, arguments);
		};
	}

	// String
	String.prototype.splashToDot = function() {
		return this.replace(/\//g, '.');
	};
	String.prototype.trimEmail = function() {
		return this.substring(0, this.indexOf('@'));
	};
	String.prototype.firstLetterUpperCase = function() {
		return this.charAt(0).toUpperCase() + this.slice(1);
	};
	String.prototype.getLength = function() {
		var str = encodeURIComponent(this);
		return str.replace(/%[A-F\d]{2}/g, 'U').length;
	};
	String.prototype.paddingLeft = function (paddingValue) {
		return String(paddingValue + this).slice(-paddingValue.length);
	};
	String.prototype.format = function () {
		var args = arguments;
		return this.replace(/{(\d+)}/g, function (match, number) {
			return typeof args[number] != 'undefined' ? args[number] : match;
		});
	};
	String.prototype.toMaxLength = function (max, replace) {
		replace = replace || '...';
		var needReplace = false;
		var split = this.split('');
		var total = 0, result = '';
		for (var i = 0; i < split.length; i += 1) {
			var length = split[i].getLength();
			total += length;
			result += split[i];
			if (total >= max) {
				needReplace = true;
				break;
			}
		}
		if (needReplace) {
			result += replace;
		}
		return result;
	};

	// Number
	Number.prototype.isOdd = function() {
		return this % 2 !== 0;
	};
	Number.prototype.isEven = function() {
		return this % 2 === 0;
	};

	// Array
	Array.prototype.shuffle = function() {
		var o = this;
		for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
		return o;
	};
	Array.prototype.last = function () {
		return this[this.length - 1];
	};
	Array.prototype.first = function () {
		return this[0];
	};
	Array.prototype.find = function(func) {
		var index = -1;
		for (var i = 0; i < this.length; i += 1) {
			if (func.apply(this, [this[i]])) {
				index = i;
				break;
			}
		}
		return index;
	};
	Array.prototype.splitInto = function(n) {
		var list = [],
			temp = this.slice(0);
			if (n < temp.length) {
				var num = Math.ceil(temp.length / n);
				while (temp.length) {
					list.push(temp.splice(0, num));
				}
				return list;
			} else {
				return [temp];
			}
	};
	Array.toArray = function(data) {
		return Array.prototype.slice.call(data);
	};
	if (typeof Array.from === 'undefined') {
		Array.from = function (data) {
			let list = [];
			for (let key in data) {
				list.push(data[key]);
			}
			return list;
			//return [].slice.call(arguments, 0);
		};
	}

	// Date
	Date.prototype.toDateInputValue = (function() {
		var local = new Date(this);
		local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
		return local.toJSON().slice(0, 10);
	});
	Date.prototype.toDateTimeInputValue = (function() {
		var local = new Date(this);
		local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
		var value = local.toJSON().slice(0, 16);
		return value;
	});
	Date.prototype.getTomorrow = function() {
		return new Date(this.getTime() + 24 * 60 * 60 * 1000);
	};
	Date.prototype.getYesterday = function () {
		return new Date(this.getTime() - 24 * 60 * 60 * 1000);
	};
	Date.prototype.toClockHour = function (hour) {
		var year = this.getUTCFullYear();
		var month = this.getMonth() + 1;
		var date = this.getDate();
		var value = new Date('{0}-{1}-{2}T{3}:{4}'.format(year, month.toString().paddingLeft('00'), date.toString().paddingLeft('00'), hour.toString().paddingLeft('00'), '00'));
		value.setHours(hour);
		return value;
	};
	Date.daysBetween = function(date1, date2) {
		//Get 1 day in milliseconds
		var one_day = 1000 * 60 * 60 * 24;
		var one_hour = 1000 * 60 * 60;

		// Convert both dates to milliseconds
		var date1_ms = date1.getTime();
		var date2_ms = date2.getTime();

		var date1_hour = date1.getHours();
		var date2_hour = date2.getHours();

		// adjust hour difference to 8 o'clock
		if (date1_hour > 8) {
			date1_ms = date1_ms - ((date1_hour - 8) * one_hour);
		} else if (date1_hour < 8) {
			date1_ms = date1_ms + ((date1_hour - 8) * one_hour);
		}
		if (date2_hour > 8) {
			date2_ms = date2_ms - ((date2_hour - 8) * one_hour);
		} else if (date2_hour < 8) {
			date2_ms = date2_ms + ((date2_hour - 8) * one_hour);
		}

		// Calculate the difference in milliseconds
		var difference_ms = date2_ms - date1_ms;
		// Convert back to days and return
		var value = Math.round(difference_ms / one_day);
		if (value === -0) {
			value = Math.abs(value);
		}
		return value;
	};
	Date.hoursBetween =  function (date1, date2) {
		var hours = Math.abs(date1 - date2) / 36e5;
		return Math.round(hours);
	};
	Date.isValidDate = function(d) {
		if (Object.prototype.toString.call(d) !== '[object Date]')
			return false;
		return !isNaN(d.getTime());
	};
	Date.isValidDateString = function(s) {
		return ( (new Date(s) !== 'Invalid Date' && !isNaN(new Date(s)) ));
	};
	Date.prototype.toDiffDisplayString = function (date, recent, hours_ago, days_ago, far) {
		var d = date || new Date();
		var diff = Date.daysBetween(this, d);
		if (diff === 0) {
			diff = Date.hoursBetween(this, d);
			if (diff === 0) {
				return recent;
			}
			return diff + hours_ago;
		} else if (diff <= 5) {
			return diff + days_ago;
		} else {
			return d.toLocaleDateString();
		}
	};

	// Function
	Function.prototype.debounce = function(wait, immediate) {
		var timeout, func = this;
		return function() {
			var context = this,
				args = arguments;
				var later = function() {
					timeout = null;
					if (!immediate) func.apply(context, args);
				};
				var callNow = immediate && !timeout;
				clearTimeout(timeout);
				timeout = setTimeout(later, wait);
				if (callNow) func.apply(context, args);
		};
	};

}());
