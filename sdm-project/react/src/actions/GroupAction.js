// import dependencies
import BaseAction from './BaseAction.js';
import ActionType from 'constants/ActionType.js';
import fetch from 'utils/fetch';

// define action
class GroupAction extends BaseAction {
	// constructor
	constructor() {
		super();
	}
	// custom function
	getData() {
		fetch('/group').then(res => {
			this.dispatch(ActionType.GROUP_GET_DATA, res.body);
		});
	}
	addData(data) {
		fetch('/group', {method: 'POST', body: data}).then(res => {
			this.dispatch(ActionType.GROUP_ADD_DATA, res.body);
		});
	}
	addMember(groupkey, member) {
		member.forEach((m, i) => {
			fetch('/group/' + groupkey + '/member/' + m, {method: 'PUT'}).then((res) => {
				this.dispatch(ActionType.GROUP_ADD_MEMBER, res.body);
			});
		});
	}
}

// export action
export default new GroupAction();
