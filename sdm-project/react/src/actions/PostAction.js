// import dependencies
import BaseAction from './BaseAction.js';
import ActionType from '../constants/ActionType.js';
import fetch from '../utils/fetch.js';
import request from 'superagent';

// define action
class PostAction extends BaseAction {
	// constructor
	constructor() {
		super();
	}
	// custom function
	getData() {
		fetch('/post').then((res) => {
			this.dispatch(ActionType.POST_GET_DATA, res.body);
		});
	}
	createData(data) {
		fetch('/post', {method: 'POST', body: data}).then((res) => {
			this.dispatch(ActionType.POST_ADD_DATA, res.body);
		});
	}
	addReply(content , postKey) {
		fetch('/post/' + postKey + '/reply', {method: 'POST', body: {body: content}}).then((res) => {
			this.dispatch(ActionType.REPLY_ADD_DATA, res.body);
		});
	}
	addReplyReply(content, replyKey) {
		fetch('/reply/' + replyKey + '/reply', {method: 'POST', body: {body: content}}).then((res) => {
			this.dispatch(ActionType.REPLY_OF_REPLY_ADD_DATA, res.body);
		});
	}
	deleteData(key) {
		fetch('/post/' + key, {method: 'DELETE'}).then((res) => {
			this.dispatch(ActionType.POST_DELETE_DATA, key);
		});
	}
	editData(key, data) {
		fetch('/post/' + key, {method: 'PUT', body: data}).then((res) => {
			console.log(res);
			this.dispatch(ActionType.POST_EDIT_DATA, res.body);
		});
	}
}

// export action
export default new PostAction();
