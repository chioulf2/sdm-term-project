// import dependencies
import BaseAction from './BaseAction.js';
import ActionType from '../constants/ActionType.js';
import fetch from '../utils/fetch.js';
import UserStore from 'stores/UserStore';

// define action
class UserAction extends BaseAction {
	// constructor
	constructor() {
		super();
	}
	// custom function
	setKey(user_key) {
		this.dispatch(ActionType.USER_SET_CURRENT_KEY, user_key);
	}
	setLanding(landing) {
		this.dispatch(ActionType.USER_SET_CURRENT_LANDING, landing);
	}
	getCurrentData() {
		fetch('/user/' + UserStore.getState().get('key')).then((res) => {
			let data = res.body;
			this.dispatch(ActionType.USER_GET_CURRENT_DATA, data);
		});
	}
	updateCurrentData(data) {
		fetch('/user/' + UserStore.getState().get('key') + '/profile/contact', {
			method: 'PUT',
			body: data
		}).then((res) => {
			this.dispatch(ActionType.USER_UPDATE_CURRENT_DATA, res.body);
		});
	}
	getOtherData() {
		fetch('/user').then((res) => {
			let data = res.body;
			let other = data.filter(d => d.key !== UserStore.getState().get('key'));
			this.dispatch(ActionType.USER_GET_OTHER_DATA, other);
		});
	}
	getEducationData(){
		fetch('/user/' + UserStore.getState().get('key') + '/profile').then((res) => {
			let data = res.body;
			this.dispatch(ActionType.USER_GET_EDECATION_DATA, data);
		});
	}
	addEducationData(data){
		fetch('/user/' + UserStore.getState().get('key') + '/profile/education', {
			method: 'PUT',
			body: data
		}).then((res) => {
			let data = res.body;
			this.dispatch(ActionType.USER_ADD_EDUCATION_DATA, data);
		});
	}
	addExperienceData(data){
		fetch('/user/' + UserStore.getState().get('key') + '/profile/experience', {
			method: 'PUT',
			body: data
		}).then((res) => {
			this.dispatch(ActionType.USER_ADD_EXPERIENCE_DATA, res.body);
		});
	}
	uploadAvatar(file) {
		let formData = new FormData();
		formData.append('image', file);

		fetch('/user/' + UserStore.getState().get('key') + '/avatar', {method: 'POST', body: formData}).then(res => {
			this.dispatch(ActionType.USER_UPLOAD_AVATAR, res.body || res.text);
		});
	}
	getAlumniData(key){
		fetch('/user/' + key + '/profile').then((res) => {
			let data = res.body;
			this.dispatch(ActionType.GET_ALUMNI_DATA, data);
		});
	}
	updateBiographyData(biography){
		fetch('/user/' + UserStore.getState().get('key') + '/biography',
			{method: 'PUT', body: {biography}}).then(res => {
			this.dispatch(ActionType.USER_UPDATE_BIOGRAPHY, res.body);
		});
	}
	searchUser(tag){
		fetch('/user/search?tags='+tag , {method: 'GET'}).then((res) => {
			this.dispatch(ActionType.SEARCH_USER_BY_TAG, res.body);
		});
	}
}

// export action
export default new UserAction();
