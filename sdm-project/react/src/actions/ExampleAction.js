// import dependencies
import BaseAction from './BaseAction.js';
import ActionType from 'constants/ActionType.js';

// define action
class ExampleAction extends BaseAction {
	// constructor
	constructor() {
		super();
	}
	// custom function
	add(num = 1) {
		this.dispatch(ActionType.EXAMPLE_ADD, num);
	}
}

// export action
export default new ExampleAction();
