// import dependencies
import BaseAction from './BaseAction.js';
import ActionType from 'constants/ActionType.js';
import fetch from 'utils/fetch.js';
import UserStore from 'stores/UserStore';

// define action
class TagAction extends BaseAction {
	// constructor
	constructor() {
		super();
	}
	// custom function
	getUserData(key) {
		fetch('/user/' + key + '/tag').then((res) => {
			this.dispatch(ActionType.TAG_GET_USER, res.body);
		});
	}
	getPostData(key) {
		fetch('/post/' + key + '/tag').then((res) => {
			this.dispatch(ActionType.TAG_GET_POST, res.body);
		});
	}
	addUserData(key, tag_name) {
		fetch('/user/' + key + '/tag/' + tag_name, {method: 'PUT'}).then((res) => {
			this.dispatch(ActionType.TAG_ADD_USER, tag_name);
		});
	}
	addPostData(key, tag_name) {
		fetch('/post/' + key + '/tag/' + tag_name, {method: 'PUT'}).then((res) => {
			this.dispatch(ActionType.TAG_ADD_POST, tag_name);
		});
	}
	removePostData(key, tag_name) {
		fetch('/post/' + key + '/tag/' + tag_name, {method: 'DELETE'}).then((res) => {
			this.dispatch(ActionType.TAG_REMOVE_POST, tag_name);
		});
	}
}

// export action
export default new TagAction();
