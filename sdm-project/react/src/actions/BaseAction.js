// import dependencies
import AppDispatcher from '../dispatcher/AppDispatcher.js';

// define action
class Action {
	// dispatch event to stores
	dispatch(actionType, payload) {
		AppDispatcher.dispatch({
			actionType,
			payload
		});
	}
}

// export action
export default Action;
