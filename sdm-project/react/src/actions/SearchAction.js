// import dependencies
import BaseAction from './BaseAction.js';
import ActionType from 'constants/ActionType.js';
import fetch from 'utils/fetch.js';
import SearchStore from 'stores/SearchStore';

// define action
class SearchAction extends BaseAction {
	// constructor
	constructor() {
		super();
	}
	changeFilter(filter) {
		this.dispatch(ActionType.SEARCH_CHANGE_FILTER, filter);
	}
	// custom function
	getPostTagComplete(prefix) {
		fetch('/post/tagcomplete?prefix=' + prefix).then(res => {
			this.dispatch(ActionType.SEARCH_POST_TAG_COMPLETE, res.body);
		});
	}
	getUserNameComplete(prefix) {
		fetch('/user/namecomplete?prefix=' + prefix).then(res => {
			this.dispatch(ActionType.SEARCH_USER_NAME_COMPLETE, res.body);
		});
	}
	getGroupNameComplete(prefix) {
		fetch('/group/namecomplete?prefix=' + prefix).then(res => {
			this.dispatch(ActionType.SEARCH_GROUP_NAME_COMPLETE, res.body);
		});
	}
	searchPostByTag(tag) {
		fetch('/post/search?tags=' + tag).then(res => {
			this.dispatch(ActionType.SEARCH_POST, res.body);
		});
	}
	searchPostByUser(name) {
		let userName = SearchStore.getState().get('userName');
		let user = userName.find(user => user.get('name') === name);
		fetch('/post/search?users=' + user.get('key')).then(res => {
			this.dispatch(ActionType.SEARCH_POST, res.body);
		});
	}
	searchPostByGroup(name) {
		let groupName = SearchStore.getState().get('groupName');
		let group = groupName.find(group => group.get('name') === name);
		fetch('/post/search?groups=' + group.get('key')).then(res => {
			this.dispatch(ActionType.SEARCH_POST, res.body);
		});
	}
	searchPost(tagPrefix, userPrefix, groupPrefix) {
		let query = [];
		if (tagPrefix) {
			query.push('tags='+tagPrefix);
		}
		if (userPrefix) {
			let userName = SearchStore.getState().get('userName');
			let user = userName.find(user => user.get('name') === userPrefix);
			if (user) {
				query.push('users='+user.get('key'));
			}
		}
		if (groupPrefix) {
			let groupName = SearchStore.getState().get('groupName');
			let group = groupName.find(group => group.get('name') === groupPrefix);
			if (group) {
				query.push('groups='+group.get('key'));
			}
		}
		fetch('/post/search?' + query.join('&') ).then(res => {
			this.dispatch(ActionType.SEARCH_POST, res.body);
		});
	}
	setPrefix(filter, value) {
		this.dispatch(ActionType.SEARCH_SET_PREFIX, {filter, value});
	}
}

// export action
export default new SearchAction();
