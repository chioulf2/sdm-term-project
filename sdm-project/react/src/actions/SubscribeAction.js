// import dependencies
import BaseAction from './BaseAction.js';
import ActionType from 'constants/ActionType.js';
import fetch from 'utils/fetch.js';
import UserStore from 'stores/UserStore';

// define action
class SubscribeAction extends BaseAction {
	// constructor
	constructor() {
		super();
	}
	// custom function
	getData() {
		fetch('/user/' + UserStore.getState().get('key') + '/subscribe').then(res => {
			this.dispatch(ActionType.SUBSCRIBE_GET_DATA, res.body);
		});
	}
	addData(id) {
		fetch('/user/' + UserStore.getState().get('key') + '/subscribe', {method: 'PUT', body: {classification_id: id}}).then(res => {
			this.dispatch(ActionType.SUBSCRIBE_ADD_DATA, res.body);
		});
	}
	removeData(id) {
		fetch('/user/' + UserStore.getState().get('key') + '/subscribe/delete', {method: 'PUT', body: {classification_id: id}}).then(res => {
			this.dispatch(ActionType.SUBSCRIBE_REMOVE_DATA, id);
		});
	}
}

// export action
export default new SubscribeAction();
