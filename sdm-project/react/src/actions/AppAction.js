// import dependencies
import BaseAction from './BaseAction.js';
import ActionType from '../constants/ActionType.js';

// define action
class AppAction extends BaseAction {
	// constructor
	constructor() {
		super();
	}
	// custom function
	setState(data = {}) {
		this.dispatch(ActionType.APP_SET_STATE, data);
	}
	setNewPostData(data = {}) {
		this.dispatch(ActionType.APP_SET_NEW_POST_DATA, data);
	}
	resetNewPostData() {
		this.dispatch(ActionType.APP_RESET_NEW_POST_DATA);
	}
	setNewReplyData(data = {}) {
		this.dispatch(ActionType.APP_SET_NEW_REPLY_DATA, data);
	}
	setNewReplyOfReplyData(data = {}) {
		this.dispatch(ActionType.APP_SET_NEW_REPLY_OF_REPLY_DATA, data);
	}
	setNewGroupData(data = {}) {
		this.dispatch(ActionType.APP_SET_NEW_GROUP_DATA, data);
	}
	resetNewGroupData() {
		this.dispatch(ActionType.APP_RESET_NEW_GROUP_DATA);
	}
	setNewTagData(data = {}) {
		this.dispatch(ActionType.APP_SET_NEW_TAG_DATA, data);
	}
	resetNewTagData() {
		this.dispatch(ActionType.APP_RESET_NEW_TAG_DATA);
	}
	setNewMemberData(data = {}) {
		this.dispatch(ActionType.APP_SET_NEW_MEMBER_DATA, data);
	}
	resetNewMemberData() {
		this.dispatch(ActionType.APP_RESET_NEW_MEMBER_DATA);
	}
}

// export action
export default new AppAction();
