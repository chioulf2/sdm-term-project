// require style
// require('styles/ExampleComponent.styl');

// import dependencies
import React, {PropTypes} from 'react';
import BaseComponent from 'components/BaseComponent';
import {FontIcon, SvgIcon} from 'material-ui';

// define component
class Button extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// props type
	static propTypes = {
	}
	// default props
	static defaultProps = {
	}
	// ***** Lifecycle *****
	// ***** Lifecycle *****
	// ***** Custom Function *****
	renderIcon = (type) => {
		switch (type) {
			case 'font':
				return FontIcon;
			case 'svg':
				return SvgIcon;
			default:
				return FontIcon;
		}
	}
	// ***** Custom Function *****
	// define UI
  render() {
		let {type, children, className, ...other} = this.props;

		let TheIcon = this.renderIcon(type);

		return (
			<TheIcon {...other}
				className={`material-icons ${className}`}>
				{children}
			</TheIcon>
		)
	}
}

// export component
export default Button;
