// import dependencies
import React from 'react';
import ReactDOM from 'react-dom';
import {history} from 'react-router';
import injectTapEventPlugin from 'react-tap-event-plugin';
import routes from './routes.js';
import UserStore from 'stores/UserStore';
import UserAction from 'actions/UserAction';
import PostStore from 'stores/PostStore';

// Needed for onTouchTap
// Can go away when react 1.0 release
injectTapEventPlugin();

// require polyfill
require('utils/polyfill');

// require css normalize
require('normalize.css');

// Store initialization
UserStore.initialize();
PostStore.initialize();

// Check login status from server
let user_key = document.getElementById('user_key').textContent;
if (user_key === '') {
	user_key = null;
}
document.getElementById('user_key').remove();
// set user key
UserAction.setKey(user_key);

let landing = document.getElementById('landing').textContent;
document.getElementById('landing').remove();
// set user landing
UserAction.setLanding(landing);

// Render the main component into the dom
ReactDOM.render(routes, document.getElementById('app'));
