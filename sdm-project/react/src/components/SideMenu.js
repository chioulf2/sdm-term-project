// require style
require('styles/ExampleComponent.styl');

// import dependencies
import React, {PropTypes} from 'react';
import BaseComponent from 'components/BaseComponent';
import AppAction from 'actions/AppAction';
import {LeftNav, MenuItem} from 'material-ui';

// define component
class SideMenu extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// default state
	// props type
	static propTypes = {
	}
	// default props
	static defaultProps = {
	}
	// ***** Lifecycle *****
	// ***** Lifecycle *****
	// ***** Custom Function *****
	handleChange = (flag, reason) => {
		AppAction.setState({
			key: 'isSideMenuOpen',
			value: flag
		});
	}
	handleItemClick = (value, e) => {
		let {history} = this.props;
		if (value === 'post') {
			history.pushState(null, '/');
		} else if (value === 'alumni') {
			history.pushState(null, '/alumni');
		} else if (value === 'profile') {
			history.pushState(null, '/profile');
		} else if (value === 'analytics') {
			history.pushState(null, '/analytics');
		}
		// close side menu anyway
		AppAction.setState({
			key: 'isSideMenuOpen',
			value: false
		});
	}
	// ***** Custom Function *****
	// define UI
	render() {
		const {style, className, ...other} = this.props;
		const {appState, intl, location} = this.props;
		const {formatMessage} = intl;

		const pathname = location.pathname.replace('/', '');

		return (
			<LeftNav open={appState.getIn(['appData', 'isSideMenuOpen'])}
				docked={false}
				onRequestChange={this.handleChange}>
				<h2 style={{textAlign: 'center'}}>{formatMessage({id: 'header_title'})}</h2>
				<MenuItem checked={pathname === ''}
					onTouchTap={this.handleItemClick.bind(null, 'post')}>{formatMessage({id: 'post_wall'})}</MenuItem>
				<MenuItem checked={pathname === 'alumni'}
					onTouchTap={this.handleItemClick.bind(null, 'alumni')}>{formatMessage({id: 'alumni_wall'})}</MenuItem>
				<MenuItem checked={pathname === 'profile'}
					onTouchTap={this.handleItemClick.bind(null, 'profile')}>{formatMessage({id: 'profile_page'})}</MenuItem>
				<MenuItem checked={pathname === 'analytics'}
					onTouchTap={this.handleItemClick.bind(null, 'analytics')}>{formatMessage({id: 'analytics'})}</MenuItem>
			</LeftNav>
		);
	}
}

// export component
export default SideMenu;
