// require style
import classStyle from 'styles/PostPage.styl';
import inlineStyle from 'styles/AlumniPage.js';

// import dependencies
import React, {PropTypes} from 'react';
import AppAction from 'actions/AppAction';
import PostAction from 'actions/PostAction';
import moment from 'moment';
import {Avatar, TextField, Table, TableBody,
		TableFooter, TableHeader, TableHeaderColumn, TableRow,
		TableRowColumn,Card ,CardHeader, CardText,
		RefreshIndicator} from 'material-ui';
import BaseComponent from 'components/BaseComponent';
import Header from 'components/Header';
import SideMenu from 'components/SideMenu';
import UserAction from 'actions/UserAction';
import TagAction from 'actions/TagAction';
import Button from 'components/Button';
import Icon from 'components/Icon';



// define component
class AlumniPage extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	state = {
		tag : null
	}
	// props type
	static propTypes = {
	}
	// default props
	static defaultProps = {
	}
	// ***** Lifecycle *****
	componentWillMount() {
		if (this._checkLoginStatus()) {
			let {appState, params} = this.props;
			let {key} = params;
			UserAction.getAlumniData(key);
			TagAction.getUserData(key);
		}
	}
	// ***** Lifecycle *****
	// ***** Custom Function *****

	handleChange = (key,e) => {
		let newData = {};
		newData[key] = e.target.value;
		this.setState(newData);
	}
	handleAddTag = () => {
		let {appState, intl, params} = this.props;
		let {key} = params;
		TagAction.addUserData(key,this.state.tag);
	}
	// ***** Custom Function *****
	// define UI
	render() {
		let {style, className, ...other} = this.props;
		let {appState, intl, params} = this.props;
		let {formatMessage} = intl;
		let {key} = params;
		let user = appState.getIn(['userData','other']).find(user => user.get('key')===key);
		if (!user) {
			return <RefreshIndicator size={40} left={20} top={20} status="loading" />;
		}
		let userData = appState.getIn(['userData','current_alumni']);
		let expData = userData.get('experiences');
		let tagData = appState.getIn(['tagData','user']);
		return (
			<div style={style} className={`Alumni-page ${className}`}>
				<Header {...other} />
				<SideMenu {...other} />
				<div style = {{position:'absolute',top:'50px',right:"50px"}}>
					<span>#</span>
					<TextField
						style = {{width: '120px'}}
						floatingLabelText = {formatMessage({id: 'add_tag'})}
						onChange = {this.handleChange.bind(null, 'tag')}/>
						<Button type='raised' onClick = {this.handleAddTag}>
							{formatMessage({id: 'add'})}
						</Button>
						<br/>
						{
							tagData.map((tag) => <div key={tag}>{tag}</div>)
						}
				</div>
				<div style={{textAlign:'center'}}>
					<Avatar src={user.get('avatar')} size={300} style = {inlineStyle.avatar}/>
					<br/>
					<span style={inlineStyle.userName}>{user.get('name')}</span>
					<Table>
						<TableHeader displaySelectAll = {false}>
							<TableRow>
								<TableHeaderColumn>{formatMessage({id: 'experience_organization'})}</TableHeaderColumn>
								<TableHeaderColumn>{formatMessage({id: 'experience_department'})}</TableHeaderColumn>
								<TableHeaderColumn>{formatMessage({id: 'education_start_date'})}</TableHeaderColumn>
								<TableHeaderColumn>{formatMessage({id: 'education_end_date'})}</TableHeaderColumn>
								<TableHeaderColumn>{formatMessage({id: 'experience_description'})}</TableHeaderColumn>
							</TableRow>
						</TableHeader>
						<TableBody
							displayRowCheckbox = {false}
							showRowHover={true}
							stripedRows={true}>
							{expData.map((exp, i) => (
							<TableRow key={i}>
								<TableRowColumn>{exp.get('organization')}</TableRowColumn>
								<TableRowColumn>{exp.get('department')}</TableRowColumn>
								<TableRowColumn>{exp.get('start_date')}</TableRowColumn>
								<TableRowColumn>{exp.get('end_date')}</TableRowColumn>
								<TableRowColumn>{exp.get('description')}</TableRowColumn>
							</TableRow>
							))}
						</TableBody>
					</Table>
					<Card style = {{width: '400px', maginTop: '30px'}}>
						<CardHeader
							title = {formatMessage({id: 'autobiography'})}
							showExpandableButton={true}/>
						<CardText expandable={true}>
							{user.get('biography')}
						</CardText>
					</Card>
				</div>
			</div>
		);
	}
}

// export component
export default AlumniPage;
