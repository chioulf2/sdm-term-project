// require style
import classStyle from 'styles/CreateGroupDialog.styl';

// import dependencies
import React, {PropTypes} from 'react';
import AppAction from 'actions/AppAction';
import BaseComponent from 'components/BaseComponent';
import Button from 'components/Button';
import PostClassificationSelect from 'components/PostClassificationSelect';
import {Dialog, TextField, Toggle} from 'material-ui';

// define component
class CreateGroupDialog extends BaseComponent {
	constructor(props) {
		super(props);
	}
	handleEditChange = (value) => {
		AppAction.setNewPostData({
			key: 'body',
			value: value
		});
	}
	handleSelectChange = (e, i, value) => {
		AppAction.setNewPostData({
			key: 'classification_id',
			value
		});
	}
	handleToggleChange = (e, toggled) => {
		AppAction.setNewGroupData({
			key: 'private',
			value: toggled ? '1' : '0'
		});
	}
	handleTextChange = (key, e) => {
		AppAction.setNewGroupData({
			key,
			value: e.target.value
		});
	}
	render() {
		let {title, isOpen, onRequestClose, onRequestSubmit, ...other} = this.props;
		let {appState, intl} = this.props;
		let {formatMessage} = intl;

		const actions = [
      <Button
				type='flat'
				text={formatMessage({id: 'cancel'})}
        secondary={true}
        onClick={onRequestClose} />,
      <Button
				type='flat'
				text={formatMessage({id: 'confirm'})}
        primary={true}
        keyboardFocused={true}
        onClick={onRequestSubmit} />
    ];

		return (
			<Dialog
        title={title}
        actions={actions}
        modal={false}
        open={isOpen}
        onRequestClose={onRequestClose}>
				<div className={classStyle['row']}>
					<div className={classStyle['cell']}>
						<TextField
							fullWidth={true}
							value={appState.getIn(['appData', 'newGroupData', 'name'])}
							onChange={this.handleTextChange.bind(null, 'name')}
							floatingLabelText={formatMessage({id: 'group_name'})}
							hintText={formatMessage({id: 'group_name'})} />
					</div>
					<div className={classStyle['cell']}>
						<Toggle
							label={formatMessage({id: 'group_private'})}
							onToggle={this.handleToggleChange} />
					</div>
				</div>
				<TextField
					fullWidth={true}
					value={appState.getIn(['appData', 'newGroupData', 'description'])}
					rows={5}
					rowsMax={5}
					multiLine={true}
					onChange={this.handleTextChange.bind(null, 'description')}
					hintText={formatMessage({id: 'group_description'})} />
      </Dialog>
		);
	}
}
// export component
export default CreateGroupDialog;
