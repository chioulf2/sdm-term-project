// require style
import classStyle from 'styles/AlumniWall.styl';
import inlineStyle from 'styles/AlumniWall.js';

// import dependencies
import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import BaseComponent from 'components/BaseComponent';
import {Avatar, GridList, GridTile, TextField, RaisedButton} from 'material-ui';
import UserAction from 'actions/UserAction';

// define component
class AlumniWall extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// default state
	state = {
		screenWidth: document.body.clientWidth,
		searchTag: null,
		showAll: true
	}
	// props type
	static propTypes = {
	}
	// default props
	static defaultProps = {
	}
	// ***** Lifecycle *****
	// Invoked once, only on the client (not on the server), immediately after the initial rendering occurs.
	componentDidMount() {
		// add listener
		window.addEventListener('resize', this.handleResize);
	}
	// Invoked immediately before a component is unmounted from the DOM.
	componentWillUnmount() {
		// remove listener
		window.removeEventListener('resize', this.handleResize);
	}
	// ***** Lifecycle *****
	// ***** Custom Function *****
	handleResize = (e) => {
		this.setState({
			screenWidth: document.body.clientWidth
		});
    }
    handleChange = (key,e) => {
		if(e.target.value == ''){
			this.setState({
				showAll: true
			});
		}else{
			UserAction.searchUser(e.target.value);
			this.setState({
				showAll: false
			});
		}
		console.log(this.state);

	}
	// ***** Custom Function *****
	// define UI
  render() {
		let {style, className, ...other} = this.props;
		let {appState, intl} = this.props;
		let {formatMessage} = intl;
		let size = 200;
		let others = this.state.showAll ? appState.getIn(['userData', 'other']) : appState.getIn(['userData', 'alumniList']);
		let cols = Math.round(this.state.screenWidth / size);

    return (
      <div style={style} className={`${classStyle['container']} ${className}`}>
				<TextField
					floatingLabelText = {formatMessage({id: 'user_tag'})}
					onChange={this.handleChange.bind(null, 'searchTag')}/>
				<GridList
					padding={10}
					cellHeight={size}
					cols={cols}>
					{
						others.map(other => (
							<Link  key={other.get('key')} to={`/alumni/${other.get('key')}`}>
								<GridTile
									title={other.get('name') || formatMessage({id: 'unknown_user'})}
									subtitle={other.get('type')}
								>
									<img src={other.get('avatar')} />
								</GridTile>
							</Link>
						))
					}
				</GridList>
			</div>
		);
	}
}

// export component
export default AlumniWall;
