// require style
import classStyle from 'styles/ExampleComponent.styl';
import inlineStyle from 'styles/ExampleComponent.js';

// import dependencies
import React, {PropTypes} from 'react';
import AppAction from 'actions/AppAction';
import BaseComponent from 'components/BaseComponent';
import Button from 'components/Button';
import {Dialog, TextField} from 'material-ui';

// define component
class CreateReplyOfReplyDialog extends BaseComponent {
	constructor(props) {
		super(props);
	}
	handleTextChange = (e) => {
		AppAction.setNewReplyOfReplyData({
			key: 'body',
			value: e.target.value
		});
	}
	render() {
		let {title, isOpen, onRequestClose, onRequestSubmit, ...other} = this.props;
		let {appState, intl} = this.props;
		let {formatMessage} = intl;

		const actions = [
      <Button
				type='flat'
				text={formatMessage({id: 'cancel'})}
        secondary={true}
        onClick={onRequestClose} />,
      <Button
				type='flat'
				text={formatMessage({id: 'confirm'})}
        primary={true}
        keyboardFocused={true}
        onClick={onRequestSubmit} />
    ];

		return (
			<Dialog
        title={title}
        actions={actions}
        modal={false}
        open={isOpen}
        onRequestClose={onRequestClose}>
				<TextField
					fullWidth={true}
					value={appState.getIn(['appData', 'newReplyOfReplyData', 'body'])}
					onChange={this.handleTextChange}
					hintText={formatMessage({id: 'reply_of_reply_hint_text'})} />
      </Dialog>
		);
	}
}
// export component
export default CreateReplyOfReplyDialog;
