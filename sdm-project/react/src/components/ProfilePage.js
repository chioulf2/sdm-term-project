// require style
import classStyle from 'styles/ProfilePage.styl';

// import dependencies
import React, {PropTypes} from 'react';
import {findDOMNode} from 'react-dom';
import BaseComponent from 'components/BaseComponent';
import Header from 'components/Header';
import SideMenu from 'components/SideMenu';
import {Avatar, TextField, RaisedButton, Table, TableBody,
		TableFooter, TableHeader, TableHeaderColumn, TableRow,
		TableRowColumn, DatePicker} from 'material-ui';
import {Button} from 'components/Button';
import UserAction from 'actions/UserAction';
import {fromJS} from 'immutable';
import Analytics from 'components/Analytics'




// define component
class ProfilePage extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	state = {
		contact_address: null,
		contact_name: null,
		contact_email: null,
		contact_phone_mobile: null,
		contact_phone_work: null,
		contact_phone_home: null,
		education_degree: null,
		education_institute: null,
		education_department: null,
		education_start_date: null,
		education_end_date: null,
		education_obtained: 1,
		education_concentration: '123',
		experience_organization: null,
		experience_department: null,
		experience_position: null,
		experience_start_date: null,
		experience_end_date: null,
		experience_description: null,
		biography: null
	}
	// props type
	static propTypes = {
	}
	// default props
	static defaultProps = {
	}
	// ***** Lifecycle *****
	componentWillMount() {
		if (this._checkLoginStatus()) {
		}
	}
	// ***** Lifecycle *****
	// ***** Custom Function *****
	addEducation = () => {
		let newData = {};
		newData['education_degree'] = null;
		newData['education_institute'] = null;
		newData['education_department'] = null;
		newData['education_start_date'] = null;
		newData['education_end_date'] = null;
		this.setState(newData);
		var data = {
			degree: this.state.education_degree,
			institute: this.state.education_institute,
			department: this.state.education_department,
			start_date: this.state.education_start_date.toLocaleDateString(),
			end_date: this.state.education_end_date.toLocaleDateString(),
			obtained: 1,
			concentration: '123'
		};
		UserAction.addEducationData(data);
	}
	addExperience = () => {
		this.setState({
			experience_organization: null,
			experience_department: null,
			experience_position: null,
			experience_start_date: null,
			experience_end_date: null,
			experience_description: null
		});
		var data = {
			organization: this.state.experience_organization,
			department: this.state.experience_department,
			position: this.state.experience_position,
			start_date: this.state.experience_start_date.toLocaleDateString(),
			end_date: this.state.experience_end_date.toLocaleDateString(),
			description: this.state.experience_description
		};
		UserAction.addExperienceData(data);
	}
	handleContactChange = (key, e) => {
		let newData = {};
		newData[key] = e.target.value;
		this.setState(newData);
	}
	handleEducationChange = (key, e) => {
		let newData = {};
		newData[key] = e.target.value;
		this.setState(newData);
	}
	handleEducationDateChange = (key, n, date) => {
		let newData = {};
		newData[key] = date;
		this.setState(newData);
	}
	handleExperienceChange = (key, e) => {
		let newData = {};
		newData[key] = e.target.value;
		this.setState(newData);
	}
	handleExperienceDateChange = (key, n, date) => {
		let newData = {};
		newData[key] = date;
		this.setState(newData);
	}
	handleBiographyChange = (key,e) => {
		let newData = {};
		newData[key] = e.target.value;
		this.setState(newData);
	}
	save = () => {
		var data = {
			address: this.state.contact_address,
			name: this.state.contact_name,
			email: this.state.contact_email,
			phone_mobile: this.state.contact_phone_mobile,
			phone_work: this.state.contact_phone_work,
			phone_home: this.state.contact_phone_home
		};
		UserAction.updateCurrentData(data);
	}
	saveBiography = () => {
		UserAction.updateBiographyData(this.state.biography);
	}
		handleUploadAvatar = () => {
			let input = findDOMNode(this.refs['file-input']);
			input.click();
		}
		handleChooseAvatar = (e) => {
			let file = e.target.files[0];
			if (file) {
				UserAction.uploadAvatar(file);
			}
		}
	// ***** Custom Function *****
	// define UI
  render() {
		let {style, className, ...other} = this.props;
		let {formatMessage} = this.props.intl;
		let {appState} = this.props;
		let currentUserData = appState.getIn(['userData', 'current']);
		let {contact_name, contact_address, contact_email,
			contact_phone_home, contact_phone_work, contact_phone_mobile} = this.state
		let {biography} = this.state
		let educationData = currentUserData.get('educations');
		let expData = currentUserData.get('experiences');
		//console.log(currentUserData)
	return (
		<div style={{paddingBottom: '50px'}} className={`profile-page ${className}`}>
				<Header {...other} />
				<SideMenu {...other} />
				<span className={classStyle['avatar-container']}
					onClick={this.handleUploadAvatar}>
					<Avatar src={currentUserData.get('avatar')} size={100}
					style ={{marginLeft: '30px',marginTop: '30px'}}/>
					<input ref='file-input'
						type='file'
						style={{display: 'none'}}
						onChange={this.handleChooseAvatar} />
				</span>
				<TextField
					value={biography !== null ? biography : currentUserData.get('biography')}
					onChange={this.handleBiographyChange.bind(null, 'biography')}
					floatingLabelText = {formatMessage({id: 'autobiography'})}
					style={{
						marginLeft: '30px',
						width: '400px'
					}}/>
				<RaisedButton label={formatMessage({id: 'save'})} onTouchTap={this.saveBiography} />
				<p style = {{fontSize: '30px', marginLeft: '30px'}}> {formatMessage({id: 'contact_tab'})} </p>
				<div>
					<TextField
						value={contact_name !== null ? contact_name : currentUserData.get('name')}
						onChange={this.handleContactChange.bind(null, 'contact_name')}
						floatingLabelText = {formatMessage({id: 'username'})}
						style={{
							marginLeft: '30px',
							width: '80px'
						}}/>

					<TextField
						value={contact_address !== null ? contact_address : currentUserData.get('address')}
						onChange={this.handleContactChange.bind(null, 'contact_address')}
						floatingLabelText = {formatMessage({id: 'address'})}
						style={{
							marginLeft: '50px',
							width: '250px'
						}}/>

					<TextField
						value={contact_email !== null ? contact_email : currentUserData.get('email')}
						onChange={this.handleContactChange.bind(null, 'contact_email')}
						floatingLabelText = {formatMessage({id: 'email'})}
						style={{
							marginLeft: '50px',
							width: '200px'
						}}/>

					<TextField
						value={contact_phone_mobile !== null ? contact_phone_mobile : currentUserData.get('phone_mobile')}
						onChange={this.handleContactChange.bind(null, 'contact_phone_mobile')}
						floatingLabelText = {formatMessage({id: 'phone_mobile'})}
						style={{
							marginLeft: '50px',
							width: '120px'
						}}/>

					<TextField
						value={contact_phone_work !== null ? contact_phone_work : currentUserData.get('phone_work')}
						onChange={this.handleContactChange.bind(null, 'contact_phone_work')}
						floatingLabelText = {formatMessage({id: 'phone_work'})}
						style={{
							marginLeft: '50px',
							width: '120px'
						}}/>

					<TextField
						value={contact_phone_home !== null ? contact_phone_home : currentUserData.get('phone_home')}
						onChange={this.handleContactChange.bind(null, 'contact_phone_home')}
						floatingLabelText = {formatMessage({id: 'phone_home'})}
						style={{
							marginLeft: '50px',
							marginRight: '30px',
							width: '120px'
						}}/>
					<RaisedButton label={formatMessage({id: 'save'})} onTouchTap={this.save} />
				</div>
				<p style = {{fontSize: '30px', marginLeft: '30px'}}> {formatMessage({id: 'education_tab'})} </p>
				<Table>
					<TableHeader displaySelectAll = {false}>
						<TableRow>
							<TableHeaderColumn>{formatMessage({id: 'education_degree'})}</TableHeaderColumn>
							<TableHeaderColumn>{formatMessage({id: 'education_institute'})}</TableHeaderColumn>
							<TableHeaderColumn>{formatMessage({id: 'education_department'})}</TableHeaderColumn>
							<TableHeaderColumn>{formatMessage({id: 'education_start_date'})}</TableHeaderColumn>
							<TableHeaderColumn>{formatMessage({id: 'education_end_date'})}</TableHeaderColumn>
						</TableRow>
					</TableHeader>
					<TableBody
						displayRowCheckbox = {false}
						showRowHover={true}
						stripedRows={true}>
						{educationData.map((edu, i) => (
						<TableRow key={i}>
							<TableRowColumn>{edu.get('degree')}</TableRowColumn>
							<TableRowColumn>{edu.get('institute')}</TableRowColumn>
							<TableRowColumn>{edu.get('department')}</TableRowColumn>
							<TableRowColumn>{edu.get('start_date')}</TableRowColumn>
							<TableRowColumn>{edu.get('end_date')}</TableRowColumn>
						</TableRow>
						))}
					</TableBody>
				</Table>
				<div>
					<TextField
						value = {this.state.education_degree}
						onChange={this.handleEducationChange.bind(null, 'education_degree')}
						floatingLabelText={formatMessage({id: 'education_degree'})}
						style={{
							marginLeft: '30px',
							width: '120px'
						}}/>
					<TextField
						value = {this.state.education_institute}
						onChange={this.handleEducationChange.bind(null, 'education_institute')}
						floatingLabelText={formatMessage({id: 'education_institute'})}
						style={{
							marginLeft: '50px',
							width: '120px'
						}}/>
					<TextField
						value = {this.state.education_department}
						onChange={this.handleEducationChange.bind(null, 'education_department')}
						floatingLabelText={formatMessage({id: 'education_department'})}
						style={{
							marginLeft: '50px',
							width: '120px'
						}}/>
					<DatePicker
						value = {this.state.education_start_date}
						floatingLabelText={formatMessage({id: 'education_start_date'})}
						mode="landscape"
						autoOk={true}
						onChange = {this.handleEducationDateChange.bind(null, 'education_start_date')}
						style = {{
							display: 'inline-block',
							marginLeft: '50px',
							width: '120px'
						}}/>
					<DatePicker
						value = {this.state.education_end_date}
						floatingLabelText={formatMessage({id: 'education_end_date'})}
						mode="landscape"
						autoOk={true}
						onChange = {this.handleEducationDateChange.bind(null, 'education_end_date')}
						style = {{
							display: 'inline-block',
							marginLeft: '50px',
							marginRight: '30px',
							width: '120px'
						}}/>
					<RaisedButton label={formatMessage({id: 'add'})} onTouchTap={this.addEducation} />
				</div>
				<p style = {{fontSize: '30px', marginLeft: '30px'}}> {formatMessage({id: 'experience_tab'})} </p>
				<Table>
					<TableHeader displaySelectAll = {false}>
						<TableRow>
							<TableHeaderColumn>{formatMessage({id: 'experience_organization'})}</TableHeaderColumn>
							<TableHeaderColumn>{formatMessage({id: 'experience_department'})}</TableHeaderColumn>
							<TableHeaderColumn>{formatMessage({id: 'experience_position'})}</TableHeaderColumn>
							<TableHeaderColumn>{formatMessage({id: 'education_start_date'})}</TableHeaderColumn>
							<TableHeaderColumn>{formatMessage({id: 'education_end_date'})}</TableHeaderColumn>
							<TableHeaderColumn>{formatMessage({id: 'experience_description'})}</TableHeaderColumn>
						</TableRow>
					</TableHeader>
					<TableBody
						displayRowCheckbox = {false}
						showRowHover={true}
						stripedRows={true}>
						{expData.map((exp, i) => (
						<TableRow key={i}>
							<TableRowColumn>{exp.get('organization')}</TableRowColumn>
							<TableRowColumn>{exp.get('department')}</TableRowColumn>
							<TableRowColumn>{exp.get('position')}</TableRowColumn>
							<TableRowColumn>{exp.get('start_date')}</TableRowColumn>
							<TableRowColumn>{exp.get('end_date')}</TableRowColumn>
							<TableRowColumn>{exp.get('description')}</TableRowColumn>
						</TableRow>
						))}
					</TableBody>
				</Table>
				<div>
					<TextField
						value ={this.state.experience_organization}
						onChange={this.handleExperienceChange.bind(null, 'experience_organization')}
						floatingLabelText={formatMessage({id: 'experience_organization'})}
						style={{
							marginLeft: '50px',
							width: '120px'
						}}/>
					<TextField
						value ={this.state.experience_department}
						onChange={this.handleExperienceChange.bind(null, 'experience_department')}
						floatingLabelText={formatMessage({id: 'experience_department'})}
						style={{
							marginLeft: '50px',
							width: '120px'
						}}/>
					<TextField
						value ={this.state.experience_position}
						onChange={this.handleExperienceChange.bind(null, 'experience_position')}
						floatingLabelText={formatMessage({id: 'experience_position'})}
						style={{
							marginLeft: '50px',
							width: '120px'
						}}/>
					<DatePicker
						value ={this.state.experience_start_date}
						floatingLabelText={formatMessage({id: 'education_start_date'})}
						mode="landscape"
						autoOk={true}
						onChange = {this.handleExperienceDateChange.bind(null, 'experience_start_date')}
						style = {{
							display: 'inline-block',
							marginLeft: '50px',
							width: '120px'
						}}/>
					<DatePicker
						value ={this.state.experience_end_date}
						floatingLabelText={formatMessage({id: 'education_end_date'})}
						mode="landscape"
						autoOk={true}
						onChange = {this.handleExperienceDateChange.bind(null, 'experience_end_date')}
						style = {{
							display: 'inline-block',
							marginLeft: '50px',
							marginRight: '30px',
							width: '120px'
						}}/>
					<TextField
						value ={this.state.experience_description}
						onChange={this.handleExperienceChange.bind(null, 'experience_description')}
						floatingLabelText={formatMessage({id: 'experience_description'})}
						style={{
							marginLeft: '50px',
							width: '120px'
						}}/>
					<RaisedButton label={formatMessage({id: 'add'})} onTouchTap={this.addExperience} />
				</div>
						</div>
		);
	}
}

// export component
export default ProfilePage;
