// import router
import React from 'react';
import {Router, Route, IndexRoute} from 'react-router'
import createBrowserHistory from 'history/lib/createBrowserHistory';
// import components
import LocaleWrapper from 'components/LocaleWrapper';
// import App from 'components/App';
import MainPage from 'components/MainPage';
import LoginPage from 'components/LoginPage';
import HomePage from 'components/HomePage';
import ProfilePage from 'components/ProfilePage';
import PostPage from 'components/PostPage';
import AlumniPage from 'components/AlumniPage';
import PostWall from 'components/PostWall';
import AlumniWall from 'components/AlumniWall';
import Analytics from 'components/Analytics';

const history = createBrowserHistory();

module.exports = (
	<Router history={history}>
		<Route component={LocaleWrapper}>
			<Route component={MainPage}>
				<Route path='/profile' component={ProfilePage} />
				<Route path='/post/:key' component={PostPage} />
				<Route path='/alumni/:key' component={AlumniPage} />
				<Route path='/analytics' component={Analytics} />
				<Route path='/' component={HomePage}>
					<Route path='/alumni' component={AlumniWall} />
					<IndexRoute component={PostWall} />
				</Route>
			</Route>
			<Route path='/login' component={LoginPage} />
		</Route>
	</Router>
);
