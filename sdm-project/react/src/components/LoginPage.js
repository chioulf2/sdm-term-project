// require style
import classStyle from 'styles/LoginPage.styl';

// import dependencies
import React, {PropTypes} from 'react';
import BaseComponent from 'components/BaseComponent';
import Button from 'components/Button';
import {Tabs, Tab} from 'material-ui';

// define component
class LoginPage extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// props type
	static propTypes = {
	}
	// default props
	static defaultProps = {
	}
	// ***** Lifecycle *****
	// ***** Lifecycle *****
	// ***** Custom Function *****
	handleLogin = () => {
		window.location.href = '/auth/saml2/login';
	}
	handleSwitchLocale = (locale) => {
		this.props.onRequestSwitchLocale(locale);
	}
	// ***** Custom Function *****
	// define UI
  render() {
		let {style, className, ...other} = this.props;
		let {formatMessage, locale} = this.props.intl;

    return (
      <div style={style} className={`${classStyle['container']} ${className}`}>
				<Tabs>
					<Tab label={formatMessage({id: 'login_intro_post'})}>
						<img src={'/images/post_' + locale + '.png'} />
					</Tab>
					<Tab label={formatMessage({id: 'login_intro_group'})}>
						<img src={'/images/group_' + locale + '.png'} />
					</Tab>
					<Tab label={formatMessage({id: 'login_intro_alumni'})}>
						<img src={'/images/alumni_' + locale + '.png'} />
					</Tab>
					<Tab label={formatMessage({id: 'login_intro_locale'})}>
						<img src={'/images/locale_' + locale + '.png'} />
					</Tab>
				</Tabs>
				<div className={classStyle['float']}>
					<Button className={classStyle['login']} onClick={this.handleLogin}>{formatMessage({id: 'login'})}</Button>
					<div className={classStyle['locale']}>
						<span onClick={this.handleSwitchLocale.bind(null, 'en')}>English</span>
						<span onClick={this.handleSwitchLocale.bind(null, 'zh')}>中文</span>
					</div>
				</div>
			</div>
		);
	}
}

// export component
export default LoginPage;
