// require style
import classStyle from 'styles/CreateGroupDialog.styl';

// import dependencies
import React, {PropTypes} from 'react';
import AppAction from 'actions/AppAction';
import BaseComponent from 'components/BaseComponent';
import Button from 'components/Button';
import {Dialog, TextField, Toggle, ListItem, Avatar, Divider} from 'material-ui';
import moment from 'moment';

const PostItem = ({
	style,
	className,
	ownerAvatar,
	ownerName,
	content,
	datetime,
	onClick
}) => (
	<div style={style} className={className}>
		<ListItem
			onTouchTap={onClick}
			leftAvatar={
				<Avatar src={ownerAvatar} />
			}
			primaryText={
				<div className={classStyle['item-primary-text']}>
					<span>{ownerName}</span>
					<span>{datetime}</span>
				</div>
			}
			secondaryText={
				<div dangerouslySetInnerHTML={{__html: content}}></div>
			}
			secondaryTextLines={1}
		/>
		<Divider inset={true} />
	</div>
)

// define component
class PostSearchResult extends BaseComponent {
	constructor(props) {
		super(props);
	}
	navigate = (key) => {
		let {history} = this.props;
		history.pushState(null, '/post/' + key);
	}
	render() {
		let {title, isOpen, onRequestClose, onRequestSubmit, ...other} = this.props;
		let {appState, intl} = this.props;
		let {formatMessage} = intl;

		const actions = [
      <Button
				type='flat'
				text={formatMessage({id: 'cancel'})}
        secondary={true}
        onClick={onRequestClose} />,
      <Button
				type='flat'
				text={formatMessage({id: 'confirm'})}
        primary={true}
        keyboardFocused={true}
        onClick={onRequestSubmit} />
    ];

		return (
			<Dialog
        title={title}
        actions={actions}
        modal={false}
        open={isOpen}
        onRequestClose={onRequestClose}>
				{
					appState.getIn(['searchData', 'post']).map(post => {
						let owner = appState.getIn(['userData', 'other']).find(user => user.get('key') === post.get('ownerkey'));
						if (!owner) {
							owner = appState.getIn(['userData', 'key']) === post.get('ownerkey') ? appState.getIn(['userData', 'current']) : null;
						}
						return (
							<PostItem key={post.get('key')} {...other}
								onClick={this.navigate.bind(null, post.get('key'))}
								ownerAvatar={owner ? owner.get('avatar') : ''}
								ownerName={(owner ? owner.get('name') : '') || formatMessage({id: 'unknown_user'})}
								datetime={moment(post.get('created_at')).format('YYYY/MM/DD HH:mm')}
								content={post.get('body')}
							/>
						);
					})
				}
      </Dialog>
		);
	}
}
// export component
export default PostSearchResult;
