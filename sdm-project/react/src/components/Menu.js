// require style
import classStyle from 'styles/Menu.styl';

// import dependencies
import React, {PropTypes} from 'react';
import AppAction from 'actions/AppAction';
import BaseComponent from 'components/BaseComponent';
import Button from 'components/Button';
import {IconMenu, MenuItem, Avatar} from 'material-ui'

// define component
class Menu extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// default state
	// props type
	static propTypes = {
	}
	// default props
	static defaultProps = {
	}
	// ***** Lifecycle *****
	// ***** Lifecycle *****
	// ***** Custom Function *****
	handleOpenChange = (open, reason) => {
		AppAction.setState({
			key: 'isMenuOpen',
			value: typeof open === 'boolean' ? open : !this.props.appState.getIn(['appData', 'isMenuOpen'])
		});
	}
	handleItemClick = (e, item) => {
		let {history, onRequestOpenLocaleDialog} = this.props;
		const {index} = item.props;

		switch (index) {
			case 0:
				history.pushState(null, '/profile');
				break;
			case 1:
				onRequestOpenLocaleDialog(true);
				break;
			case 2:
				window.location.href = '/auth/saml2/logout';
				break;
		}
		this.handleOpenChange(false);
	}
	// ***** Custom Function *****
	// define UI
  render() {
		let {style, className, ...other} = this.props;
		let {appState, intl} = this.props;
		let {formatMessage} = intl;

    return (
			<IconMenu
				open={appState.getIn(['appData', 'isMenuOpen'])}
				iconButtonElement={
					<Button
						type='icon'
						style={{padding: 0}}
						>
						<Avatar src={appState.getIn(['userData', 'current', 'avatar'])}
							size={50}
							className={classStyle['avatar']}
						/>
					</Button>
				}
				onRequestChange={this.handleOpenChange}
				onItemTouchTap={this.handleItemClick}
				desktop={true}
				>
				<MenuItem index={0} primaryText={formatMessage({id: 'profile_page'})} />
				<MenuItem index={1} primaryText={formatMessage({id: 'locale'})} />
				<MenuItem index={2} primaryText={formatMessage({id: 'logout'})} />
			</IconMenu>
		);
	}
}

// export component
export default Menu;
