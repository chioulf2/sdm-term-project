// require style
require('styles/ExampleComponent.styl');

// import dependencies
import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import BaseComponent from './BaseComponent.js';
import ActionType from 'constants/ActionType';
import ExampleAction from 'actions/ExampleAction';
import ExampleStore from 'stores/ExampleStore';
import {FlatButton, Badge, FontIcon} from 'material-ui';
import {FormattedMessage} from 'react-intl';
import Header from 'components/Header';
import SideMenu from 'components/SideMenu';

// define component
class Analytics extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// default state
	state = {
		foo: 'bar',
		addFoo: ExampleStore.getState()
	}
	// props type
	static propTypes = {
		foo: PropTypes.string
	}
	// default props
	static defaultProps = {
		foo: 'bar'
	}
	// ***** Lifecycle *****
	// Invoked once, both on the client and server, immediately before the initial rendering occurs.
	componentWillMount() {
	}
	// Invoked once, only on the client (not on the server), immediately after the initial rendering occurs.
	componentDidMount() {
		// add listener
		ExampleStore.addChangeListener(this.onStoreChange);
	}
	// Invoked when a component is receiving new props.
	componentWillReceiveProps(nextProps, nextState) {
	}
	// Invoked before rendering when new props or state are being received.
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	// Invoked immediately before rendering when new props or state are being received.
	componentWillUpdate() {
	}
	// Invoked immediately after the component's updates are flushed to the DOM.
	componentDidUpdate() {
	}
	// Invoked immediately before a component is unmounted from the DOM.
	componentWillUnmount() {
		// remove listener
		ExampleStore.removeChangeListener(this.onStoreChange);
	}
	// ***** Lifecycle *****
	// ***** Custom Function *****
	foo = () => {
		alert(this.state.foo);
	}
	onStoreChange = () => {
		// update component state to the lastest store state
		this.setState({
			addFoo: ExampleStore.getState()
		});
	}
	handleClick = () => {
		// dispatch action
		ExampleAction.add(1);
	}
	// ***** Custom Function *****
	// define UI
  render() {
		let {style, className, ...other} = this.props;
		let {formatMessage} = this.props.intl;
		let {appState} = this.props;
		let userData = appState.get('userData');
		let postData = appState.get('postData');
		let groupData = appState.get('groupData');
		let tagData = appState.get('tagData');

		let userNumber = userData.get('other').size + 1;
		let postNumber = postData !== null ? postData.size : 0;
		let replyNumber = 0;
		let replyOfReplyNumber = 0;
		postData !== null ? postData.forEach(post => {
			post.get('replies').forEach(reply => {
				replyNumber ++;
				reply.get('replies').forEach(reply => {
					replyOfReplyNumber ++;
				});
			});
		}) : 0;
		let groupNumber = groupData !== null ? groupData.size : 0;
		let userTagNumber = tagData !== null ? tagData.get('user').size : 0;
		let postTagNumber = tagData !== null ? tagData.get('post').size : 0;

    return (
		<div className={`analytics ${className}`}>
			<Header {...other} />
			<SideMenu {...other} />
			<p>{formatMessage({id: 'analytics'})}</p>
			<Badge badgeContent={userNumber} primary={true}>
				<FontIcon className="material-icons" color='black'>face</FontIcon>
			</Badge>
			<Badge badgeContent={postNumber} primary={true}>
				<FontIcon className="material-icons" color='black'>description</FontIcon>
			</Badge>
			<Badge badgeContent={replyNumber} primary={true}>
				<FontIcon className="material-icons" color='black'>reply</FontIcon>
			</Badge>
			<Badge badgeContent={replyOfReplyNumber} primary={true}>
				<FontIcon className="material-icons" color='black'>reply_all</FontIcon>
			</Badge>
			<Badge badgeContent={groupNumber} primary={true}>
				<FontIcon className="material-icons" color='black'>group</FontIcon>
			</Badge>
			{/*
			<Badge badgeContent={userTagNumber} primary={true}>
				<FontIcon className="material-icons" color='black'>label</FontIcon>
			</Badge>
			<Badge badgeContent={postTagNumber} primary={true}>
				<FontIcon className="material-icons" color='black'>label_outline</FontIcon>
			</Badge>*/}
		</div>
		);
	}
}

// export component
export default Analytics;
