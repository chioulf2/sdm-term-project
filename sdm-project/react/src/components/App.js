// require style
require('styles/App.styl');

// import dependencies
import React from 'react';
import BaseComponent from './BaseComponent.js';
import {fromJS} from 'immutable';
import AppStore from 'stores/AppStore';
import UserStore from 'stores/UserStore';
import PostStore from 'stores/PostStore';
import TagStore from 'stores/TagStore';
import SubscribeStore from 'stores/SubscribeStore';
import GroupStore from 'stores/GroupStore';
import SearchStore from 'stores/SearchStore';


// define component
class App extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
		this.state = {
			appState: fromJS({
				appData: AppStore.getState(),
				userData: UserStore.getState(),
				postData: PostStore.getState(),
				tagData: TagStore.getState(),
				subscribeData: SubscribeStore.getState(),
				groupData: GroupStore.getState(),
				searchData: SearchStore.getState()
			})
		};
	}
	// ***** Lifecycle *****
	componentDidMount() {
		AppStore.addChangeListener(this.updateAppData);
		UserStore.addChangeListener(this.updateUserData);
		PostStore.addChangeListener(this.updatePostData);
		TagStore.addChangeListener(this.updateTagData);
		SubscribeStore.addChangeListener(this.updateSubscribeData);
		GroupStore.addChangeListener(this.updateGroupData);
		SearchStore.addChangeListener(this.updateSearchData);
	}
	componentWillUnmount() {
		AppStore.removeChangeListener(this.updateAppData);
		UserStore.removeChangeListener(this.updateUserData);
		PostStore.removeChangeListener(this.updatePostData);
		TagStore.removeChangeListener(this.updateTagData);
		SubscribeStore.removeChangeListener(this.updateSubscribeData);
		GroupStore.removeChangeListener(this.updateGroupData);
		SearchStore.removeChangeListener(this.updateSearchData);
	}
	// ***** Lifecycle *****
	// ***** Custom Function *****
	updateAppData = () => {
		this.setState({
			appState: this.state.appState.set('appData', AppStore.getState())
		})
	}
	updateUserData = () => {
		this.setState({
			appState: this.state.appState.set('userData', UserStore.getState())
		});
	}
	updatePostData = () => {
		this.setState({
			appState: this.state.appState.set('postData', PostStore.getState())
		});
	}
	updateTagData = () => {
		this.setState({
			appState: this.state.appState.set('tagData', TagStore.getState())
		});
	}
	updateSubscribeData = () => {
		this.setState({
			appState: this.state.appState.set('subscribeData', SubscribeStore.getState())
		});
	}
	updateGroupData = () => {
		this.setState({
			appState: this.state.appState.set('groupData', GroupStore.getState())
		});
	}
	updateSearchData = () => {
		this.setState({
			appState: this.state.appState.set('searchData', SearchStore.getState())
		});
	}
	// ***** Custom Function *****
	// define UI
  render() {
		let {style, children, ...other} = this.props;
		let appState = this.state.appState;
		let {formatMessage} = this.props.intl;

    return (
			<div className={`app ${style}`}>
				{
					React.Children.map(children, child => (
						React.cloneElement(child, {
							...other,
							appState
						})
					))
				}
			</div>
    );
  }
}

// export component
export default App;
