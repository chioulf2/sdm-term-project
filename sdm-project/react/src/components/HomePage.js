// require style
//require('styles/ExampleComponent.styl');

// import dependencies
import React, {PropTypes} from 'react';
import BaseComponent from 'components/BaseComponent';
import UserAction from 'actions/UserAction';
import PostAction from 'actions/PostAction';
import TagAction from 'actions/TagAction';
import Header from 'components/Header';
import SideMenu from 'components/SideMenu';
import PostWall from 'components/PostWall';

// define component
class HomePage extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// props type
	static propTypes = {
	}
	// default props
	static defaultProps = {
	}
	// ***** Lifecycle *****
	componentWillMount() {
		if (this._checkLoginStatus()) {
		}
	}
	// ***** Lifecycle *****
	// ***** Custom Function *****
	// ***** Custom Function *****
	// define UI
  render() {
		let {style, className, children, ...other} = this.props;
		let {formatMessage} = this.props.intl;

    return (
      <div style={style} className={`home-page ${className}`}>
				<Header {...other} />
				<SideMenu {...other} />
				<div>
					{React.cloneElement(children, {...other})}
				</div>
			</div>
		);
	}
}

// export component
export default HomePage;
