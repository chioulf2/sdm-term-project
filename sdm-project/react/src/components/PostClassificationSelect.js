// require style
import classStyle from 'styles/ExampleComponent.styl';
import inlineStyle from 'styles/ExampleComponent.js';

// import dependencies
import React, {PropTypes} from 'react';
import BaseComponent from 'components/BaseComponent';
import {SelectField, MenuItem} from 'material-ui';

// define component
class PostClassificationSelect extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// default state
	state = {
	}
	// props type
	static propTypes = {
	}
	// default props
	static defaultProps = {
	}
	// ***** Lifecycle *****
	// ***** Lifecycle *****
	// ***** Custom Function *****
	// ***** Custom Function *****
	// define UI
  render() {
		let {style, className, value, onChange, floatingLabelText, ...other} = this.props;
		let {formatMessage} = this.props.intl;

    return (
			<SelectField style={style}
				value={value}
				floatingLabelText={floatingLabelText}
				onChange={onChange}>
				<MenuItem value='0' primaryText={formatMessage({id: 'classification_id_0'})}></MenuItem>
				<MenuItem value='1' primaryText={formatMessage({id: 'classification_id_1'})}></MenuItem>
				<MenuItem value='2' primaryText={formatMessage({id: 'classification_id_2'})}></MenuItem>
				<MenuItem value='3' primaryText={formatMessage({id: 'classification_id_3'})}></MenuItem>
				<MenuItem value='4' primaryText={formatMessage({id: 'classification_id_4'})}></MenuItem>
				<MenuItem value='5' primaryText={formatMessage({id: 'classification_id_5'})}></MenuItem>
			</SelectField>
		);
	}
}

// export component
export default PostClassificationSelect;
