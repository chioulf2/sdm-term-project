// require style
import 'quill/dist/quill.base.css';
import 'quill/dist/quill.snow.css';

// import dependencies
import React, {PropTypes} from 'react';
import {findDOMNode} from 'react-dom';
import BaseComponent from 'components/BaseComponent';
import Quill from 'quill';

// define component
class Editor extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// default state
	state = {
	}
	// props type
	static propTypes = {
	}
	// default props
	static defaultProps = {
	}
	// ***** Lifecycle *****
	// Invoked once, only on the client (not on the server), immediately after the initial rendering occurs.
	componentDidMount() {
		this.editor = new Quill(findDOMNode(this.refs['editor']), {
			theme: 'snow',
			modules: {
				'toolbar': { container: findDOMNode(this.refs['toolbar']) },
				'link-tooltip': true
			}
		});
		this.editor.on('text-change', (delta, source) => {
			if (source == 'api') {
			} else if (source == 'user') {
			}
			let value = this.editor.getHTML();
			// remove react id
			value = value.replace(/\sdata-reactid="(.+)"/g, '');
			this.props.onValueChange(value);
		});
		this.editor.setHTML(this.props.value);
	}
	// Invoked immediately after the component's updates are flushed to the DOM.
	componentDidUpdate() {
		if (this.editor.getHTML() !== this.props.value) {
			this.editor.setHTML(this.props.value);
		}
	}
	// ***** Lifecycle *****
	// ***** Custom Function *****
	// ***** Custom Function *****
	// define UI
  render() {
		let {style, className, ...other} = this.props;

    return (
      <div style={style || {height: '300px'}} className={`editor ${className}`}>
				<div ref='toolbar'>
					<select className='ql-size' defaultValue='13px'>
						<option value='10px'>Small</option>
						<option value='13px'>Normal</option>
						<option value='18px'>Large</option>
						<option value='32px'>Huge</option>
					</select>
					<span className="ql-format-group">
						<span title="Bold" className="ql-format-button ql-bold"></span>
						<span className="ql-format-separator"></span>
						<span title="Italic" className="ql-format-button ql-italic"></span>
						<span className="ql-format-separator"></span>
						<span title="Underline" className="ql-format-button ql-underline"></span>
						<span className="ql-format-separator"></span>
						<span title="Strikethrough" className="ql-format-button ql-strike"></span>
					</span>
					<span className="ql-format-group">
						<span title="List" className="ql-format-button ql-list"></span>
						<span className="ql-format-separator"></span>
						<span title="Bullet" className="ql-format-button ql-bullet"></span>
					</span>
				</div>
				<div ref='editor' style={{backgroundColor: '#F5F5F5'}}>
				</div>
			</div>
		);
	}
}

// export component
export default Editor;
