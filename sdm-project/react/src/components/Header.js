// require style
// require('styles/ExampleComponent.styl');

// import dependencies
import React, {PropTypes} from 'react';
import BaseComponent from 'components/BaseComponent';
import AppAction from 'actions/AppAction';
import Menu from 'components/Menu';
import {AppBar} from 'material-ui';

// define component
class Header extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// default state
	// props type
	static propTypes = {
	}
	// default props
	static defaultProps = {
	}
	// ***** Lifecycle *****
	// ***** Lifecycle *****
	// ***** Custom Function *****
	handleLeftButtonClick = () => {
		AppAction.setState({
			key: 'isSideMenuOpen',
			value: true
		});
	}
	handleTitleClick = () => {
		let {history} = this.props;
		//history.pushState(null, '/');
		window.location.href = '/';
	}
	// ***** Custom Function *****
	// define UI
  render() {
		let {style, className, ...other} = this.props;
		let {appState, intl} = this.props;
		let {formatMessage} = intl;

    return (
			<AppBar
				title={formatMessage({id: 'header_title'})}
				onLeftIconButtonTouchTap={this.handleLeftButtonClick}
				onTitleTouchTap={this.handleTitleClick}
				iconElementRight={<Menu {...other} />}
			/>
		);
	}
}

// export component
export default Header;
