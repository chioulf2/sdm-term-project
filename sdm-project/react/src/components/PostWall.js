// require style
import inlineStyle from 'styles/PostWall.js';
import classStyle from 'styles/PostWall.styl';

// import dependencies
import React, {PropTypes} from 'react';
import moment from 'moment';
import AppAction from 'actions/AppAction';
import PostAction from 'actions/PostAction';
import GroupAction from 'actions/GroupAction';
import SubscribeAction from 'actions/SubscribeAction';
import SearchAction from 'actions/SearchAction';
import {LeftNav, MenuItem, List, ListItem, Avatar, Divider, RefreshIndicator} from 'material-ui';
import BaseComponent from 'components/BaseComponent';
import Button from 'components/Button';
import Icon from 'components/Icon';
import CreatePostDialog from 'components/CreatePostDialog';
import CreateGroupDialog from 'components/CreateGroupDialog';
import PostClassificationSelect from 'components/PostClassificationSelect';
import AddMemberDialog from 'components/AddMemberDialog';
import PostSearchResult from 'components/PostSearchResult';
import PostSearchDialog from 'components/PostSearchDialog';

const PostItem = ({
	style,
	className,
	ownerAvatar,
	ownerName,
	content,
	datetime
}) => (
	<div style={style} className={className}>
		<ListItem
			leftAvatar={
				<Avatar src={ownerAvatar} />
			}
			primaryText={
				<div className={classStyle['item-primary-text']}>
					<span>{ownerName}</span>
					<span>{datetime}</span>
				</div>
			}
			secondaryText={
				<div dangerouslySetInnerHTML={{__html: content}}></div>
			}
			secondaryTextLines={1}
		/>
		<Divider inset={true} />
	</div>
)

// define component
class PostWall extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// default state
	state = {
		tagSource: []
	}
	// props type
	static propTypes = {
	}
	// default props
	static defaultProps = {
	}
	// ***** Lifecycle *****
	// ***** Lifecycle *****
	// ***** Custom Function *****
	handleLeftNavOpen = (open, reason) => {
		AppAction.setState({
			key: 'isGroupNavOpen',
			value: open
		});
	}
	handlePostDialogOpen = (open) => {
		AppAction.setState({
			key: 'isPostCreateDialogOpen',
			value: open
		});
	}
	handleGroupDialogOpen = (open) => {
		AppAction.setState({
			key: 'isGroupCreateDialogOpen',
			value: open
		});
	}
	handlePostCreate = () => {
		let {appState} = this.props;
		if (appState.getIn(['appData', 'newPostData', 'body']).trim() === '') {
			return;
		}
		// send data
		PostAction.createData({groupkey: appState.getIn(['appData', 'currentGroupkey']), ...appState.getIn(['appData', 'newPostData']).toJS()});
		// reset data
		AppAction.resetNewPostData();
		// close dialog
		AppAction.setState({
			key: 'isPostCreateDialogOpen',
			value: false
		});
	}
	handleGroupCreate = () => {
		let {appState} = this.props;
		let newData = appState.getIn(['appData', 'newGroupData']).toJS();
		if (newData.name.trim() === '') {
			return;
		} else if (newData.description.trim() === '') {
			return;
		}
		// send data
		GroupAction.addData(newData);
		// reset data
		AppAction.resetNewGroupData();
		// close dialog
		AppAction.setState({
			key: 'isGroupCreateDialogOpen',
			value: false
		});
	}
	handlePostNavigate = (id) => {
		let {history} = this.props;
		history.pushState(null, '/post/' + id);
	}
	handleSelectChange = (e, i, value) => {
		AppAction.setState({
			key: 'currentPostClassification',
			value
		})
	}
	handleSubscribe = () => {
		let {appState} = this.props;
		let subscribed = this.hasSubscribed();
		if (subscribed) {
			SubscribeAction.removeData(appState.getIn(['appData', 'currentPostClassification']));
		} else {
			SubscribeAction.addData(appState.getIn(['appData', 'currentPostClassification']));
		}
	}
	hasSubscribed = () => {
		let {appState} = this.props;
		return appState.get('subscribeData').findIndex(s => s.get('classification_id') === appState.getIn(['appData', 'currentPostClassification'])) !== -1;
	}
	getCurrentGroup = () => {
		let {appState} = this.props;
		return appState.get('groupData').find(g => g.get('key') === appState.getIn(['appData', 'currentGroupkey']));
	}
	handleGroupChange = (value, e) => {
		AppAction.setState({
			key: 'currentGroupkey',
			value
		});
		AppAction.setState({
			key: 'isGroupNavOpen',
			value: false
		});
	}
	handleMemberDialogOpen = (open) => {
		AppAction.setState({
			key: 'isMemberAddDialogOpen',
			value: open
		});
	}
	handleMemberAdd = () => {
		let {appState} = this.props;
		let groupkey = appState.getIn(['appData', 'currentGroupkey']);
		let member = appState.getIn(['appData', 'newMemberData']);
		GroupAction.addMember(groupkey, member);
		AppAction.resetNewMemberData();
		AppAction.setState({
			key: 'isMemberAddDialogOpen',
			value: false
		});
	}
	handlePostSearchResultOpen = (open) => {
		AppAction.setState({
			key: 'isPostSearchResultOpen',
			value: open
		});
	}
	handlePostSearchDialogOpen = (open) => {
		AppAction.setState({
			key: 'isPostSearchDialogOpen',
			value: open
		});
	}
	handleSearch = () => {
		let {appState} = this.props;
		let tagPrefix = appState.getIn(['searchData', 'tagPrefix']);
		let userPrefix = appState.getIn(['searchData', 'userPrefix']);
		let groupPrefix = appState.getIn(['searchData', 'groupPrefix']);
		SearchAction.searchPost(tagPrefix, userPrefix, groupPrefix);
		this.handlePostSearchDialogOpen(false);
		this.handlePostSearchResultOpen(true);
	}
	renderPostList = () => {
		let {style, className, ...other} = this.props;
		let {appState, intl} = this.props;
		let {formatMessage} = intl;
		let postData = appState.get('postData');

		if (postData === null) {
			return <RefreshIndicator size={40} left={20} top={20} status="loading" />;
		}

		return (
			<List>
				{appState.get('postData').filter(post => {
					return post.get('classification_id') === appState.getIn(['appData', 'currentPostClassification']);
				}).filter(post => {
					return post.get('groupkey') === appState.getIn(['appData', 'currentGroupkey']);
				}).map(post => {
					let owner = appState.getIn(['userData', 'other']).find(user => user.get('key') === post.get('ownerkey'));
					if (!owner) {
						owner = appState.getIn(['userData', 'key']) === post.get('ownerkey') ? appState.getIn(['userData', 'current']) : null;
					}
					return (
						<div onClick={this.handlePostNavigate.bind(null, post.get('key'))}
							key={post.get('key')}>
							<PostItem {...other}
								ownerAvatar={owner ? owner.get('avatar') : ''}
								ownerName={(owner ? owner.get('name') : '') || formatMessage({id: 'unknown_user'})}
								datetime={moment(post.get('created_at')).format('YYYY/MM/DD HH:mm')}
								content={post.get('body')}
							/>
						</div>
					);
				})}
			</List>
		);
	}
	renderGroupList = () => {
		let {appState, intl} = this.props;
		let {formatMessage} = intl;
		let currentGroupkey = appState.getIn(['appData', 'currentGroupkey']);
		return appState.get('groupData').map((group, i) => {
			return (
				<MenuItem key={group.get('key')}
					checked={currentGroupkey === group.get('key')}
					onTouchTap={this.handleGroupChange.bind(null, group.get('key'))}>
					{
						group.get('name') === 'dummy'
							? formatMessage({id: 'default_group_name'})
							: group.get('name')
					}
				</MenuItem>
			);
		});
	}
	// ***** Custom Function *****
	// define UI
  render() {
		let {style, className, ...other} = this.props;
		let {appState, intl} = this.props;
		let {formatMessage} = intl;

		let postList = this.renderPostList();
		let groupList = this.renderGroupList();

		let subscribed = this.hasSubscribed();
		let currentGroup = this.getCurrentGroup();

    return (
      <div style={style} className={`${classStyle.container} ${className}`}>
				<div className={classStyle.header}>
					<div className={classStyle.headerLeft}>
						<Button type='icon' onClick={this.handleLeftNavOpen.bind(null, true)}>
							<Icon type='font'>dashboard</Icon>
						</Button>
						<label>{currentGroup ? (currentGroup.get('name') === 'dummy' ? formatMessage({id: 'default_group_name'}) : currentGroup.get('name')) : ''}</label>
					</div>
					<div className={classStyle.headerRight}>
						<Button type='icon' onClick={this.handlePostSearchDialogOpen.bind(null, true)}>
							<Icon type='font'>search</Icon>
						</Button>
						<PostClassificationSelect {...other}
							style={{width: '150px', marginRight: '10px'}}
							value={appState.getIn(['appData', 'currentPostClassification'])}
							onChange={this.handleSelectChange} />
						{
							currentGroup
								? currentGroup.get('name') === 'dummy'
									? null
									:
										<Button style={{margin: '0 10px'}}
											type='raised'
											onClick={this.handleMemberDialogOpen.bind(null, true)}>
											{formatMessage({id: 'add_member'})}
										</Button>
								: null
							}
						<Button style={{margin: '0 10px'}}
							type='raised'
							onClick={this.handleSubscribe}>
							{formatMessage({id: subscribed ? 'unsubscribe' : 'subscribe'})}
						</Button>
					</div>
				</div>
				<div style={{position: 'relative'}}>
					{postList}
				</div>
				<Button type='floatingAction' style={inlineStyle.createBtn}
					onClick={this.handlePostDialogOpen.bind(null, true)}>
					<Icon type='font'>add</Icon>
				</Button>
				<LeftNav
					className={classStyle['group-container']}
					open={appState.getIn(['appData', 'isGroupNavOpen'])}
					docked={false}
					onRequestChange={this.handleLeftNavOpen}
					>
					<div className={classStyle['group-container-item-list']}>
						{groupList}
					</div>
					<Divider />
          <MenuItem onTouchTap={this.handleGroupDialogOpen.bind(null, true)}>{formatMessage({id: 'create_group'})}</MenuItem>
        </LeftNav>
				<CreatePostDialog {...other}
					isOpen={appState.getIn(['appData', 'isPostCreateDialogOpen'])}
					onRequestClose={this.handlePostDialogOpen.bind(null, false)}
					onRequestSubmit={this.handlePostCreate}
					title={formatMessage({id: 'post_create_dialog_title'})}
				/>
				<CreateGroupDialog {...other}
					isOpen={appState.getIn(['appData', 'isGroupCreateDialogOpen'])}
					onRequestClose={this.handleGroupDialogOpen.bind(null, false)}
					onRequestSubmit={this.handleGroupCreate}
					title={formatMessage({id: 'group_create_dialog_title'})}
				/>
				<AddMemberDialog {...other}
					isOpen={appState.getIn(['appData', 'isMemberAddDialogOpen'])}
					onRequestClose={this.handleMemberDialogOpen.bind(null, false)}
					onRequestSubmit={this.handleMemberAdd}
					title={formatMessage({id: 'add_member'})}
				/>
				<PostSearchResult {...other}
					isOpen={appState.getIn(['appData', 'isPostSearchResultOpen'])}
					onRequestClose={this.handlePostSearchResultOpen.bind(null, false)}
					onRequestSubmit={this.handlePostSearchResultOpen.bind(null, false)}
					title={formatMessage({id: 'search_result'})}
				/>
				<PostSearchDialog {...other}
					isOpen={appState.getIn(['appData', 'isPostSearchDialogOpen'])}
					onRequestClose={this.handlePostSearchDialogOpen.bind(null, false)}
					onRequestSubmit={this.handleSearch}
					onRequestOpenSearchResult={this.handlePostSearchResultOpen}
					title={formatMessage({id: 'search'})}
				/>
			</div>
		);
	}
}

// export component
export default PostWall;
