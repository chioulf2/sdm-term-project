// require style
// require('styles/ExampleComponent.styl');

// import dependencies
import React, {PropTypes} from 'react';
import BaseComponent from 'components/BaseComponent';
import {FlatButton, RaisedButton, IconButton, FloatingActionButton} from 'material-ui';

// define component
class Button extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// props type
	static propTypes = {
	}
	// default props
	static defaultProps = {
	}
	// ***** Lifecycle *****
	// ***** Lifecycle *****
	// ***** Custom Function *****
	renderButton = (type) => {
		switch (type) {
			case 'flat':
				return FlatButton;
			case 'raised':
				return RaisedButton;
			case 'icon':
				return IconButton;
			case 'floatingAction':
				return FloatingActionButton;
			default:
				return FlatButton;
		}
	}
	// ***** Custom Function *****
	// define UI
  render() {
		let {
			type,
			style,
			className,
			text,
			children,
			onClick,
			onTouchTap,
			primary,
			keyboardFocused,
			...other
		} = this.props;

		let TheButton = this.renderButton(type);

		return (
			<TheButton
				{...other}
				style={style}
				className={className}
				label={text}
				onTouchTap={onClick || onTouchTap}
				primary={primary}
				keyboardFocused={keyboardFocused}
				>
				{children}
			</TheButton>
		)
	}
}

// export component
export default Button;
