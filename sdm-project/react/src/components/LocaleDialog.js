// require style
// require('styles/ExampleComponent.styl');

// import dependencies
import React, {PropTypes} from 'react';
import BaseComponent from 'components/BaseComponent';
import Button from 'components/Button';
import {Dialog, RadioButtonGroup, RadioButton, FlatButton} from 'material-ui';

// define component
class LocaleDialog extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// ***** Lifecycle *****
	// ***** Lifecycle *****
	// ***** Custom Function *****
	handleChange = (evt, selected) => {
		if (!this._orgValue) {
			this._orgValue = this.props.intl.locale;
		}
		this.props.onRequestSwitchLocale(selected);
	}
	handleCancelTouchTap = () => {
		if (this._orgValue) {
			this.props.onRequestSwitchLocale(this._orgValue);
			delete this._orgValue;
		}
		// close dialog
		this.props.onRequestClose();
	}
	handleConfirmTouchTap = () => {
		// todo: save locale setting to DB or localStorage
		// close dialog
		this.props.onRequestClose();
		delete this._orgValue;
	}
	// ***** Custom Function *****
	// define UI
	render() {
		let {style, isOpen, onRequestClose, intl, ...other} = this.props;
		const {formatMessage, locale} = intl;
		// dialog buttons
		const actions = [
			<Button type='flat' text={formatMessage({id: 'cancel'})} onClick={this.handleCancelTouchTap} />,
			<Button type='flat' text={formatMessage({id: 'confirm'})} onClick={this.handleConfirmTouchTap} />
		];

		return (
			<Dialog
				title={formatMessage({id: 'locale_dialog_title'})}
				actions={actions}
				open={isOpen}
				onRequestClose={this.handleCancelTouchTap}>
				<RadioButtonGroup name="locale" defaultSelected={locale}
					onChange={this.handleChange}>
					<RadioButton
						value='en'
						label='English'
						style={{marginBottom:16}} />
					<RadioButton
						value='zh'
						label='中文'
						style={{marginBottom:16}}/>
				</RadioButtonGroup>
			</Dialog>
		);
	}
}

// export component
export default LocaleDialog;
