// require style
import classStyle from 'styles/PostPage.styl';

// import dependencies
import React, {PropTypes} from 'react';
import AppAction from 'actions/AppAction';
import PostAction from 'actions/PostAction';
import TagAction from 'actions/TagAction';
import moment from 'moment';
import {Avatar, TextField, IconMenu, MenuItem, RefreshIndicator} from 'material-ui';
import BaseComponent from 'components/BaseComponent';
import Header from 'components/Header';
import SideMenu from 'components/SideMenu';
import Button from 'components/Button';
import Icon from 'components/Icon';
import CreatePostDialog from 'components/CreatePostDialog';
import CreateReplyOfReplyDialog from 'components/CreateReplyOfReplyDialog';

class TagList extends BaseComponent {
	constructor(props) {
		super(props);
	}
	handleTagDelete = (tag_name) => {
		let {params} = this.props;
		let {key} = params;
		TagAction.removePostData(key, tag_name);
	}
	render() {
		let {appState} = this.props;
		let tags = appState.getIn(['tagData', 'post']);
		return (
			<div className={classStyle['tag-list']}>
				{
					tags.map(tag => {
						return (
							<span key={tag} className={classStyle['tag']}>
								{'@' + tag}
								<Button type='icon' onClick={this.handleTagDelete.bind(null, tag)}>
									<Icon type='font'>remove_circle_outline</Icon>
								</Button>
							</span>
						);
					})
				}
			</div>
		);
	}
}

class TagInput extends BaseComponent {
	constructor(props) {
		super(props);
	}
	handleTextChange = (e) => {
		let value = e.target.value;
		AppAction.setNewTagData({
			key: 'post',
			value
		});
	}
	handleTagCreate = () => {
		let {appState, params} = this.props;
		let {key} = params;
		let text = appState.getIn(['appData', 'newTagData', 'post']);
		if (text.trim() === '') {
			return;
		}
		TagAction.addPostData(key, text);
		AppAction.resetNewTagData();
	}
	render() {
		let {appState, intl} = this.props;
		let {formatMessage} = intl;
		return (
			<div>
				<span>@</span>
				<TextField
					style={{width: '80px'}}
					value={appState.getIn(['appData', 'newTagData', 'post'])}
					onChange={this.handleTextChange}
					hintText={formatMessage({id: 'add_tag'})} />
				<Button type='icon' onClick={this.handleTagCreate}>
					<Icon type='font'>add_circle_outline</Icon>
				</Button>
			</div>
		);
	}
}

class Item extends BaseComponent {
	render() {
		let {
			className,
			ownerAvatar,
			ownerName,
			content,
			datetime,
			onClick,
			replies = [],
			getOwner
		} = this.props;
		return (
			<div>
				<div className={`${classStyle['item']} ${className}`}
					onClick={onClick}>
					<div className={classStyle['item-left']}>
						<Avatar src={ownerAvatar} />
					</div>
					<div className={classStyle['item-right']}>
						<div className={classStyle['item-header']}>
							<span className={classStyle['name']}>{ownerName}</span>
							<span className={classStyle['datetime']}>{datetime}</span>
						</div>
						<pre dangerouslySetInnerHTML={{__html: content}} />
					</div>
				</div>
				{replies.map(reply => {
					let owner = getOwner(reply);
					return (
						<Item key={reply.get('key')}
							className={classStyle['sub-sub-item']}
							ownerAvatar={owner ? owner.get('avatar') : ''}
							ownerName={(owner ? owner.get('name') : '') || formatMessage({id: 'unknown_user'})}
							content={reply.get('body')}
							datetime={moment(reply.get('created_at')).format('YYYY/MM/DD HH:mm')}
						/>
					)
				})}
			</div>
		);
	}
}

// define component
class PostPage extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// props type
	static propTypes = {
	}
	// default props
	static defaultProps = {
	}
	// ***** Lifecycle *****
	componentWillMount() {
		if (this._checkLoginStatus()) {
			let {appState, params} = this.props;
			let {key} = params;
			TagAction.getPostData(key);
		}
	}
	// ***** Lifecycle *****
	// ***** Custom Function *****
	getOwner = (data) => {
		let {appState} = this.props;
		let owner = appState.getIn(['userData', 'other']).find(user => user.get('key') === data.get('ownerkey'));
		if (!owner) {
			owner = appState.getIn(['userData', 'key']) === data.get('ownerkey') ? appState.getIn(['userData', 'current']) : null;
		}
		return owner;
	}
	handleEnter = (e) => {
		let {appState, params} = this.props;
		let content = appState.getIn(['appData', 'newReplyData', 'body']);
		if (content.trim() === '') {
			return;
		}
		PostAction.addReply(content, params.key);
		AppAction.setNewReplyData({
			key: 'body',
			value: ''
		});
	}
	handleTextChange = (e) => {
		let value = e.target.value;
		AppAction.setNewReplyData({
			key: 'body',
			value
		});
	}
	handleDeleteRequest = (key) => {
		let {history} = this.props;
		PostAction.deleteData(key);
		history.pushState(null, '/');
	}
	handleEditRequest = (key) => {
		let {appState} = this.props;
		let post = appState.get('postData').find(post => post.get('key') === key);
		this.editKey = key;
		AppAction.setNewPostData({
			key: 'body',
			value: post.get('body')
		});
		AppAction.setNewPostData({
			key: 'classification_id',
			value: post.get('classification_id')
		});
		this.handlePostDialogOpen(true);
	}
	handlePostEdit = () => {
		let {appState} = this.props;
		let data = appState.getIn(['appData', 'newPostData']);
		if (appState.getIn(['appData', 'newPostData', 'body']).trim() === '') {
			return;
		}
		let {body, classification_id} = data.toJS();
		// send data
		PostAction.editData(this.editKey, {body, classification_id});
		// reset data
		AppAction.resetNewPostData();
		// close dialog
		AppAction.setState({
			key: 'isPostEditDialogOpen',
			value: false
		});
	}
	handlePostDialogOpen = (open) => {
		AppAction.setState({
			key: 'isPostEditDialogOpen',
			value: open
		});
	}
	handleReplyOfReplyDialogOpen = (open) => {
		AppAction.setState({
			key: 'isReplyOfReplyDialogOpen',
			value: open
		});
	}
	handleReplyReply = () => {
		let {appState} = this.props;
		PostAction.addReplyReply(appState.getIn(['appData', 'newReplyOfReplyData', 'body']), appState.getIn(['appData', 'isReplyOfReplyDialogOpen']));
		this.handleReplyOfReplyDialogOpen(false);
		AppAction.setNewReplyOfReplyData({
			key: 'body',
			value: ''
		});
	}
	renderMenuButton = (post) => {
		let {appState, intl} = this.props;
		let {formatMessage} = intl;
		let owner = this.getOwner(post);

		if (owner && owner.get('key') === appState.getIn(['userData', 'key'])) {
			return (
				<IconMenu iconButtonElement={<Button type='icon'><Icon type='font'>more_vert</Icon></Button>}>
					<MenuItem primaryText={formatMessage({id: 'edit_post'})} onTouchTap={this.handleEditRequest.bind(null, post.get('key'))} />
					<MenuItem primaryText={formatMessage({id: 'delete_post'})} onTouchTap={this.handleDeleteRequest.bind(null, post.get('key'))} />
				</IconMenu>
			);
		}
	}
	// ***** Custom Function *****
	// define UI
  render() {
		let {style, className, ...other} = this.props;
		let {appState, intl, params} = this.props;
		let {formatMessage} = intl;
		let {key} = params;
		let post = appState.get('postData')

		if (!post) {
			return <RefreshIndicator size={40} left={20} top={20} status="loading" />;
		}
	post = post.find(post => post.get('key') === key);
		let owner = this.getOwner(post);

		let menuButton = this.renderMenuButton(post);

		let isOwner = owner && owner.get('key') === appState.getIn(['userData', 'key']);

    return (
      <div style={style} className={`post-page ${className}`}>
				<Header {...other} />
				<SideMenu {...other} />
				<div className={classStyle['container']}>
					<div className={classStyle['header']}>
						{
							isOwner
								? <div style={{flex: 1}}></div>
								:	<div className={classStyle['tag-container']}>
										<TagInput {...other} />
										<TagList {...other} />
									</div>
						}
						{menuButton}
					</div>
					<div className={classStyle['item-container']}>
						<Item
							ownerAvatar={owner ? owner.get('avatar') : ''}
							ownerName={(owner ? owner.get('name') : '') || formatMessage({id: 'unknown_user'})}
							content={post.get('body')}
							datetime={moment(post.get('created_at')).format('YYYY/MM/DD HH:mm')}
						/>
						{
							post.get('replies').map(reply => {
								let owner = this.getOwner(reply);
								return (
									<Item key={reply.get('key')}
										onClick={this.handleReplyOfReplyDialogOpen.bind(null, reply.get('key'))}
										className={classStyle['sub-item']}
										ownerAvatar={owner ? owner.get('avatar') : ''}
										ownerName={(owner ? owner.get('name') : '') || formatMessage({id: 'unknown_user'})}
										content={reply.get('body')}
										datetime={moment(reply.get('created_at')).format('YYYY/MM/DD HH:mm')}
										replies={reply.get('replies')}
										getOwner={this.getOwner}
									/>
								);
							})
						}
					</div>
					<div className={classStyle['input-container']}>
						<TextField
							value={appState.getIn(['appData', 'newReplyData', 'body'])}
							onChange={this.handleTextChange}
							onEnterKeyDown={this.handleEnter}
							fullWidth={true}
							hintText={formatMessage({id: 'reply_hint_text'})} />
					</div>
				</div>
				<CreatePostDialog {...other}
					isOpen={appState.getIn(['appData', 'isPostEditDialogOpen'])}
					onRequestClose={this.handlePostDialogOpen.bind(null, false)}
					onRequestSubmit={this.handlePostEdit}
					title={formatMessage({id: 'post_edit_dialog_title'})} />
				<CreateReplyOfReplyDialog {...other}
					isOpen={appState.getIn(['appData', 'isReplyOfReplyDialogOpen']) ? true : false}
					onRequestClose={this.handleReplyOfReplyDialogOpen.bind(null, false)}
					onRequestSubmit={this.handleReplyReply}
					title={formatMessage({id: 'reply_of_reply_dialog_title'})} />
			</div>
		);
	}
}

// export component
export default PostPage;
