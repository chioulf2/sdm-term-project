// require style
import classStyle from 'styles/ExampleComponent.styl';
import inlineStyle from 'styles/ExampleComponent.js';

// import dependencies
import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import BaseComponent from 'components/BaseComponent';
import ActionType from 'constants/ActionType';
import ExampleAction from 'actions/ExampleAction';
import ExampleStore from 'stores/ExampleStore';
import Button from 'components/Button';

// define component
class ExampleComponent extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// default state
	state = {
		foo: 'bar',
		addFoo: ExampleStore.getState()
	}
	// props type
	static propTypes = {
		foo: PropTypes.string
	}
	// default props
	static defaultProps = {
		foo: 'bar'
	}
	// ***** Lifecycle *****
	// Invoked once, both on the client and server, immediately before the initial rendering occurs.
	componentWillMount() {
	}
	// Invoked once, only on the client (not on the server), immediately after the initial rendering occurs.
	componentDidMount() {
		// add listener
		ExampleStore.addChangeListener(this.onStoreChange);
	}
	// Invoked when a component is receiving new props.
	componentWillReceiveProps(nextProps, nextState) {
	}
	// Invoked before rendering when new props or state are being received.
	shouldComponentUpdate(nextProps, nextState) {
		return true;
	}
	// Invoked immediately before rendering when new props or state are being received.
	componentWillUpdate() {
	}
	// Invoked immediately after the component's updates are flushed to the DOM.
	componentDidUpdate() {
	}
	// Invoked immediately before a component is unmounted from the DOM.
	componentWillUnmount() {
		// remove listener
		ExampleStore.removeChangeListener(this.onStoreChange);
	}
	// ***** Lifecycle *****
	// ***** Custom Function *****
	foo = () => {
		alert(this.state.foo);
	}
	onStoreChange = () => {
		// update component state to the lastest store state
		this.setState({
			addFoo: ExampleStore.getState()
		});
	}
	handleClick = () => {
		// dispatch action
		ExampleAction.add(1);
	}
	// ***** Custom Function *****
	// define UI
  render() {
		let {style, className, ...other} = this.props;
		let {appState, intl} = this.props;
		let {formatMessage} = intl;

    return (
      <div style={style} className={`${classStyle['container']} ${className}`}>
				<Button type='raised' onClick={this.foo}>Foo</Button>
				<h3>State foo: {this.state.foo}</h3>
				<h3>Props foo: {this.props.foo}</h3>
				<h3>State addFoo: {this.state.addFoo}</h3>
				<Button type='flat' onClick={this.handleClick}>Add Foo</Button>
				<Link to='login'><Button>LoginPage</Button></Link>
				<span style={inlineStyle.text}>{formatMessage({id: 'test'}, {name:'annie'})}</span>
			</div>
		);
	}
}

// export component
export default ExampleComponent;
