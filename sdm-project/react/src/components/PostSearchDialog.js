// require style
import classStyle from 'styles/CreateGroupDialog.styl';

// import dependencies
import React, {PropTypes} from 'react';
import AppAction from 'actions/AppAction';
import SearchAction from 'actions/SearchAction';
import BaseComponent from 'components/BaseComponent';
import Button from 'components/Button';
import {Dialog, AutoComplete} from 'material-ui';

// define component
class PostSearchDialog extends BaseComponent {
	constructor(props) {
		super(props);
	}
	handleAutoComplete = (string, filter) => {
		let {appState} = this.props;
		if (this.completeTimeout) {
			clearTimeout(this.completeTimeout);
		}
		this.completeTimeout = setTimeout(() => {
			if (filter === 'tag') {
				SearchAction.getPostTagComplete(string);
			} else if (filter === 'user') {
				SearchAction.getUserNameComplete(string);
			} else if (filter === 'group') {
				SearchAction.getGroupNameComplete(string);
			}
		}, 500);
		SearchAction.setPrefix(filter, string);
	}
	handleChoose = (value, filter) => {
		SearchAction.setPrefix(filter, value);
	}
	getDataSource = (filter) => {
		let {appState} = this.props;
		if (filter === 'tag') {
			return appState.getIn(['searchData', 'postTag']).toJS()
		} else if (filter === 'user') {
			return appState.getIn(['searchData', 'userName']).toJS().map(d => d.name);
		} else if (filter === 'group') {
			return appState.getIn(['searchData', 'groupName']).toJS().map(d => d.name);
		}
	}
	render() {
		let {title, isOpen, onRequestClose, onRequestSubmit, ...other} = this.props;
		let {appState, intl} = this.props;
		let {formatMessage} = intl;

		const actions = [
      <Button
				type='flat'
				text={formatMessage({id: 'cancel'})}
        secondary={true}
        onClick={onRequestClose} />,
      <Button
				type='flat'
				text={formatMessage({id: 'confirm'})}
        primary={true}
        keyboardFocused={true}
        onClick={onRequestSubmit} />
    ];

		return (
			<Dialog
        title={title}
        actions={actions}
        modal={false}
        open={isOpen}
        onRequestClose={onRequestClose}>
				<div>
					<label style={{display: 'inline-block', marginTop: '10px'}}>{formatMessage({id: 'search_tag'})}: </label>
					<AutoComplete
						fullWidth={true}
						dataSource={this.getDataSource('tag')}
						onUpdateInput={(t) => {this.handleAutoComplete(t, 'tag')}}
						onNewRequest={(t) => {this.handleChoose(t, 'tag')}} />
				</div>
				<div>
					<label style={{display: 'inline-block', marginTop: '10px'}}>{formatMessage({id: 'search_user'})}: </label>
					<AutoComplete
						fullWidth={true}
						dataSource={this.getDataSource('user')}
						onUpdateInput={(t) => {this.handleAutoComplete(t, 'user')}}
						onNewRequest={(t) => {this.handleChoose(t, 'user')}} />
				</div>
				<div>
					<label style={{display: 'inline-block', marginTop: '10px'}}>{formatMessage({id: 'search_group'})}: </label>
					<AutoComplete
						fullWidth={true}
						dataSource={this.getDataSource('group')}
						onUpdateInput={(t) => {this.handleAutoComplete(t, 'group')}}
						onNewRequest={(t) => {this.handleChoose(t, 'group')}} />
				</div>
      </Dialog>
		);
	}
}
// export component
export default PostSearchDialog;
