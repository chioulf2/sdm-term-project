// require style
//require('styles/ExampleComponent.styl');

// import dependencies
import React, {PropTypes} from 'react';
import BaseComponent from 'components/BaseComponent';
import UserAction from 'actions/UserAction';
import PostAction from 'actions/PostAction';
import TagAction from 'actions/TagAction';
import SubscribeAction from 'actions/SubscribeAction';
import GroupAction from 'actions/GroupAction';
import Header from 'components/Header';
import SideMenu from 'components/SideMenu';
import PostWall from 'components/PostWall';

// define component
class MainPage extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// props type
	static propTypes = {
	}
	// default props
	static defaultProps = {
	}
	// ***** Lifecycle *****
	componentWillMount() {
		if (this._checkLoginStatus()) {
			UserAction.getCurrentData();
			UserAction.getOtherData();
			UserAction.getEducationData();
			PostAction.getData();
			//TagAction.getData();
			SubscribeAction.getData();
			GroupAction.getData();
			// for first-login user to fill profile
			this._checkLanding();
		}
	}
	// ***** Lifecycle *****
	// ***** Custom Function *****
	// ***** Custom Function *****
	// define UI
  render() {
		let {style, className, children, ...other} = this.props;
		let {formatMessage} = this.props.intl;

    return (
			<div style={style} className={`main-page ${className}`}>
				{React.cloneElement(children, {...other})}
			</div>
		);
	}
}

// export component
export default MainPage;
