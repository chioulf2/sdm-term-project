// require style
import classStyle from 'styles/PostSearchBar.styl';
// import inlineStyle from 'styles/ExampleComponent.js';

// import dependencies
import React, {PropTypes} from 'react';
import SearchAction from 'actions/SearchAction';
import BaseComponent from 'components/BaseComponent';
import Icon from 'components/Icon';
import {AutoComplete, SelectField, MenuItem} from 'material-ui';

// define component
class PostSearchBar extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// default state
	state = {
	}
	// props type
	static propTypes = {
	}
	// default props
	static defaultProps = {
	}
	// ***** Lifecycle *****
	// ***** Lifecycle *****
	// ***** Custom Function *****
	handleAutoComplete = (string) => {
		let {appState} = this.props;
		let filter = appState.getIn(['searchData', 'filter']);
		if (filter === 'tag') {
			SearchAction.getPostTagComplete(string);
		} else if (filter === 'user') {
			SearchAction.getUserNameComplete(string);
		} else if (filter === 'group') {
			SearchAction.getGroupNameComplete(string);
		}
	}
	handleSearch = (value) => {
		let {appState} = this.props;
		let filter = appState.getIn(['searchData', 'filter']);
		if (filter === 'tag') {
			SearchAction.searchPostByTag(value);
		} else if (filter === 'user') {
			SearchAction.searchPostByUser(value);
		} else if (filter === 'group') {
			SearchAction.searchPostByGroup(value);
		}
		this.props.onRequestOpenSearchResult(true);
	}
	handleSelect = (e, i, value) => {
		SearchAction.changeFilter(value);
	}
	getDataSource = () => {
		let {appState} = this.props;
		let filter = appState.getIn(['searchData', 'filter']);
		if (filter === 'tag') {
			return appState.getIn(['searchData', 'postTag']).toJS()
		} else if (filter === 'user') {
			return appState.getIn(['searchData', 'userName']).toJS().map(d => d.name);
		} else if (filter === 'group') {
			return appState.getIn(['searchData', 'groupName']).toJS().map(d => d.name);
		}
	}
	// ***** Custom Function *****
	// define UI
  render() {
		let {style, className, ...other} = this.props;
		let {appState, intl} = this.props;
		let {formatMessage} = intl;

		let dataSource = this.getDataSource();
    return (
      <div style={style} className={`${classStyle['container']} ${className}`}>
				<Icon type='font'>search</Icon>
				<AutoComplete
					style={{width: '150px'}}
					dataSource={dataSource}
					onUpdateInput={(t) => {this.handleAutoComplete(t)}}
					onNewRequest={(t) => {this.handleSearch(t)}} />
				<SelectField
					style={{width: '100px'}}
					value={appState.getIn(['searchData', 'filter'])}
					onChange={this.handleSelect}>
					<MenuItem value='tag' primaryText={formatMessage({id: 'search_tag'})}></MenuItem>
					<MenuItem value='user' primaryText={formatMessage({id: 'search_user'})}></MenuItem>
					<MenuItem value='group' primaryText={formatMessage({id: 'search_group'})}></MenuItem>
				</SelectField>
			</div>
		);
	}
}

// export component
export default PostSearchBar;
