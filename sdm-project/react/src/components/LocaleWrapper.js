// require style

// import dependencies
import React, {PropTypes} from 'react';
import BaseComponent from './BaseComponent.js';
import LocaleDialog from './LocaleDialog.js';
import App from './App.js';
// import locale
import {IntlProvider, addLocaleData, injectIntl} from 'react-intl';
import zh from 'react-intl/lib/locale-data/zh';
import intlData from '../locales/index.js';
// add zh into locale data
addLocaleData(zh);

class Container extends BaseComponent {
	render() {
		let {isLocaleDialogOpen, onRequestOpenLocaleDialog, ...other} = this.props;
		let {onRequestSwitchLocale} = this.props;
		return (
			<div>
				<App {...other}
					onRequestOpenLocaleDialog={onRequestOpenLocaleDialog} />
				<LocaleDialog {...other}
					isOpen={isLocaleDialogOpen}
					onRequestClose={onRequestOpenLocaleDialog.bind(null, false)}
					onRequestSwitchLocale={onRequestSwitchLocale} />
			</div>
		);
	}
}
// inject intl into the tree
const InjectedContainer = injectIntl(Container);

// define component
class LocaleWrapper extends BaseComponent {
	// constructor: initial variables here
	constructor(props) {
		super(props);
	}
	// default state
	state = {
		locale: localStorage.getItem('locale') || 'en',
		isLocaleDialogOpen: false
	}
	// ***** Custom Function *****
	switchLocale = (locale) => {
		this.setState({locale}, () => {
			localStorage.setItem('locale', locale);
		});
	}
	openLocaleDialog = (flag) => {
		this.setState({
			isLocaleDialogOpen: typeof flag === 'boolean' ? flag : !this.state.isLocaleDialogOpen
		});
	}
	// ***** Custom Function *****
	// define UI
  render() {
		let {locale, isLocaleDialogOpen} = this.state;
    return (
			<IntlProvider locale={locale} messages={intlData[locale]}>
				<InjectedContainer
					{...this.props}
					isLocaleDialogOpen={isLocaleDialogOpen}
					onRequestSwitchLocale={this.switchLocale}
					onRequestOpenLocaleDialog={this.openLocaleDialog}
					/>
			</IntlProvider>
		);
	}
}

// export component
export default LocaleWrapper;
