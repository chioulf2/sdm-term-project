// require style
import classStyle from 'styles/ExampleComponent.styl';
import inlineStyle from 'styles/ExampleComponent.js';

// import dependencies
import React, {PropTypes} from 'react';
import AppAction from 'actions/AppAction';
import BaseComponent from 'components/BaseComponent';
import Button from 'components/Button';
import PostClassificationSelect from 'components/PostClassificationSelect';
import Editor from 'components/Editor';
import {Dialog} from 'material-ui';

// define component
class CreatePostDialog extends BaseComponent {
	constructor(props) {
		super(props);
	}
	handleEditChange = (value) => {
		AppAction.setNewPostData({
			key: 'body',
			value: value
		});
	}
	handleSelectChange = (e, i, value) => {
		AppAction.setNewPostData({
			key: 'classification_id',
			value
		});
	}
	render() {
		let {title, isOpen, onRequestClose, onRequestSubmit, ...other} = this.props;
		let {appState, intl} = this.props;
		let {formatMessage} = intl;

		const actions = [
      <Button
				type='flat'
				text={formatMessage({id: 'cancel'})}
        secondary={true}
        onClick={onRequestClose} />,
      <Button
				type='flat'
				text={formatMessage({id: 'confirm'})}
        primary={true}
        keyboardFocused={true}
        onClick={onRequestSubmit} />
    ];

		return (
			<Dialog
        title={title}
        actions={actions}
        modal={false}
        open={isOpen}
        onRequestClose={onRequestClose}>
				<PostClassificationSelect {...other}
					floatingLabelText={formatMessage({id: 'classification'})}
					value={appState.getIn(['appData', 'newPostData', 'classification_id'])}
					onChange={this.handleSelectChange} />
				<Editor
					value={appState.getIn(['appData', 'newPostData', 'body'])}
					onValueChange={this.handleEditChange}
				/>
      </Dialog>
		);
	}
}
// export component
export default CreatePostDialog;
