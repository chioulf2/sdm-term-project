// import dependencies
import React from 'react';

// define component
class BaseComponent extends React.Component {
	// constructor
	constructor(props) {
		super(props);
	}
	_checkLoginStatus = () => {
		let {history, appState} = this.props;
		if (!appState.getIn(['userData', 'key'])) {
			return history.pushState(null, 'login');
		}
		return true;
	}
	_checkLanding = () => {
		let {history, appState} = this.props;
		if (!appState.getIn(['userData', 'landing'])) {
			return history.pushState(null, 'profile');
		}
	}
	// custom function for all components
	helloWorld = () => {
		alert('hello world');
	}
}

// export component
export default BaseComponent;
