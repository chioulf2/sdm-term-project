// require style
import classStyle from 'styles/CreateGroupDialog.styl';

// import dependencies
import React, {PropTypes} from 'react';
import AppAction from 'actions/AppAction';
import BaseComponent from 'components/BaseComponent';
import Button from 'components/Button';
import {Dialog, List, ListItem, Avatar} from 'material-ui';

// define component
class AddMemberDialog extends BaseComponent {
	constructor(props) {
		super(props);
	}
	handleSelect = (key) => {
		let {appState} = this.props;
		let members = appState.getIn(['appData', 'newMemberData']);
		if (members.indexOf(key) !== -1) {
			members = members.filter(m => m !== key);
		} else {
			members = members.push(key);
		}
		AppAction.setNewMemberData(members);
	}
	render() {
		let {title, isOpen, onRequestClose, onRequestSubmit, ...other} = this.props;
		let {appState, intl} = this.props;
		let {formatMessage} = intl;
		let members = appState.getIn(['appData', 'newMemberData']);

		const actions = [
      <Button
				type='flat'
				text={formatMessage({id: 'cancel'})}
        secondary={true}
        onClick={onRequestClose} />,
      <Button
				type='flat'
				text={formatMessage({id: 'confirm'})}
        primary={true}
        keyboardFocused={true}
        onClick={onRequestSubmit} />
    ];

		let others = appState.getIn(['userData', 'other']);

		return (
			<Dialog
        title={title}
        actions={actions}
        modal={false}
				bodyStyle={{overflow: 'auto'}}
        open={isOpen}
        onRequestClose={onRequestClose}>
				<List>
					{
						others.map(other => {
							return (
								<ListItem key={other.get('key')}
									style={members.indexOf(other.get('key')) !== -1 ? {backgroundColor: '#DEDEDE'} : {}}
									leftAvatar={<Avatar src={other.get('avatar')} />}
									primaryText={other.get('name')}
									onTouchTap={this.handleSelect.bind(null, other.get('key'))}
									>
								</ListItem>
							);
						})
					}
				</List>
      </Dialog>
		);
	}
}
// export component
export default AddMemberDialog;
