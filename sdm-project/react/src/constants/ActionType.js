// import dependencies
import keymirror from 'keymirror';
export default keymirror({
	CHANGE: null,
	EXAMPLE_ADD: null,
	APP_SET_STATE: null,
	APP_SET_NEW_POST_DATA: null,
	APP_RESET_NEW_POST_DATA: null,
	APP_SET_NEW_REPLY_DATA: null,
	APP_SET_NEW_REPLY_OF_REPLY_DATA: null,
	APP_SET_NEW_GROUP_DATA: null,
	APP_RESET_NEW_GROUP_DATA: null,
	APP_SET_NEW_TAG_DATA: null,
	APP_RESET_NEW_TAG_DATA: null,
	APP_SET_NEW_MEMBER_DATA: null,
	APP_RESET_NEW_MEMBER_DATA: null,
	USER_SET_CURRENT_KEY: null,
	USER_SET_CURRENT_LANDING: null,
	USER_GET_CURRENT_DATA: null,
	USER_UPDATE_CURRENT_DATA: null,
	USER_GET_OTHER_DATA: null,
	USER_GET_EDECATION_DATA: null,
	USER_ADD_EDUCATION_DATA: null,
	USER_ADD_EXPERIENCE_DATA: null,
	USER_UPLOAD_AVATAR: null,
	USER_UPDATE_BIOGRAPHY: null,
	POST_GET_DATA: null,
	POST_ADD_DATA: null,
	POST_DELETE_DATA: null,
	POST_EDIT_DATA: null,
	REPLY_ADD_DATA: null,
	REPLY_OF_REPLY_ADD_DATA: null,
	TAG_ADD_DATA: null,
	TAG_GET_USER: null,
	TAG_GET_POST: null,
	TAG_ADD_USER: null,
	TAG_ADD_POST: null,
	SUBSCRIBE_GET_DATA: null,
	SUBSCRIBE_ADD_DATA: null,
	SUBSCRIBE_REMOVE_DATA: null,
	GROUP_GET_DATA: null,
	GROUP_ADD_DATA: null,
	GROUP_ADD_MEMBER: null,
	GET_ALUMNI_DATA: null,
	SEARCH_TAG_COMPLETE: null,
	SEARCH_POST_BY_TAG: null,
	SEARCH_USER_BY_TAG: null,
	SEARCH_POST_TAG_COMPLETE: null,
	SEARCH_USER_NAME_COMPLETE: null,
	SEARCH_GROUP_NAME_COMPLETE: null,
	SEARCH_POST: null,
	SEARCH_CHANGE_FILTER: null,
	SEARCH_SET_PREFIX: null
});
