export default {
	test: '{name} 嗨',
	locale: '語系選擇',
	locale_dialog_title: '請選擇語系',
	cancel: '取消',
	confirm: '確認',
	login: '登入',
	logout: '登出',
	login_page: '登入頁面',
	profile_page: '個人資料',
	post_hint: '嗨~大家好',
	contact_tab: '聯絡資訊',
	education_tab: '教育程度',
	experience_tab: '工作經歷',
	username: '姓名',
	address: '地址',
	email: '電子信箱',
	phone_mobile: '行動電話',
	phone_work: '公司電話',
	phone_home: '家用電話',
	enter_username: '輸入姓名',
	enter_address: '輸入地址',
	enter_email: '輸入電子信箱',
	enter_phone_mobile: '輸入行動電話',
	enter_phone_work: '輸入公司電話',
	enter_phone_home: '輸入家用電話',
	create: '新增',
	post_create_dialog_title: '發布POST',
	post_edit_dialog_title: '編輯POST',
	group_create_dialog_title: '建立群組',
	reply_create_dialog_title: '留言',
	title: '標題',
	content: '內容',
	education_degree: '學歷',
	education_degree_bachelor: '學士',
	education_degree_master: '碩士',
	education_degree_doctor: '博士',
	education_student_id: '學號',
	education_institute: '學校',
	education_department: '系所',
	education_start_date: '開始日期',
	education_end_date: '結束日期',
	experience_organization: '組織',
	experience_department: '部門',
	experience_position: '職位',
	experience_description: '工作描述',
	save: '儲存',
	post_wall: '文章牆',
	alumni_wall: '校友牆',
	add:'新增',
	add_tag: '貼標籤',
	enter_tag_name: '輸入新標籤',
	header_title: '校友書',
	unknown_user: '無名氏',
	reply_hint_text: '回覆..',
	classification: '分類',
	classification_id_0: '一般',
	classification_id_1: '閒聊',
	classification_id_2: '徵才',
	classification_id_3: '募資',
	classification_id_4: '技能',
	classification_id_5: '疑問',
	subscribe: '訂閱',
	unsubscribe: '取消訂閱',
	create_group: '建立群組',
	group_name: '群組名稱',
	group_description: '群組敘述',
	group_private: '私人',
	default_group_name: '校園',
	autobiography: '自傳',
	user_tag: '用戶標籤',
	search: '搜尋',
	add_member: '增加成員',
	edit_post: '編輯',
	delete_post: '刪除',
	search_result: '搜尋結果',
	login_intro_post: '發文',
	login_intro_group: '群組',
	login_intro_alumni: '校友',
	login_intro_locale: '語系',
	search_tag: '標籤',
	search_user: '使用者',
	search_group: '群組',
	analytics: '統計資料',
	reply_of_reply_dialog_title: '回覆Reply',
	reply_of_reply_hint_text: '回覆回覆...'
};
