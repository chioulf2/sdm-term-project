import React from 'react';

export default {
	childContextTypes: {
		masonryUpdate: React.PropTypes.func
	},
	getChildContext: function () {
		return {
			masonryUpdate: this.forceUpdate.bind(this)
		};
	}
}
