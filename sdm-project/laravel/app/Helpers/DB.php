<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB as DBFacade;
use Illuminate\Database\SQLiteConnection;

class DB {
    /**
     * Turn on/off the database foreign key constraint option.
     *
     * @param  bool  $on
     * @return void
     */
    public static function foreignKeyConstraint($on)
    {
        if ($on) {
            if (DBFacade::connection() instanceof SQLiteConnection) {
                DBFacade::statement('PRAGMA foreign_keys = ON');
            }
        } else {
            if (DBFacade::connection() instanceof SQLiteConnection) {
                DBFacade::statement('PRAGMA foreign_keys = OFF');
            }
        }
    }
}
