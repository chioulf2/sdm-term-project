<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;
use App\Jobs\Job;
use App\Entities as Entities;


class SendSubscribeNotificationEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;


    protected $classificationId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($classification_id)
    {
        $this->classificationId = $classification_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->attempts() < 3) {
            $userIds = Entities\UserSubscribe::where('classification_id', $this->classificationId)
                    ->get()->pluck('user_id'); #取出所選分類的user_id
            $userEmailList = Entities\ExternalId::whereIn('user_id', $userIds)->get()->pluck('body');


            $classification = ['一般', '閒聊', '徵才', '募資', '技能', '疑問'];
            $id = $this->classificationId;

            foreach ($userEmailList as $userEmail) {
                $userEmail = $userEmail . "@ntu.edu.tw";
                $data = [
                    'classification' => $classification[$id],
                ];
                Mail::send('laravel.resources.views.emails.notification', $data,
                    function($message) use ($userEmail)
                {
                    $message->from('sdm-group-3@mailgun.com', 'sdm-group-3');
                    $message->to($userEmail)->subject('校友系統-新文章通知');
                });
            }

            // $mail_content = "您好:
            // 您在校友系統訂閱的文章分類---「" .$classification[$id]. "」中
            // 有新的主題發佈了！
            // 快點回系統看看，並參與討論吧！";
            // foreach ($userEmailList as $userEmail) {
            //     $userEmail = $userEmail . "@ntu.edu.tw";
            //     Mail::raw($mail_content, function($message) use ($userEmail)
            //     {
            //         $message->from('sdm-group-3@mailgun.com', 'sdm-group-3');
            //         $message->to($userEmail)->subject('校友系統-新文章通知');
            //     });
            // }
        }
    }
}
