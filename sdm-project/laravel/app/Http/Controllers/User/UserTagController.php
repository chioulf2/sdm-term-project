<?php

namespace App\Http\Controllers\User;

use App\Entities\User;
use App\Entities\UserTag;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserTagController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\User  $target
     * @return array
     */
    public function index(Request $request, User $target)
    {
        $owner = $request->user();

        return $owner->userTagData->where('target_user_id', $target->id)->pluck('tag');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\User  $target
     * @param  string  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $target, $tag)
    {
        if (!$tag) {
            return response('User Tag Not Provided', 404);
        } else {
            $owner = $request->user();

            $already = $this->findTag([
                'owner_user_id' => $owner->id,
                'target_user_id' => $target->id,
                'tag' => $tag,
            ]);
            $already ? null : UserTag::makeAndSave($owner, $target, $tag);
        }

        return $tag;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\User  $target
     * @param  string  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $target, $tag)
    {
        if (!$tag) {
            return response('User Tag Not Provided', 404);
        } else {
            $owner = $request->user();

            $found = $this->findTag([
                'owner_user_id' => $owner->id,
                'target_user_id' => $target->id,
                'tag' => $tag,
            ]);

            return $found->delete() ? response('OK', 200) : response('User Tag Not Found', 404);
        }
    }

    /**
     * Find the user tag from storage with the provided composit attributes.
     *
     * @param  array  $composite
     * @return \App\Entities\UserTag
     */
    private function findTag($composite)
    {
        return UserTag::where('owner_user_id', $composite['owner_user_id'])
            ->where('target_user_id', $composite['target_user_id'])
            ->where('tag', $composite['tag'])->first();
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request)
    {
        $prefix = $request->input('prefix');
        $tags = UserTag::where('owner_user_id', $request->user()->id)
            ->where('tag', 'LIKE', $prefix . '%')->get();

        return array_unique($tags->pluck('tag')->toArray());
    }
}
