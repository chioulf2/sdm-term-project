<?php

namespace App\Http\Controllers\User;

use App\Entities\Shared as Shared;
use App\Entities\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $userExtId = $user->externalIds->first()->body;
        $contact = Shared\Contact::where('id', $userExtId)->first();
        $educationcations = Shared\Education::where('id', $userExtId)->get();
        $experienceeriences = Shared\Experience::where('id', $userExtId)->get();

        return response()->json([
            'contact' => $contact,
            'educations' => $educationcations,
            'experiences' => $experienceeriences,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function getContact(User $user)
    {
        $userExtId = $user->externalIds->first()->body;

        return Shared\Contact::where('id', $userExtId)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function putContact(Request $request, User $user)
    {
        $userExtId = $user->externalIds->first()->body;
        $found = Shared\Contact::where('id', $userExtId)->first();
        $contact = $found ?: new Shared\Contact;

        $contact->id = $userExtId;
        $contact->name = $request->input('name') ?: $contact->name;
        $contact->address = $request->input('address') ?: $contact->address;
        $contact->email = $request->input('email') ?: $contact->email;
        $contact->phone_mobile = $request->input('phone_mobile') ?: $contact->phone_mobile;
        $contact->phone_work = $request->input('phone_work') ?: $contact->phone_work;
        $contact->phone_home = $request->input('phone_home') ?: $contact->phone_home;
        $contact->save();

        return $contact;
    }

    /**
     * Find the education with the provided composite key in storage.
     *
     * @param  array  $composite
     * @return \App\Entities\Shared\Experience
     */
    private function findEducation($composite)
    {
        return Shared\Education::where('id', $composite['id'])
            ->where('degree', $composite['degree'])
            ->where('institute', $composite['institute'])
            ->where('start_date', $composite['start_date'])
            ->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function getEducations(User $user)
    {
        $userExtId = $user->externalIds->first()->body;

        return Shared\Education::where('id', $userExtId)->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function putEducation(Request $request, User $user)
    {
        $userExtId = $user->externalIds->first()->body;
        $inputDegree = $request->input('degree');
        $inputInstitute = $request->input('institute');
        $inputStartDate = $request->input('start_date');

        $found = $this->findEducation([
            'id' => $userExtId,
            'degree' => $inputDegree,
            'institute' => $inputInstitute,
            'start_date' => $inputStartDate,
        ]);

        $education = $found ?: new Shared\Education;
        $education->id = $userExtId;
        $education->degree = $inputDegree;
        $education->institute = $inputInstitute;
        $education->department = $request->input('department') ?: $education->department;
        $education->start_date = $inputStartDate;
        $education->end_date = $request->input('end_date') ?: $education->end_date;
        $education->concentration = $request->input('concentration') ?: $education->concentration;
        $education->obtained = $request->input('obtained') ?: $education->obtained;
        $education->save();

        return $education;
    }

    /**
     * Delete the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function deleteEducation(Request $request, User $user)
    {
        $id = $user->externalIds->first()->body;
        $degree = $request->input('degree');
        $institute = $request->input('institute');
        $start_date = $request->input('start_date');

        $deleted = DB::connection(config('database.shared'))->table('education')
            ->where('id', $id)
            ->where('degree', $degree)
            ->where('institute', $institute)
            ->where('start_date', $start_date)
            ->delete();

        if ($deleted) {
            return response('OK', 200);
        } else {
            return response('Education Not Found', 404);
        }
    }

    /**
     * Find the experience with the provided composite key in storage.
     *
     * @param  array  $composite
     * @return \App\Entities\Shared\Experience
     */
    private function findExperience($composite)
    {
        return Shared\Experience::where('id', $composite['id'])
            ->where('organization', $composite['organization'])
            ->where('start_date', $composite['start_date'])
            ->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function getExperiences(User $user)
    {
        $userExtId = $user->externalIds->first()->body;

        return Shared\Experience::where('id', $userExtId)->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function putExperience(Request $request, User $user)
    {
        $userExtId = $user->externalIds->first()->body;
        $inputOrganization = $request->input('organization');
        $inputStartDate = $request->input('start_date');

        $found = $this->findExperience([
            'id' => $user->externalIds->first(),
            'organization' => $inputOrganization,
            'start_date' => $request->input('start_date'),
        ]);

        $experience = $found ?: new Shared\Experience;
        $experience->id = $userExtId;
        $experience->organization = $inputOrganization;
        $experience->department = $request->input('department') ?: $experience->department;
        $experience->position = $request->input('position') ?: $experience->position;
        $experience->start_date = $inputStartDate;
        $experience->end_date = $request->input('end_date') ?: $experience->end_date;
        $experience->description = $request->input('description') ?: $experience->description;
        $experience->save();

        return $experience;
    }

    /**
     * Delete the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function deleteExperience(Request $request, User $user)
    {
        $id = $user->externalIds->first()->body;
        $organization = $request->input('organization');
        $start_date = $request->input('start_date');

        $deleted = DB::connection(config('database.shared'))->table('experience')
            ->where('id', $id)
            ->where('organization', $organization)
            ->where('start_date', $start_date)
            ->delete();

        if ($deleted) {
            return response('OK', 200);
        } else {
            return response('Experience Not Found', 404);
        }
    }

    public function phoneQuery(Request $request)
    {
        $name = $request->input('name');
        $results = Shared\Contact::where('name', $name)->get();

        $contacts = $results->map(function ($item) {
            return [
                "id" => $item->id,
                "name" => $item->name,
                "phone_mobile" => $item->phone_mobile,
            ];
        });
        return $contacts;
    }
}
