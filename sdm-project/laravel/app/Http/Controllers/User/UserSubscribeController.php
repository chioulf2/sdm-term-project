<?php

namespace App\Http\Controllers\User;

use App\Entities\User;
use App\Entities\UserSubscribe;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

class UserSubscribeController extends Controller
{
    /**
     * Find the tag with the provided composite key in storage.
     *
     * @param  array  $composite
     * @return \App\Entities\UserSubscribe
     */
    private function findSubscribe($composite)
    {
        return UserSubscribe::where('user_id', $composite['user_id'])
            ->where('classification_id', $composite['classification_id'])
            ->first();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $userId = $user->id;
        return UserSubscribe::where('user_id', $userId)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $userId = $user->id;
        $classificationId = $request->input('classification_id') ?: 0;

        $found = $this->findSubscribe([
            'user_id' => $userId,
            'classification_id' => $classificationId,
        ]);

        $userSubscribe = $found ?: new UserSubscribe;
        if (!$found) {
            $userSubscribe->user_id = $userId;
            $userSubscribe->classification_id = $classificationId;
            $userSubscribe->post_count = 0;
            $userSubscribe->save();
        }
        return UserSubscribe::find($userSubscribe->id);
    }

    /**
     * Delete the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        $userId = $user->id;
        $classificationId = $request->input('classification_id');

        $deleted = UserSubscribe::where('user_id', $userId)
                ->where('classification_id', $classificationId)
                ->delete();

        if ($deleted) {
            return response('OK', 200);
        } else {
            return response('Tag Not Found', 404);
        }
    }
}
