<?php

namespace App\Http\Controllers\User;

use App\Entities\Shared as Shared;
use App\Entities\User;
use App\Entities\UserTag;
use App\Entities\ExternalId;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all()->map(function ($u) {
            // May be very slow, need to be improved.
            $userExtId = $u->externalIds->first()->body;
            $latestEdu = Shared\Education::where('id', $userExtId)
                ->orderBy('start_date', 'desc')
                ->first();
            $latestEdu = $latestEdu ? $latestEdu->toArray() : [];

            $u = $u->toArray();
            unset($u['type_id']);

            return array_merge($u, $latestEdu);
        });

        return response()->json($users);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $userExtId = $user->externalIds->first()->body;
        $contact = Shared\Contact::where('id', $userExtId)->first();

        $user = $user->toArray();
        unset($user['type_id']);
        $contact = $contact ? $contact->toArray() : [];

        return response()->json(array_merge($user, $contact));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function saveBiography(Request $request, User $user)
    {
        $biography_content = $request->biography ?: $user->biography;
        $user->biography = $biography_content;
        $user->save();

        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\User  $user
     * @return string
     */
    public function getAvatar(User $user)
    {
        return $user->avatar;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function saveAvatar(Request $request, User $user)
    {
        $fileGot = $request->file('image');
        if (!$fileGot) {
            return response('Bad Request', 400);
        }
        if ($fileGot->isValid()) {
            $data = File::get($fileGot);
            $dataUri = 'data:' . $fileGot->getMimeType() . ';base64,' . base64_encode($data);
            $user->avatar = $dataUri;
            $user->save();

            return $user->avatar;
        } else {
            return response('Invalid File', 400);
        }
    }

    /**
     * Reset the specified resource to default value.
     *
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function resetAvatar(User $user)
    {
        $identicon = new \Identicon\Identicon();
        $user->avatar = $identicon->getImageDataUri($user->hash, 256);
        $user->save();

        return response('OK', 200);
    }

    /**
     * Search against the resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $tags = $request->input('tags') ? explode(',', $request->input('tags')) : [];
        $usersOfTags = collect($tags)->map(function ($t) use ($request) {
            return UserTag::where('owner_user_id', $request->user()->id)
                ->where('tag', $t)->get()->pluck('target_user_id');
        })->toArray();

        $tgt = array_reduce($usersOfTags, function ($carry, $item) {
            return array_merge($carry, $item);
        }, []);

        return User::whereIn('id', $tgt)->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request)
    {
        $prefix = strtolower($request->input('prefix'));
        $found = Shared\Contact::where('name', 'LIKE', $prefix . '%')->get();
        $result = $found->map(function ($c) {
            $uid = ExternalId::where('body', $c->id)->first()->user_id;
            return [
                'name' => $c->name,
                'key' => User::find($uid)->key,
            ];
        })->toArray();

        return $result;
    }
}
