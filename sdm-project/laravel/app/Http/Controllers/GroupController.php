<?php

namespace App\Http\Controllers;

use App\Entities\Group;
use App\Entities\GroupUserRoleType;
use App\Entities\User;
use App\Entities\TextCategoryName;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a list of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Group::visibleBy($request->user())->get();
    }

    /**
     * Display a list of the resource.
     *
     * @param  \App\Entities\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function postIndex(Group $group)
    {
        $this->authorize($group);

        return Text::ofCateg('level', 0)->ofGroup($group->key)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->input('name');
        $creator = $request->user();
        $desc = $request->input('description');
        $private = $request->input('private');

        $group = Group::makeAndSave($name, $creator, $desc, $private);

        return $group ? Group::find($group->id) : response('The Group Already Exists', 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        $this->authorize($group);

        return $group;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $this->authorize($group);

        $group->description = $request->input('description') ?: $group->description;
        $private = $request->input('private') !== null ? $request->input('private') : $group->private;
        $group->private = $private ? '1' : '0';
        $group->save();

        return $group;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        $this->authorize($group);

        $group->delete();

        return response('OK', 200);
    }

    /**
     * Add the given user into this group.
     *
     * @param  \App\Entities\Group  $group
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function addMember(Group $group, User $user)
    {
        $this->authorize($group);

        if ($group->addMember($user)) {
            return response('OK', 200);
        } else {
            return response('Not Modified', 304);
        }
    }

    /**
     * Add the given user into this group as an admin.
     *
     * @param  \App\Entities\Group  $group
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function addAdmin(Group $group, User $user)
    {
        $this->authorize($group);

        if ($group->addAdmin($user)) {
            return response('OK', 200);
        } else {
            return response('Not Modified', 304);
        }
    }

    /**
     * Remove the given user from this group.
     *
     * @param  \App\Entities\Group  $group
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Http\Response
     */
    public function removeMember(Group $group, User $user)
    {
        $this->authorize($group);

        if ($group->removeMember($user)) {
            return response('OK', 200);
        }
        return response('User Not In Group', 404);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request)
    {
        $prefix = $request->input('prefix');
        $parentsIds = Group::visibleBy($request->user())->get()->pluck('category_id');
        $found = TextCategoryName::whereIn('id', $parentsIds)->
            where('name', 'LIKE', $prefix . '%')->get();
        $result = $found->map(function ($g) {
            return [
                'name' => $g->name,
                'key' => Group::where('category_id', $g->id)->first()->key,
            ];
        })->toArray();

        return $result;
    }
}
