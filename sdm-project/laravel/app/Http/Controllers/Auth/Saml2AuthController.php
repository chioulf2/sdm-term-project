<?php

namespace App\Http\Controllers\Auth;

use App\Entities\Group;
use App\Entities\User;
use App\Entities\ExternalId;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use OneLogin_Saml2_Auth;
use OneLogin_Saml2_Error;

class Saml2AuthController extends Controller
{
    /**
     * API provider of `php-saml`, the SAML package we use.
     *
     * @var OneLogin_Saml2_Auth
     */
    private $saml2Auth;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('land', ['only' => ['emitLogin', 'handleLoginResult']]);

        $saml2Settings = $this->prepareSettings(config('saml2'));
        $this->saml2Auth = new OneLogin_Saml2_Auth($saml2Settings);
    }

    /**
     * Create a new authentication controller instance.
     *
     * @param  array  $settings
     * @return void
     */
    private function prepareSettings($settings)
    {
        // TODO Fix this dummy code.
        $spEntityId = &$settings['sp']['entityId'];
        $spEntityId = starts_with($spEntityId, 'http://') ?: url($spEntityId);
        $spAcsUrl = &$settings['sp']['assertionConsumerService']['url'];
        $spAcsUrl = starts_with($spAcsUrl, 'http://') ?: url($spAcsUrl);
        $spSlsUrl = &$settings['sp']['singleLogoutService']['url'];
        $spSlsUrl = starts_with($spSlsUrl, 'http://') ?: url($spSlsUrl);

        return $settings;
    }

    /**
     * Emit a login request to the IdP according to the settings.
     *
     * @return mixed
     */
    public function emitLogin()
    {
        $this->saml2Auth->login();
        // previous function call exits the script already

        return '';
    }

    /**
     * Emit a logout request to the IdP according to the settings.
     *
     * @return mixed
     */
    public function emitLogout()
    {
        $this->saml2Auth->logout();
        // previous function call exits the script already

        return '';
    }

    /**
     * Provide the metadata of this app (SP).
     *
     * @return mixed
     */
    public function getMetadata()
    {
        $settings = $this->saml2Auth->getSettings();
        $metadata = $settings->getSPMetadata();
        $errors = $settings->validateMetadata($metadata);

        if (empty($errors)) {
            return response($metadata)
                        ->header('Content-Type', 'text/xml');
        } else {
            throw new OneLogin_Saml2_Error(
                "Invalid SP metadata: " . implode(', ', $errors),
                OneLogin_Saml2_Error::METADATA_SP_INVALID
            );
        }
    }

    /**
     * Handle the login result responded by the IdP.
     *
     * @return mixed
     */
    public function handleLoginResult()
    {
        $this->saml2Auth->processResponse();
        $errors = $this->saml2Auth->getErrors();

        if (!empty($errors)) {
            // TODO Fix this dummy code.
            return response('<p>' . implode(', ', $errors) . '</p>');
        }
        if (!$this->saml2Auth->isAuthenticated()) {
            // TODO Fix this dummy code.
            return response('<p>Not authenticated.</p>');
        }

        $attributes = $this->saml2Auth->getAttributes();
        $ssoUid = $attributes['uid'][0];
        $userId = $this->findOrCreateUser($ssoUid);
        Auth::loginUsingId($userId);

        return redirect('/');
    }

    /**
     * Handle the logout result responded by the IdP.
     *
     * @return mixed
     */
    public function handleLogoutResult()
    {
        $this->saml2Auth->processSLO();
        $errors = $this->saml2Auth->getErrors();

        // we logout the user on our side anyway
        Auth::logout();

        if (!empty($errors)) {
            // TODO Fix this dummy code.
            return response('<p>' . implode(', ', $errors) . '</p>');
        } else {
            // TODO Fix this dummy code.
            return redirect('/');
        }
    }

    /**
     * Find or create a user with the give SSO UID.
     *
     * @param  string  $ssoUid
     * @return int
     */
    private function findOrCreateUser($ssoUid)
    {
        $user = User::havingExternalId($ssoUid)->first();

        if (!$user) {
            $user = User::makeAndSave();
            $externalId = ExternalId::make($ssoUid);
            $user->externalIds()->save($externalId);
            $defaultGroup = Group::find(0);
            $defaultGroup->addMember($user);
        }

        return $user->id;
    }
}
