<?php

namespace App\Http\Controllers;

use App\Entities\Text;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReplyReplyController extends ReplyController
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\Text  $text
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Text $reply, $level = null)
    {
        return parent::store($request, $reply, 2);
    }
}
