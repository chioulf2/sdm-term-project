<?php

namespace App\Http\Controllers;

use App\Entities\Text;
use Illuminate\Http\Request;

class PostReplyController extends ReplyController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\Text  $text
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Text $reply, $level = null)
    {
        return parent::store($request, $reply, 1);
    }
}
