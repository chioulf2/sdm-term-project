<?php

namespace App\Http\Controllers;

use App\Entities\Group;
use App\Entities\Text;
use App\Entities\TextCategoryType;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Jobs\SendSubscribeNotificationEmail;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Text::ofCateg('level', 0)->postVisibleBy($request->user())->get();
    }

    /**
     * Display a listing of the resource by classification_id.
     *
     * @return \Illuminate\Http\Response
     */
    public function getByCategory($classification_id)
    {
        $levels = TextCategoryType::where('name', 'level')->first()->categories;
        $tgt = $levels->where('name', '0')->first()->texts
            ->where('classification_id', $classification_id)->pluck('id');

        return Text::whereIn('id', $tgt)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user();

        $groupkey = $request->input('groupkey');
        if ($groupkey) {
            $group = Group::where('key', $groupkey)->first();
            if (!$group) {
                return response('Group Not Found', 404);
            } elseif (!$group->hasMember($user)) {
                return response('This action is unauthorized.', 403);
            }
        } else {
            $group = null;
        }

        $classification_id = $request->input('classification_id') ?: 0;

        $post = Text::makeAndSave($request->input('body'), $request->user(), 0, $classification_id, $group);

        // Send suscribe email.
        $this->dispatch(new SendSubscribeNotificationEmail($classification_id));

        return Text::find($post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\Text  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Text $post)
    {
        $this->authorize($post);

        return $post;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\Text  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Text $post)
    {
        $this->authorize($post);

        $post->body = $request->input('body');
        $post->classification_id = $request->input('classification_id') ?: $post->classification_id;
        $post->save();

        return $post;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\Text  $text
     * @return \Illuminate\Http\Response
     */
    public function destroy(Text $post)
    {
        $this->authorize($post);

        $post->delete();

        return response('OK', 200);
    }

    /**
     * Search against the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $visibleTexts = Text::postVisibleBy($request->user())->
            ofCateg('level', 0)->get()->pluck('id');

        $tags = $request->input('tags') ? explode(',', $request->input('tags')) : [];
        $users = $request->input('users') ? explode(',', $request->input('users')) : [];
        $groups = $request->input('groups') ? explode(',', $request->input('groups')) : [];

        $textsUnderTags = collect($tags)->map(function ($t) use ($visibleTexts) {
            return Text::ofCateg('tag', $t)
                ->whereIn('id', $visibleTexts)->get()->pluck('id');
        })->toArray();
        $textsOfUsers = collect($users)->map(function ($ukey) use ($visibleTexts) {
            $found = User::where('key', $ukey)->first();
            if ($found) {
                return Text::where('user_id', $found->id)
                    ->whereIn('id', $visibleTexts)->get()->pluck('id');
            }
            return [];
        })->toArray();
        $textsInGroups = Collect($groups)->map(function ($gkey) use ($visibleTexts) {
            return Text::ofGroup($gkey)->whereIn('id', $visibleTexts)
                ->get()->pluck('id');
        })->toArray();

        $f = function ($carry, $item) {
            return array_merge($carry, $item);
        };

        $dontCare = array_reduce(array_merge($textsUnderTags, $textsOfUsers, $textsInGroups), $f, []);
        $textsUnderTags = $tags ? array_reduce($textsUnderTags, $f, []) : $dontCare;
        $textsOfUsers = $users ? array_reduce($textsOfUsers, $f, []) : $dontCare;
        $textsInGroups = $groups ? array_reduce($textsInGroups, $f, []) : $dontCare;
        $array = array_intersect($textsUnderTags, $textsOfUsers, $textsInGroups);

        return Text::whereIn('id', array_unique($array))->get();
    }
}
