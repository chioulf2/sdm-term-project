<?php

namespace App\Http\Controllers;

use App\Entities\Text;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Entities\Text  $text
     * @return \Illuminate\Http\Response
     */
    public function index(Text $text)
    {
        return $text->replies;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\Text  $text
     * @param  int  $level
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Text $text, $level = null)
    {
        $reply = Text::makeAndSave($request->input('body'), $request->user(), $level);
        $reply->parent_id = $text->id;
        $reply->save();

        return Text::find($reply->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\Text  $text
     * @param  \App\Entities\Text  $reply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Text $reply)
    {
        $reply->body = $request->input('body');
        $reply->save();

        return $reply;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\Text  $text
     * @param  \App\Entities\Text  $reply
     * @return \Illuminate\Http\Response
     */
    public function destroy(Text $reply)
    {
        $reply->delete();

        return response('OK', 200);
    }
}
