<?php

namespace App\Http\Controllers;

use App\Entities\Text;
use App\Entities\TextCategoryName;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PostTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Entities\Text  $post
     * @return \Illuminate\Http\Response
     */
    public function index(Text $post)
    {
        return $post->tags;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Entities\Text  $post
     * @param  string $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Text $post, $tag)
    {
        if (!$tag) {
            return response('Post Tag Not Provided', 404);
        } else {
            $tag = strtolower($tag); // TODO: Make it normalized.

            $already = !Text::OfCateg('tag', $tag)->where('key', $post->key)->get()->isEmpty();
            if ($already) {
                return response('OK', 200);
            }

            $tag = $this->tagExist($tag) ?: TextCategoryName::makeAndSave($tag, 'tag');
            $post->classes()->attach($tag);

            return $tag->name;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\Text  $post
     * @param  string $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Text $post, $tag)
    {
        if (!$tag) {
            return response('Post Tag Not Provided', 404);
        } else {
            $tag = strtolower($tag); // TODO: Make it normalized.

            return $this->tagExist($tag)->delete() ?
                response('OK', 200) : response('Post Tag Not Found', 404);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request)
    {
        $prefix = strtolower($request->input('prefix'));
        $result = TextCategoryName::OfType('tag')->where('name', 'LIKE', $prefix . '%')->get();

        return $result->pluck('name');
    }

    /**
     * Check if a tag exists.
     *
     * @param  string  $tag
     * @return \App\Entities\TextCategoryName
     */
    public function tagExist($tag)
    {
        return TextCategoryName::OfType('tag')->where('name', $tag)->first();
    }
}
