<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$sendClient = function () {
    $user = Auth::user();
    $user_key = $user ? $user->key : null;
    $landing = $user ? $user->baiscInfosUpToDate() : null;
    return view('client', ['user_key' => $user_key, 'landing' => $landing]);
};

Route::get('/', $sendClient);
Route::get('/login', $sendClient);
Route::get('/profile', $sendClient);
Route::get('/post/{any}', $sendClient);
Route::get('/alumni', $sendClient);
Route::get('/alumni/{any}', $sendClient);
Route::get('/analytics', $sendClient);

// Authentication Routes
Route::group(['prefix' => 'auth/saml2', 'namespace' => 'Auth'], function () {

    Route::get('login', 'Saml2AuthController@emitLogin');
    Route::get('logout', 'Saml2AuthController@emitLogout');
    Route::get('metadata', 'Saml2AuthController@getMetadata');
    Route::post('acs', 'Saml2AuthController@handleLoginResult');
    Route::get('sls', 'Saml2AuthController@handleLogoutResult');

});

Route::get('api/phone_query', 'User\ProfileController@phoneQuery');
Route::post('api/phone_query', 'User\ProfileController@phoneQuery');

// Login-required Routes
Route::group(['middleware' => 'auth.saml2'], function () {

    // Main API Routes of the App
    Route::group(['prefix' => 'api'], function () {

        // User-related Routes
        Route::group(['prefix' => 'user', 'namespace' => 'User'], function () {
            Route::get('/', 'UserController@index');
            Route::get('search', 'UserController@search');
            Route::get('tagcomplete', 'UserTagController@autocomplete');
            Route::get('namecomplete', 'UserController@autocomplete');
            Route::group(['prefix' => '{user}'], function () {

                Route::get('/', 'UserController@show');
                //biography
                Route::put('biography', 'UserController@saveBiography');
                // avatar
                Route::get('avatar', 'UserController@getAvatar');
                Route::post('avatar', 'UserController@saveAvatar');
                Route::delete('avatar', 'UserController@resetAvatar');
                // tag
                Route::resource('tag', 'UserTagController',
                                ['only' => ['index', 'update', 'destroy']]);
                // subscribe
                Route::get('subscribe', 'UserSubscribeController@index');
                Route::put('subscribe', 'UserSubscribeController@store');
                Route::put('subscribe/delete', 'UserSubscribeController@destroy');
                // profile
                Route::group(['prefix' => 'profile'], function () {
                    Route::get('/', 'ProfileController@show');
                    Route::get('contact', 'ProfileController@getContact');
                    Route::put('contact', 'ProfileController@putContact');
                    Route::get('education', 'ProfileController@getEducations');
                    Route::put('education', 'ProfileController@putEducation');
                    Route::put('education/delete', 'ProfileController@deleteEducation');
                    Route::get('experience', 'ProfileController@getExperiences');
                    Route::put('experience', 'ProfileController@putExperience');
                    Route::put('experience/delete', 'ProfileController@deleteExperience');
                });

            });
        });

        // Post-related Routes
        Route::get('post/search', 'PostController@search');
        Route::get('post/tagcomplete', 'PostTagController@autocomplete');
        Route::get('post/category/{category_id}', 'PostController@getByCategory');
        Route::resource('post', 'PostController',
                        ['except' => ['create', 'edit']]);
        Route::resource('post.reply', 'PostReplyController',
                        ['only' => ['store', 'index']]);
        Route::resource('post.tag', 'PostTagController',
                        ['only' => ['index', 'update', 'destroy']]);

        // Reply-related Routes
        Route::group(['prefix' => 'reply/{reply}'], function () {
            Route::get('reply', 'ReplyReplyController@index');
            Route::post('reply', 'ReplyReplyController@store');
            Route::put('/', 'ReplyReplyController@update');
            Route::delete('/', 'ReplyReplyController@destroy');
        });

        // Group-related Routes
        Route::group(['prefix' => 'group/{group}'], function () {
            Route::put('member/{user}', 'GroupController@addMember');
            Route::put('admin/{user}', 'GroupController@addAdmin');
            Route::delete('member/{user}', 'GroupController@removeMember');
        });
        Route::get('group/namecomplete', 'GroupController@autocomplete');
        Route::get('group/{group}/post}', 'GroupController@postIndex');
        Route::resource('group', 'GroupController',
                        ['except' => ['create', 'edit']]);

    });

    // Landing Page Placeholder
    Route::get('landed', function () {
        // TODO Fix the dummy code.
        $user = Auth::user();
        $msg  = "<img style=\"border:1px solid\" src=\"$user->avatar\">\n"
                . "<p>User '{$user->externalIds->first()->body}' with "
                . "key '$user->key' has been logged in.</p>"
                . "<p>The csrf_token of the session is '" . csrf_token()
                . "'.</p>";
        return $msg;
    });

    Route::get('avatar_upload_test', function () {
        return view('laravel.resources.views.avatar_upload_test');
    });

});
