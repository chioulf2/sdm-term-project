<?php

namespace App\Providers;

use App\Entities\Group;
use App\Entities\Text;
use App\Entities\User;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        parent::boot($router);

        $router->bind('user', function ($value) {
            $user = User::where('key', $value)->first();
            return $user ?: abort(404, 'User Not Found');
        });

        $router->bind('post', function ($value) {
            $post = Text::where('key', $value)->first();
            return $post && $post->level === '0' ?
                        $post : abort(404, 'Post Not Found');
        });

        $router->bind('reply', function ($value) {
            $reply = Text::where('key', $value)->first();
            return $reply && ($reply->level === '1' || $reply->level === '2') ?
                        $reply : abort(404, 'Reply Not Found');
        });

        $router->bind('group', function ($value) {
            $group = Group::where('key', $value)->first();
            return $group ?: abort(404, 'Group Not Found');
        });

    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
