<?php

namespace App\Policies;

use App\Entities\Group;
use App\Entities\Text;
use App\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TextPolicy
{
    use HandlesAuthorization;

    /**
     * Create a Post policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given post can be stored by the user.
     *
     * @param  \App\Entities\User  $user
     * @param  \App\Entities\Text  $post
     * @return bool
     */
    public function store(User $user, Text $post)
    {
        return (bool) $post->group->hasMember($user);
    }

    /**
     * Determine if the given post can be showed to the user.
     *
     * @param  \App\Entities\User  $user
     * @param  \App\Entities\Text  $post
     * @return bool
     */
    public function show(User $user, Text $post)
    {
        return $post->group->hasMember($user) || !$post->group->private;
    }

    /**
     * Determine if the given post can be updated by the user.
     *
     * @param  \App\Entities\User  $user
     * @param  \App\Entities\Text  $post
     * @return bool
     */
    public function update(User $user, Text $post)
    {
        return $post->group->hasMember($user) && $post->owner->key === $user->key;
    }

    /**
     * Determine if the given post can be destroyed by the user.
     *
     * @param  \App\Entities\User  $user
     * @param  \App\Entities\Text  $post
     * @return bool
     */
    public function destroy(User $user, Text $post)
    {
        return $post->group->hasMember($user) && $post->owner->key === $user->key;
    }
}
