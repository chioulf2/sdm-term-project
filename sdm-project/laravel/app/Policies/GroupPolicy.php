<?php

namespace App\Policies;

use App\Entities\Group;
use App\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given group can be showed to the user.
     *
     * @param  \App\Entities\User  $user
     * @param  \App\Entities\Group  $group
     * @return bool
     */
    public function show(User $user, Group $group)
    {
        return $group->hasMember($user) || !$group->private;
    }

    /**
     * Determine if the given group settings can be updated by the user.
     *
     * @param  \App\Entities\User  $user
     * @param  \App\Entities\Group  $group
     * @return bool
     */
    public function update(User $user, Group $group)
    {
        return (bool) $group->hasAdmin($user);
    }

    /**
     * Determine if the given group can be destroyed by the user.
     *
     * @param  \App\Entities\User  $user
     * @param  \App\Entities\Group  $group
     * @return bool
     */
    public function destroy(User $user, Group $group)
    {
        return (bool) $group->hasAdmin($user);
    }

    /**
     * Determine if the user can add an member in the given group.
     *
     * @param  \App\Entities\User  $user
     * @param  \App\Entities\Group  $group
     * @return bool
     */
    public function addMember(User $user, Group $group)
    {
        return (bool) $group->hasAdmin($user);
    }

    /**
     * Determine if the user can add an admin in the given group.
     *
     * @param  \App\Entities\User  $user
     * @param  \App\Entities\Group  $group
     * @return bool
     */
    public function addAdmin(User $user, Group $group)
    {
        return (bool) $group->hasAdmin($user);
    }

    /**
     * Determine if the user can remove a member in the given group.
     *
     * @param  \App\Entities\User  $user
     * @param  \App\Entities\Group  $group
     * @return bool
     */
    public function removeMember(User $user, Group $group)
    {
        return (bool) $group->hasAdmin($user);
    }
}
