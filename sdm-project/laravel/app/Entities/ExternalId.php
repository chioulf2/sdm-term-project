<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ExternalId extends Model
{
    /**
     * Get the owner of this ID.
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get the type of this ID.
     */
    public function type()
    {
        return $this->belongsTo(ExternalIdType::class);
    }

    /**
     * Make a plain ExternalId entity.
     *
     * @param  string  $body
     * @param  string  $type  (optional, default is according to the config)
     * @return self
     */
    public static function make($body, $type = null)
    {
        $id = new self;
        $id->body = $body;
        $type = $type ?: config('auth.idtype');
        $type = ExternalIdType::where('name', $type)->first();
        $id->type()->associate($type);

        return $id;
    }

    /**
     * Create a ExternalId in the database.
     *
     * @param  string  $body
     * @param  string  $type  (optional, default is according to the config)
     * @return self
     */
    public static function makeAndSave($body, $type = null)
    {
        $id = self::make($body, $type);
        $id->save();

        return $id;
    }
}
