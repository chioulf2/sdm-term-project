<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class GroupUserRole extends Model
{
    /**
     * Get the corresponding user of this role.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the corresponding group of this role.
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    /**
     * Get the type of this role.
     */
    public function type()
    {
        return $this->belongsTo(GroupUserRoleType::class);
    }

    /**
     * Scope a query to only include the roles of a given type.
     *
     * @param  string  $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfType($query, $type)
    {
        $type = GroupUserRoleType::where('name', $type)->first();
        $tgt = $type ? $type->memberships->pluck('id') : [];

        return $query->whereIn('id', $tgt);
    }
}
