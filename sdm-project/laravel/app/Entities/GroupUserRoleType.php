<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class GroupUserRoleType extends Model
{
    /**
     * Get all the group memberships that belongs to this role type.
     */
    public function memberships()
    {
        return $this->hasMany(GroupUserRole::class, 'type_id');
    }
}
