<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'created_at',
        'updated_at',
        'key',
        'body',
        'classification_id',
        'ownerkey',
        'parentkey',
        'level',
        'groupkey',
        'tags',
        'replies',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'ownerkey',
        'parentkey',
        'level',
        'group',
        'groupkey',
        'tags',
        'replies',
    ];

    /**
     * Get the owner of this text.
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get the parent subject of this text.
     */
    public function parent()
    {
        return $this->belongsTo(get_class($this), 'parent_id');
    }

    /**
     * Get all the categories that this text has been assigned to.
     */
    public function classes()
    {
        return $this->belongsToMany(TextCategoryName::class, 'text_classifications', 'text_id', 'category_id');
    }

    /**
     * Get all the replies of this text.
     */
    public function replies()
    {
        return $this->hasMany(get_class($this), 'parent_id');
    }

    /**
     * Get the owner of this text.
     *
     * @return bool
     */
    public function getOwnerkeyAttribute()
    {
        return User::find($this->user_id)->key;
    }

    /**
     * Get the parent key of this text.
     *
     * @return bool
     */
    public function getParentkeyAttribute()
    {
        return $this->parent->key;
    }

    /**
     * Get the level class of this text.
     *
     * @return bool
     */
    public function getLevelAttribute()
    {
        $levels = TextCategoryName::ofType('level')->get()->pluck('id');
        $classification = TextClassification::where('text_id', $this->id)
            ->whereIn('category_id', $levels)->first();
        $level = $classification->category->name;

        return $level;
    }

    /**
     * Get the group of this text.
     *
     * @return bool
     */
    public function getGroupAttribute()
    {
        $id = [$this->id, $this->parent->id, $this->parent->parent->id][$this->level];
        $groups = TextCategoryName::ofType('group')->get()->pluck('id');
        $classification = TextClassification::where('text_id', $id)
            ->whereIn('category_id', $groups)->first();

        return Group::where('category_id', $classification->category->id)->first();
    }

    /**
     * Get the group key of this text.
     *
     * @return bool
     */
    public function getGroupkeyAttribute()
    {
        return $this->group ? $this->group->key : null;
    }

    /**
     * Get the tag classes of this text.
     *
     * @return bool
     */
    public function getTagsAttribute()
    {
        $allTags = TextCategoryName::ofType('tag')->get()->pluck('id');
        $classifications = TextClassification::where('text_id', $this->id)
            ->whereIn('category_id', $allTags)->get();
        $tags = $classifications->map(function ($c) {
            return $c->category->name;
        });

        return $tags;
    }

    /**
     * Append the replies of this text into the attributes.
     *
     * @return bool
     */
    public function getRepliesAttribute()
    {
        return $this->replies()->get();
    }

    /**
     * Make a plain Text entity.
     *
     * @param  \App\Entities\User  $owner
     * @param  string  $body
     * @return self
     */
    public static function make($body, $classification_id = 0, $owner = null)
    {
        $text = new self;
        $text->key = str_random(config('app.internal_key_length'));
        $text->body = $body;
        $text->classification_id = $classification_id;
        $owner ? $text->owner()->associate($owner) : null;

        return $text;
    }

    /**
     * Create a Text in the database.
     *
     * @param  string  $body
     * @param  \App\Entities\User  $owner
     * @param  int  $level  (optional, default is 0)
     * @param  \App\Entities\Group  $group  (optional)
     * @param  int  $classification_id  (optional, default is 0)
     * @return self
     */
    public static function makeAndSave($body, $owner, $level = 0, $classification_id = 0, $group = null)
    {
        $text = self::make($body, $classification_id, $owner);
        $text->save();

        // Set the text to be a member of the class of the given level.
        $class = TextCategoryName::ofType('level')
            ->where('name', (string) $level)->first();
        $text->classes()->attach($class);

        // Set the text to be a member of the default group.
        $group = $group ?: Group::find(0);
        $text->classes()->attach($group->parentTableRecord);

        return $text;
    }

    /**
     * Scope a query to only include the texts of the given quest.
     *
     * @param  int  $level
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfCateg($query, $type, $quest)
    {
        $categ = TextCategoryName::OfType($type)->where('name', (string) $quest)->first();
        $tgt = $categ ? $categ->texts->pluck('id') : [];

        return $query->whereIn('id', $tgt);
    }

    /**
     * Scope a query to only include the texts of a given group (key).
     *
     * @param  string  $key
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfGroup($query, $key)
    {
        $group = Group::where('key', $key)->first();
        $tgt = $group ? $group->parentTableRecord->texts->pluck('id') : [];

        return $query->whereIn('id', $tgt);
    }

    /**
     * Scope a query to only include the posts visible to a given user.
     *
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePostVisibleBy($query, $user)
    {
        $textsFromUserGroups = $user->groups->map(function ($g) {
            return self::ofGroup($g->key)->get()->pluck('id');
        })->toArray();

        $textsFromPublicGroups = Group::bePublic()->get()->map(function ($g) {
            return self::ofGroup($g->key)->get()->pluck('id');
        })->toArray();

        $array = array_merge($textsFromUserGroups, $textsFromPublicGroups);

        $tgt = array_reduce($array, function ($carry, $item) {
            return array_merge($carry, $item);
        }, []);

        return $query->whereIn('id', array_unique($tgt));
    }
}
