<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ExternalIdType extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get all the external IDs of this type.
     */
    public function idsOfThisType()
    {
        return $this->hasMany(ExternalId::class, 'type_id');
    }
}
