<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class TextCategoryName extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the type of this category.
     */
    public function type()
    {
        return $this->belongsTo(TextCategoryType::class);
    }

    /**
     * Get all the texts under this category.
     */
    public function texts()
    {
        return $this->belongsToMany(Text::class, 'text_classifications', 'category_id', 'text_id');
    }

    /**
     * Make a plain TextCategoryName entity.
     *
     * @param  string  $name
     * @param  string  $type
     * @return self
     */
    public static function make($name, $type)
    {
        $category = new self;
        $category->name = $name;
        $type = TextCategoryType::where('name', $type)->first();
        $category->type()->associate($type);

        return $category;
    }

    /**
     * Create a TextCategoryName in the database.
     *
     * @param  string  $name
     * @param  string  $type
     * @return self
     */
    public static function makeAndSave($name, $type)
    {
        $category = self::make($name, $type);
        $category->save();

        return $category;
    }

    /**
     * Scope a query to only include categories of a given type.
     *
     * @param  string  $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfType($query, $type)
    {
        $tgt = TextCategoryType::where('name', $type)->first()->id;

        return $query->where('type_id', $tgt);
    }
}
