<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserSubscribe extends Model
{
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['updated_at', 'user_id', 'classification_id', 'post_count'];

    /**
     * Get user of this user subscribe.
     */
    public function userOfThisSubscribe()
    {
        $this->belongsTo(User::class);
    }

    public static function make($ownerId, $classification_id = 0, $post_count = 0)
    {
        $userSubscribe = new UserSubscribe;
        $userSubscribe->user_id = $ownerId;
        $userSubscribe->classification_id = 0;
        $userSubscribe->post_count = 0;

        return $userSubscribe;
    }

    public static function makeAndSave($ownerId, $classification_id = 0, $post_count = 0)
    {
        $userSubscribe = self::make($ownerId, $classification_id, $post_count);
        $userSubscribe->save();

        return $userSubscribe;
    }
}
