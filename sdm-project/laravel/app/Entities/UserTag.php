<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserTag extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the owner of this user tag.
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_user_id');
    }

    /**
     * Get the owner of this user tag.
     */
    public function target()
    {
        return $this->belongsTo(User::class, 'target_user_id');
    }

    /**
     * Make a UserTag entity.
     *
     * @param  \App\Entities\User  $owner
     * @param  \App\Entities\User  $user
     * @param  string  $tag
     * @return self
     */
    public static function make($owner, $target, $tag)
    {
        $record = new self;
        $record->owner()->associate($owner);
        $record->target()->associate($target);
        $record->tag = $tag;

        return $record;
    }

    /**
     * Create a UserTag record in the database.
     *
     * @param  \App\Entities\User  $owner
     * @param  \App\Entities\User  $user
     * @param  string  $tag
     * @return self
     */
    public static function makeAndSave($owner, $target, $tag)
    {
        $record = self::make($owner, $target, $tag);
        $record->save();

        return $record;
    }
}
