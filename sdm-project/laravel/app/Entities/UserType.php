<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['name'];

    /**
     * Get all the users of this user type.
     */
    public function usersOfThisType()
    {
        return $this->hasMany(User::class, 'type_id');
    }
}
