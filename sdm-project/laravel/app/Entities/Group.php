<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'created_at',
        'updated_at',
        'key',
        'name',
        'description',
        'private',
        'admins',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['name', 'admins'];

    /**
     * Get all the memberships of this group.
     */
    public function memberships()
    {
        return $this->hasMany(GroupUserRole::class);
    }

    /**
     * Get all the users in this group. (This has some bugs!!)
     */
    public function members()
    {
        return $this->belongsToMany(User::class, 'group_user_roles');
    }

    /**
     * Get all the users in this group.
     */
    public function adminMembers()
    {
        $tgt = GroupUserRole::OfType('admin')->get()->pluck('user_id');

        return $this->belongsToMany(User::class, 'group_user_roles')->whereIn('users.id', $tgt);
    }

    /**
     * Get the TextCategoryName record of this group.
     */
    public function parentTableRecord()
    {
        return $this->belongsTo(TextCategoryName::class, 'category_id');
    }

    /**
     * Get the name of this group.
     *
     * @return bool
     */
    public function getNameAttribute()
    {
        return $this->parentTableRecord->name;
    }

    /**
     * Get the admins' user key of this group.
     *
     * @return bool
     */
    public function getAdminsAttribute()
    {
        return $this->adminMembers->pluck('key');
    }

    /**
     * Make a plain Group entity.
     *
     * @param  \App\Entities\TextCategoryName  $category
     * @param  \App\Entities\User  $creater
     * @param  string  $desc  (optional)
     * @return self
     */
    public static function make($category, $desc = null, $private = false)
    {
        $group = new self;
        $group->key = str_random(config('app.internal_key_length'));
        $group->description = $desc ?: 'The description of the group.';
        $group->private = $private ? 1 : 0;
        $group->parentTableRecord()->associate($category);

        return $group;
    }

    /**
     * Create a Group record in the database.
     *
     * @param  string  $name
     * @param  \App\Entities\User  $creater
     * @param  string  $desc  (optional)
     * @param  bool  $private  (optional)
     * @return self
     */
    public static function makeAndSave($name, $creator, $desc = null, $private = false)
    {
        $nameUsed = TextCategoryName::OfType('group')->where('name', $name)->first();
        if ($nameUsed) {
            return null;  // Creation failed.
        }

        $category = TextCategoryName::makeAndSave($name, 'group');
        $group = self::make($category, $desc, $private);
        $group->save();

        // Add creator as the first admin of the Group
        $asAdmin = GroupUserRoleType::where('name', 'admin')->first();
        $group->members()->attach($creator, ['type_id' => $asAdmin->id]);

        return $group;
    }

    /**
     * Check that the given user is in this group.
     *
     * @param  \App\Entities\User  $user
     * @return \App\Entities\User (if found)
     */
    public function hasMember(User $user)
    {
        $found = GroupUserRole::where('group_id', $this->id)
            ->where('user_id', $user->id)->first();
        return $found;
    }

    /**
     * Check that the given user is in this group and is an admin.
     *
     * @param  \App\Entities\User  $user
     * @return \App\Entities\User (if found)
     */
    public function hasAdmin(User $user)
    {
        $asAdmin = GroupUserRoleType::where('name', 'admin')->first();
        $found = GroupUserRole::where('group_id', $this->id)
            ->where('user_id', $user->id)->where('type_id', $asAdmin->id)->first();
        return $found;
    }

    /**
     * Add the given user into this group.
     *
     * @param  \App\Entities\User  $user
     * @return bool
     */
    public function addMember(User $user)
    {
        if ($this->hasAdmin($user)) {
            $this->members()->detach($user);
        }
        if ($this->hasMember($user)) {
            return true;
        }
        $asRegular = GroupUserRoleType::where('name', 'regular')->first();
        $this->members()->attach($user, ['type_id' => $asRegular->id]);

        return true;
    }

    /**
     * Add the given user to this group as an admin.
     *
     * @param  \App\Entities\User  $user
     * @return bool
     */
    public function addAdmin(User $user)
    {
        if ($this->hasMember($user)) {
            $this->members()->detach($user);
        }
        $asAdmin = GroupUserRoleType::where('name', 'admin')->first();
        $this->members()->attach($user, ['type_id' => $asAdmin->id]);

        return true;
    }

    /**
     * Remove the given user from this group.
     *
     * @param  \App\Entities\User  $user
     * @return bool
     */
    public function removeMember(User $user)
    {
        if ($this->hasMember($user)) {
            $this->members()->detach($user);
            return true;
        }
        return false;
    }

    /**
     * Scope a query to only include the groups that are public.
     *
     * @param  string  $key
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeBePublic($query) {
        $tgt = self::where('private', '0')->get()->pluck('id');

        return $query->whereIn('id', $tgt);
    }

    /**
     * Scope a query to only include the groups visible to the given user.
     *
     * @param  \App\Entities\User  $user
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVisibleBy($query, $user)
    {
        $groupsUserIn = $user->groups->pluck('id')->toArray();
        $publicGroup = self::bePublic()->get()->pluck('id')->toArray();
        $tgt = array_merge($groupsUserIn, $publicGroup);

        return $query->whereIn('id', array_unique($tgt));
    }
}
