<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class TextCategoryType extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get all the categories of this category type.
     */
    public function categories()
    {
        return $this->hasMany(TextCategoryName::class, 'type_id');
    }
}
