<?php

namespace App\Entities\Shared;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Education extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'education';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id'];

    /**
     * Create a new model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->connection = config('database.shared');
    }
}
