<?php

namespace App\Entities;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['key', 'biography', 'avatar', 'type', 'name', 'sid'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['type', 'name', 'contact', 'sid'];

    /**
     * Get the user type of this user.
     */
    public function type()
    {
        return $this->belongsTo(UserType::class);
    }

    /**
     * Get all the external IDs of this user.
     */
    public function externalIds()
    {
        return $this->hasMany(ExternalId::class);
    }

    /**
     * Get all costomized user tags that this user owns.
     */
    public function userTagData()
    {
        return $this->hasMany(UserTag::class, 'owner_user_id');
    }

    /**
     * Get all the subscribe classifications of this user.
     */
    public function subscribes()
    {
        return $this->hasMany(UserSubscribe::class);
    }

    /**
     * Get all the groups of this user.
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class, 'group_user_roles');
    }

    /**
     * Get all the group_user_roles of this user.
     */
    public function groupRoles()
    {
        return $this->hasMany(GroupUserRole::class);
    }

    /**
     * Get the user type for this user.
     *
     * @return bool
     */
    public function getTypeAttribute()
    {
        return UserType::where('id', $this->type_id)->first()->name;
    }

    /**
    * Get the contact of this user.
    *
    * @return bool
    */
    public function getContactAttribute()
    {
        return Shared\Contact::where('id', $this->externalIds->first()->body)->first();
    }

    public function getSidAttribute()
    {
        return $this->contact ? $this->contact->id : null;
    }

    /**
     * Get the name of this user from his contact.
     *
     * @return bool
     */
    public function getNameAttribute()
    {
        return $this->contact ? $this->contact->name : null;
    }

    /**
     * Make a plain User entity.
     *
     * @param  string  $type  (optional)
     * @return self
     */
    public static function make($type = null)
    {
        $user = new self;
        $user->key = str_random(config('app.internal_key_length'));
        $identicon = new \Identicon\Identicon();
        $user->avatar = $identicon->getImageDataUri($user->key, 256);
        $type = $type ?: 'regular';
        $type = UserType::where('name', $type)->first();
        $user->type()->associate($type);

        return $user;
    }

    /**
     * Create a User in the database.
     *
     * @param  string  $type  (optional, default is 'regular')
     * @return self
     */
    public static function makeAndSave($type = null)
    {
        $user = self::make($type);
        $user->save();

        return $user;
    }

    /**
     * Get the user with the given external ID.
     *
     * @param  string  $id
     * @param  string  $type  (optional, default is according to the config)
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHavingExternalId($query, $id, $type = null)
    {
        $type = $type ?: config('auth.idtype');
        $ids = ExternalIdType::where('name', $type)->first()->idsOfThisType;
        $found = $ids->where('body', $id)->first();
        $tgt = $found ? [$found->owner->id] : [];

        return $query->whereIn('id', $tgt);
    }

    /**
     * Check that the basic informations of this user are up-to-date.
     */
    public function baiscInfosUpToDate() {
        $contact = $this->contact;
        return $contact ? $contact->name && $contact->email : false;
    }
}
