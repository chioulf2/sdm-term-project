<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class TextClassification extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the subject of this classification.
     */
    public function text()
    {
        return $this->belongsTo(Text::class);
    }

    /**
     * Get the object of this classification.
     */
    public function category()
    {
        return $this->belongsTo(TextCategoryName::class);
    }
}
