<?php

return [

    // If 'strict' is True, then the PHP Toolkit will reject unsigned or
    // unencrypted messages if it expects them to be signed or encrypted.
    // Also it will reject the messages if the SAML standard is not strictly
    // followed: Destination, NameId, Conditions, ... , are validated too.
    'strict' => false,

    // Enable debug mode (to print errors).
    'debug' => false,

    // Service Provider Data that we are deploying.
    'sp' => [

        // Identifier of the SP entity (must be a URI).
        // If not start with http://... , it will be taken as a relative
        // path against the app URL root.
        'entityId' => 'auth/saml2/metadata',

        // Specifies info about where and how the <AuthnResponse> message MUST
        // be returned to the requester, in this case our SP.
        'assertionConsumerService' => [

            // URL Location where the <Response> from the IdP will be returned.
            // If not start with http://... , it will be taken as a relative
            // path against the app URL root.
            'url' => 'auth/saml2/acs',

            // SAML protocol binding to be used when returning the <Response>
            // message. OneLogin Toolkit supports this endpoint for the
            // HTTP-POST binding only.
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',

        ],

        // Specifies info about where and how the <Logout Response> message MUST
        // be returned to the requester, in this case our SP.
        'singleLogoutService' => [

            // URL Location where the <Response> from the IdP will be returned.
            // If not start with http://... , it will be taken as a relative
            // path against the app URL root.
            'url' => 'auth/saml2/sls',

            // SAML protocol binding to be used when returning the <Response>
            // message. OneLogin Toolkit supports the HTTP-Redirect binding
            // only for this endpoint.
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',

        ],

        // Specifies the constraints on the name identifier to be used to
        // represent the requested subject. Take a look on
        // lib/Saml2/Constants.php to see the NameIdFormat supported.
        'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',

        // Usually x509cert and privateKey of the SP are provided by files
        // placed at the certs folder. But we can also provide them with the
        // following parameters.
        'x509cert' => '',
        'privateKey' => '',

    ],

    // Identity Provider Data that we want connected with our SP.
    'idp' => [

        // Identifier of the IdP entity (must be a URI).
        'entityId' => 'http://sdm.im.ntu.edu.tw/simplesamlphp/saml2/idp/metadata.php',

        // SSO endpoint info of the IdP (Authentication Request protocol).
        'singleSignOnService' => [

            // URL Target of the IdP where the Authentication Request Message
            // will be sent.
            'url' => 'http://sdm.im.ntu.edu.tw/simplesamlphp/saml2/idp/SSOService.php',

            // SAML protocol binding to be used when returning the <Response>
            // message. OneLogin Toolkit supports the HTTP-Redirect binding
            // only for this endpoint.
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',

        ],

        // SLO endpoint info of the IdP.
        'singleLogoutService' => [

            // URL Location of the IdP where SLO Request will be sent.
            'url' => 'http://sdm.im.ntu.edu.tw/simplesamlphp/saml2/idp/SingleLogoutService.php',

            // SAML protocol binding to be used when returning the <Response>
            // message. OneLogin Toolkit supports the HTTP-Redirect binding
            // only for this endpoint.
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',

        ],

        // Public x509 certificate of the IdP.
        'x509cert' => 'MIIEAzCCAuugAwIBAgIJAIDHA9QoK1uLMA0GCSqGSIb3DQEBCwUAMIGXMQswCQYDVQQGEwJ0dzEPMA0GA1UECAwGdGFpd2FuMQ8wDQYDVQQHDAZ0YWlwZWkxIzAhBgNVBAoMGk5hdGlvbmFsIFRhaXdhbiBVbml2ZXJzaXR5MSUwIwYDVQQLDBxJbmZvcm1hdGlvbiBNYW5hZ2VtZW50IERlcHQuMRowGAYDVQQDDBFzZG0uaW0ubnR1LmVkdS50dzAeFw0xNTEyMDEyMzI4MzZaFw0yNTExMzAyMzI4MzZaMIGXMQswCQYDVQQGEwJ0dzEPMA0GA1UECAwGdGFpd2FuMQ8wDQYDVQQHDAZ0YWlwZWkxIzAhBgNVBAoMGk5hdGlvbmFsIFRhaXdhbiBVbml2ZXJzaXR5MSUwIwYDVQQLDBxJbmZvcm1hdGlvbiBNYW5hZ2VtZW50IERlcHQuMRowGAYDVQQDDBFzZG0uaW0ubnR1LmVkdS50dzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALj0FOPN3nzFUXyE63uXGb5ZH8i55qWXPamrMeKxQOcc+Wn3E/b5GQ7geEgwoC34McAO8NBIXQpfLprIq+x0osE0IEbnDRMEu1gi+Rf9ZgQpZm/5gPN1pNQ1mYiCFkyebIYq8q1itlYu9Kn2M2Iqraoa6yRC48vjIS/NsBJ/5vq/UFgib6m78yAnxPW7IcdUkCnrjAKoKJnbxyJVhYJlo3QTS8VF8fYkse/HU9DpE1FNKJnmOcOhyQDZykjxATQ4wT472+Zk9W1bp2WXsCPN5uMUjN+pKOX1PdQH5TyWKVZ+1zrG3VK5Owupo/gD5TZxAueMcLJAGcYUuW/u8X3g97MCAwEAAaNQME4wHQYDVR0OBBYEFIoL6EqeXA/OqiqAAKE6DrsjAXNhMB8GA1UdIwQYMBaAFIoL6EqeXA/OqiqAAKE6DrsjAXNhMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAHp/Ky/dR3W8x4bYSBTJvlmMRRZbsvSgqeuLs7SYCNZIUju4uyyUkHcWwsYA0aLOD695wr+Ri1zI8Ms3ypUNt0+ZZIfqdQiKwNU4WIK8m19ieIdgo/Iy3m9hKnwxhhWHTCaTQDtOsv4SuVCTOEsM80b0zRyPmGFozaX+lp6hUIJvC7KjHw7H+Q3VnIbgoVOEMN8WfcAHmftYoqg1XzARVvVylKjIjzD3mHIQSab8FI3SttsY3/Nx54BGy07il8Lzd+fK1H0sZR7l3gFbVbDWuMJnw5arT9WwxjYAyPpEFUHuCtziwN7R1U83vJErD0eGvB+lfWmrdQIvq6AVo83/0ZM=',

        /*
        | Instead of use the whole x509cert you can use a fingerprint in order
        | to validate a SAMLResponse.
        | (openssl x509 -noout -fingerprint -in "idp.crt" to generate it,
        |  or add for example the -sha256 , -sha384 or -sha512 parameter)
        |
        | If a fingerprint is provided, then the certFingerprintAlgorithm is
        | required in order to let the toolkit know which algorithm was used.
        | Possible values: sha1, sha256, sha384 or sha512
        | sha1 is the default value.
        |
        | Notice that if you want to validate any SAML Message sent by the
        | HTTP-Redirect binding, you will need to provide the whole x509cert.
        */

        // 'certFingerprint' => '',
        // 'certFingerprintAlgorithm' => 'sha1',

    ],
];
