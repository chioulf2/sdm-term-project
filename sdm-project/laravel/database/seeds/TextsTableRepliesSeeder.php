<?php

use Illuminate\Database\Seeder;

use App\Entities\Text;
use App\Entities\TextCategoryName;
use Test\Support\Random;

class TextsTableRepliesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Text::ofCateg('level', 1)->delete();
        factory(Text::class, 10)->make()->each(function($r) {
            $r->user_id = Random::user()->id;
            $r->parent_id = Random::textOfLevel(0)->id;
            $r->save();

            $c = TextCategoryName::ofType('level')->where('name', 1)->first();
            $r->classes()->attach($c);
        });
    }
}
