<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserRelatedTablesSeeder::class);
        // $this->call(TextsTablePostsSeeder::class);
        $this->call(TextsTablePostsSeeder2::class);
        $this->call(TextsTableRepliesSeeder::class);
        $this->call(TextsTableRoRsSeeder::class);

        Model::reguard();
    }
}
