<?php

use Illuminate\Database\Seeder;

use App\Entities\User;
use App\Entities\Text;
use App\Entities\TextCategoryName;
use Test\Support\Random;

class TextsTableRoRsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Text::ofCateg('level', 2)->delete();
        factory(Text::class, 10)->make()->each(function($r) {
            $r->user_id = Random::user()->id;
            $r->parent_id = Random::textOfLevel(1)->id;
            $r->save();

            $c = TextCategoryName::ofType('level')->where('name', 2)->first();
            $r->classes()->attach($c);
        });
    }
}
