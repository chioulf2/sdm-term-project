<?php

use Illuminate\Database\Seeder;

use App\Entities\User;
use App\Entities\ExternalId;
use App\Entities\Shared\Contact;

class UserRelatedTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contact::where('id', '<>', '')->delete();
        User::all()->map(function ($u) {
            $u->delete();
        });
        factory(User::class, 15)->create()->each(function ($u) {
            $u->externalIds()->save(factory(ExternalId::class)->make());
            $c = factory(Contact::class)->make();
            $c->id = $u->externalIds->first()->body;
            $c->save();
        });
    }
}
