<?php

use App\Entities\Group;
use App\Entities\Text;
use App\Entities\TextCategoryName;
use Illuminate\Database\Seeder;
use Test\Support\Random;

class TextsTablePostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Text::ofCateg('level', 0)->delete();
        factory(Text::class, 10)->make()->each(function ($p) {
            $p->user_id = Random::user()->id;
            $p->save();
            // Put it into level 0
            $c = TextCategoryName::ofType('level')->where('name', 0)->first();
            $p->classes()->attach($c);
            // Put it into default group
            $p->classes()->attach(Group::find(0)->parentTableRecord);
        });
    }
}
