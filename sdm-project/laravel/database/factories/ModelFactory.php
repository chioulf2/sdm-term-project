<?php

use App\Entities as Entities;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

$factory->define(Entities\User::class, function () {
    return Entities\User::make()->getAttributes();
});

$factory->define(Entities\ExternalId::class, function (Faker\Generator $faker) {
    $bachelor = array_fill(1, 60, 'b');
    $master = array_fill(1, 20, 'r');
    $phd = array_fill(1, 10, 'd');
    $others = ['a', 'c', 'e', 'f', 'h', 'j', 'k', 'm', 'n', 'p', 's', 't'];
    $idPrefix = array_merge($bachelor, $master, $phd, $others);

    $studentId = $idPrefix[array_rand($idPrefix)] . rand(10000000, 99999999);
    $staffId = strtolower($faker->userName);
    $body = rand(0, 9) > 8 ? $staffId : $studentId;
    return Entities\ExternalId::make($body)->getAttributes();
});

$factory->define(Entities\Shared\Contact::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'address' => (rand(0, 9) > 6 ? null : $faker->address),
        'email' => $faker->email,
        'phone_mobile' => (rand(0, 9) > 8 ? null : $faker->phoneNumber),
        'phone_work' => (rand(0, 9) > 8 ? null : $faker->phoneNumber),
        'phone_home' => (rand(0, 9) > 5 ? null : $faker->phoneNumber),
    ];
});

$factory->define(Entities\Shared\Education::class, function (Faker\Generator $faker) {
    return [
        'concentration' => str_random(10),
        'degree' => str_random(20),
        'department' => str_random(20),
        'end_date' => str_random(20),
        'institute' => str_random(10),
        'obtained' => str_random(1),
        'start_date' => str_random(10),
    ];
});

$factory->define(Entities\Shared\Experience::class, function (Faker\Generator $faker) {
    return [
        'organization' => str_random(10),
        'department' => str_random(10),
        'position' => str_random(10),
        'start_date' => str_random(10),
        'end_date' => str_random(10),
        'description' => str_random(10),
    ];
});

$factory->define(Entities\Text::class, function (Faker\Generator $faker) {
    return Entities\Text::make($faker->realText())->getAttributes();
});
