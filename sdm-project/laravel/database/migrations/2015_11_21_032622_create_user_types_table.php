<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('user_types');
        Schema::create('user_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
        });

        // Create default user types.
        DB::table('user_types')->insert([
            ['name' => 'super'],
            ['name' => 'admin'],
            ['name' => 'regular'],
            ['name' => 'limited'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_types');
    }
}
