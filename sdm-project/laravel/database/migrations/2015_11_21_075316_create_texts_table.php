<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('texts');
        Schema::create('texts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->char('key', config('app.internal_key_length'))->unique();
            $table->string('body');
            $table->integer('classification_id')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('parent_id')->unsigned()->default(0);

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('parent_id')
                ->references('id')->on('texts')
                ->onDelete('cascade');
        });

        // Create a default dummy text to be the parent of posts.
        \App\Helpers\DB::foreignKeyConstraint(false);
        $dummy = new App\Entities\Text;
        $dummy->id = 0;
        $dummy->key = str_repeat('0', config('app.internal_key_length'));
        $dummy->body = 'dummy';
        $dummy->classification_id = 0;
        $dummy->user_id = 0;
        $dummy->parent_id = 0;
        $dummy->save();
        \App\Helpers\DB::foreignKeyConstraint(true);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('texts');
    }
}
