<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTextClassificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('text_classifications');
        Schema::create('text_classifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('text_id')->unsigned();
            $table->integer('category_id')->unsigned();

            $table->foreign('text_id')
                ->references('id')->on('texts')
                ->onDelete('cascade');
            $table->foreign('category_id')
                ->references('id')->on('text_category_names')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('text_classifications');
    }
}
