<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('user_tags');
        Schema::create('user_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('owner_user_id')->unsigned();
            $table->integer('target_user_id')->unsigned();
            $table->string('tag');

            $table->foreign('owner_user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('target_user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_tags');
    }
}
