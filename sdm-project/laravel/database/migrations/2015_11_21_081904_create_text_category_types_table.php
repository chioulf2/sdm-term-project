<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTextCategoryTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('text_category_types');
        Schema::create('text_category_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
        });

        // Create default categorie types.
        DB::table('text_category_types')->insert([
            ['name' => 'level'],
            ['name' => 'tag'],
            ['name' => 'group'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('text_category_types');
    }
}
