<?php

use App\Entities\TextCategoryName;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTextCategoryNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('text_category_names');
        Schema::create('text_category_names', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('type_id');

            $table->foreign('type_id')
                ->references('id')->on('text_category_types')
                ->onDelete('cascade');
        });

        // Create default categories.
        TextCategoryName::make('0', 'level')->save();
        TextCategoryName::make('1', 'level')->save();
        TextCategoryName::make('2', 'level')->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('text_category_names');
    }
}
