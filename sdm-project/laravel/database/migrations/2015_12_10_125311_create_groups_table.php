<?php

use App\Entities\TextCategoryName;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::dropIfExists('groups');
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('category_id')->unsigned();
            $table->char('key', config('app.internal_key_length'))->unique();
            $table->string('description');
            $table->boolean('private')->default(false);

            $table->foreign('category_id')
                ->references('id')->on('text_category_names')
                ->onDelete('cascade');
        });

        // Create default group.
        $dummyParent = TextCategoryName::makeAndSave('dummy', 'group');
        $dummyChild = new App\Entities\Group;
        $dummyChild->id = 0;
        $dummyChild->description = 'default group';
        $dummyChild->key = str_repeat('0', config('app.internal_key_length'));
        $dummyChild->parentTableRecord()->associate($dummyParent);
        $dummyChild->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('groups');
    }
}
