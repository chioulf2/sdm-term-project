<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSharedExperienceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(config('database.shared'))->dropIfExists('experience');
        Schema::connection(config('database.shared'))->create('experience',
            function (Blueprint $table) {
                $table->string('id', 20);
                $table->string('organization', 50);
                $table->string('department', 50)->nullable();
                $table->string('position', 20)->nullable();
                $table->date('start_date');
                $table->date('end_date')->nullable();
                $table->string('description', 100)->nullable();

                $table->primary(['id', 'organization', 'start_date']);
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(config('database.shared'))->dropIfExists('experience');
    }
}
