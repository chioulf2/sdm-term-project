<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupUserRoleTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::dropIfExists('group_user_role_types');
        Schema::create('group_user_role_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        // Create default user-group role types.
        DB::table('group_user_role_types')->insert([
            ['name' => 'admin'],
            ['name' => 'regular'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('group_user_role_types');
    }
}
