<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExternalIdTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('external_id_types');
        Schema::create('external_id_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
        });

        // Create default external ID types.
        DB::table('external_id_types')->insert([
            'name' => config('auth.idtype'),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('external_id_types');
    }
}
