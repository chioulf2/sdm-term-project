<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSharedEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(config('database.shared'))->dropIfExists('education');
        Schema::connection(config('database.shared'))->create('education',
            function (Blueprint $table) {
                $table->string('id', 20);
                $table->string('degree', 40);
                $table->string('institute', 50);
                $table->string('department', 50)->nullable();
                $table->date('start_date');
                $table->date('end_date')->nullable();
                $table->string('concentration', 100)->nullable();
                $table->tinyInteger('obtained');

                $table->primary(['id', 'degree', 'institute', 'start_date']);
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(config('database.shared'))->dropIfExists('education');
    }
}
