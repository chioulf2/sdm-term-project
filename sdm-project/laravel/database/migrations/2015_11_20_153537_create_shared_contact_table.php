<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSharedContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(config('database.shared'))->dropIfExists('contact');
        Schema::connection(config('database.shared'))->create('contact',
            function (Blueprint $table) {
                $table->string('id', 20);
                $table->string('name', 30);
                $table->string('address', 100)->nullable();
                $table->string('email', 50);
                $table->string('phone_mobile', 16)->nullable();
                $table->string('phone_work', 16)->nullable();
                $table->string('phone_home', 16)->nullable();

                $table->primary('id');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(config('database.shared'))->dropIfExists('contact');
    }
}
