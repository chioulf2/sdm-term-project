<?php
namespace Test;

use App\Entities\Group;
use App\Entities\GroupUserRoleType;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Session;

class GroupTest extends TestCase
{
    use DatabaseTransactions;

    public function testIndexSimple()
    {
        $creator = Support\Create::regularUser();
        $user = Support\Create::regularUser();
        $name1 = Support\Create::testName();
        $name2 = Support\Create::testName();
        $publicGroup = Group::makeAndSave($name1, $creator, 'A public group');
        $privateGroup = Group::makeAndSave($name2, $creator, 'A private group', true);

        Session::start();
        $this->actingAs($user)
            ->get('api/group/')
            ->see(Group::find(0)->key)
            ->see($publicGroup->key);
    }

    public function testStoreSimple()
    {
        $creator = Support\Create::regularUser();
        $name = Support\Create::testName();

        Session::start();
        $this->actingAs($creator)
            ->post('api/group/', [
                '_token' => csrf_token(),
                'name' => $name,
            ])
            ->seeJson([
                'name' => $name,
                'private' => '0', // default
                'admins' => [$creator->key],
            ]);
    }

    public function testStoreSameNameNotAllowed()
    {
        $creator = Support\Create::regularUser();
        $name = Support\Create::testName();
        $group = Group::makeAndSave($name, $creator);

        Session::start();
        $this->actingAs($creator)
            ->post('api/group/', [
                '_token' => csrf_token(),
                'name' => $name,
            ])
            ->assertResponseStatus(409);
    }

    public function testStoreToBePrivate()
    {
        $creator = Support\Create::regularUser();
        $name = Support\Create::testName();

        Session::start();
        $this->actingAs($creator)
            ->post('api/group/', [
                '_token' => csrf_token(),
                'name' => $name,
                'private' => '1',
            ])
            ->seeJson([
                'name' => $name,
                'private' => '1',
                'admins' => [$creator->key],
            ]);
    }

    public function testUpdateSimple()
    {
        $creator = Support\Create::regularUser();
        $name = Support\Create::testName();
        $descToUpdate = Support\Create::testTextBody();
        $group = Group::makeAndSave($name, $creator);
        $this->assertNotNull($group);

        Session::start();
        $this->actingAs($creator)
            ->put('api/group/' . $group->key, [
                '_token' => csrf_token(),
                'description' => $descToUpdate,
                'private' => '1',
            ])
            ->seeJson([
                'name' => $group->name,
                'description' => $descToUpdate,
                'private' => '1',
                'admins' => [$creator->key],
            ]);
    }

    public function testUpdateMemberNotAllowed()
    {
        $creator = Support\Create::regularUser();
        $member = Support\Create::regularUser();
        $name = Support\Create::testName();
        $group = Group::makeAndSave($name, $creator);
        $group->addMember($member);

        Session::start();
        $this->actingAs($member)
            ->put('api/group/' . $group->key, [
                '_token' => csrf_token(),
                'private' => '1',
            ])
            ->assertResponseStatus(403);
    }

    public function testUpdateAlienNotAllowed()
    {
        $creator = Support\Create::regularUser();
        $alien = Support\Create::regularUser();
        $name = Support\Create::testName();
        $group = Group::makeAndSave($name, $creator);

        Session::start();
        $this->actingAs($alien)
            ->put('api/group/' . $group->key, [
                '_token' => csrf_token(),
                'private' => '1',
            ])
            ->assertResponseStatus(403);
    }

    public function testShowSimple()
    {
        $creator = Support\Create::regularUser();
        $name = Support\Create::testName();
        $group = Group::makeAndSave($name, $creator);

        Session::start();
        $this->actingAs($creator)
            ->get('api/group/' . $group->key)
            ->seeJson([
                'name' => $group->name,
                'description' => 'The description of the group.',
                'private' => '0',
                'admins' => [$creator->key],
            ]);
    }

    public function testShowNotAllowed()
    {
        $creator = Support\Create::regularUser();
        $alien = Support\Create::regularUser();
        $name = Support\Create::testName();
        $group = Group::makeAndSave($name, $creator, 'A private group', true);

        Session::start();
        $this->actingAs($alien)
            ->get('api/group/' . $group->key)
            ->assertResponseStatus(403);
    }

    public function testDestroySimple()
    {
        $creator = Support\Create::regularUser();
        $name = Support\Create::testName();
        $group = Group::makeAndSave($name, $creator);
        $groupKey = $group->key;

        Session::start();
        $this->actingAs($creator)
            ->delete('api/group/' . $groupKey, [
                '_token' => csrf_token(),
            ])
            ->assertResponseOk();

        $clearInDB = Group::where('key', $groupKey)->get()->isEmpty();
        $this->assertTrue($clearInDB);
    }

    public function testAddMemberSimple()
    {
        $creator = Support\Create::regularUser();
        $member = Support\Create::regularUser();
        $name = Support\Create::testName();
        $group = Group::makeAndSave($name, $creator);

        Session::start();
        $this->actingAs($creator)
            ->put('api/group/' . $group->key . '/member/' . $member->key, [
                '_token' => csrf_token(),
            ])
            ->assertResponseOk();

        $this->assertNotNull($group->hasMember($member));
    }

    public function testAddAdminSimple()
    {
        $creator = Support\Create::regularUser();
        $member = Support\Create::regularUser();
        $name = Support\Create::testName();
        $group = Group::makeAndSave($name, $creator);
        $group->addMember($member);

        Session::start();
        $this->actingAs($creator)
            ->put('api/group/' . $group->key . '/admin/' . $member->key, [
                '_token' => csrf_token(),
            ])
            ->assertResponseOk();

        $this->assertNotNull($group->hasAdmin($member));
    }

    public function testChangeAdminToMember()
    {
        $creator = Support\Create::regularUser();
        $member = Support\Create::regularUser();
        $name = Support\Create::testName();
        $group = Group::makeAndSave($name, $creator);
        $group->addAdmin($member);

        Session::start();
        $this->actingAs($creator)
            ->put('api/group/' . $group->key . '/member/' . $member->key, [
                '_token' => csrf_token(),
            ])
            ->assertResponseOk();

        $this->assertNull($group->hasAdmin($member));
        $this->assertNotNull($group->hasMember($member));
    }

    public function testChangeMemberToAdmin()
    {
        $creator = Support\Create::regularUser();
        $member = Support\Create::regularUser();
        $name = Support\Create::testName();
        $group = Group::makeAndSave($name, $creator);
        $group->addMember($member);

        Session::start();
        $this->actingAs($creator)
            ->put('api/group/' . $group->key . '/admin/' . $member->key, [
                '_token' => csrf_token(),
            ])
            ->assertResponseOk();

        $this->assertNotNull($group->hasAdmin($member));
    }

    public function testRemoveMember()
    {
        $creator = Support\Create::regularUser();
        $member = Support\Create::regularUser();
        $name = Support\Create::testName();
        $group = Group::makeAndSave($name, $creator);
        $group->addMember($member);

        Session::start();
        $this->actingAs($creator)
            ->delete('api/group/' . $group->key . '/member/' . $member->key, [
                '_token' => csrf_token(),
            ])
            ->assertResponseOk();

        $this->assertNull($group->hasMember($member));
    }

    public function testRemoveAdminMember()
    {
        $creator = Support\Create::regularUser();
        $member = Support\Create::regularUser();
        $name = Support\Create::testName();
        $group = Group::makeAndSave($name, $creator);
        $group->addAdmin($member);

        Session::start();
        $this->actingAs($creator)
            ->delete('api/group/' . $group->key . '/member/' . $member->key, [
                '_token' => csrf_token(),
            ])
            ->assertResponseOk();

        $this->assertNull($group->hasAdmin($member));
        $this->assertNull($group->hasMember($member));
    }

    public function testAutocomplete()
    {
        $creator = Support\Create::regularUser();
        $user = Support\Create::regularUser();

        $publicGroup = Group::makeAndSave('aab', $creator, 'A public group');
        $privateGroup1 = Group::makeAndSave('aba', $creator, 'A private group', true);
        $privateGroup2 = Group::makeAndSave('baa', $creator, 'Another private group', true);
        $privateGroup2->addAdmin($user);

        Session::start();
        $response = $this->actingAs($user)
            ->call('GET', 'api/group/namecomplete', ['prefix' => 'a']);

        $actual = json_decode($response->content(), true);
        $this->assertEquals([
            ['name' => $publicGroup->name, 'key' => $publicGroup->key],
        ], $actual);
    }
}
