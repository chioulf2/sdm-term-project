<?php
namespace Test;

use App\Entities\User;
use App\Entities\UserTag;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Session;

class UserTagTest extends TestCase
{
    use DatabaseTransactions;

    public function testIndex()
    {
        $tagOwner = User::makeAndSave();
        $tagTarget = User::makeAndSave();
        $tag1 = str_random(10);
        $tag2 = str_random(10);
        UserTag::makeAndSave($tagOwner, $tagTarget, $tag1);
        UserTag::makeAndSave($tagOwner, $tagTarget, $tag2);

        Session::start();
        $this->actingAs($tagOwner)
            ->get('api/user/' . $tagTarget->key . '/tag')
            ->seeJsonEquals([$tag1, $tag2]);
    }

    public function testUpdate()
    {
        $tagOwner = User::makeAndSave();
        $tagTarget = User::makeAndSave();
        $tag = str_random(10);

        Session::start();
        $this->actingAs($tagOwner)
            ->put('api/user/' . $tagTarget->key . '/tag/' . $tag, [
                '_token' => csrf_token(),
            ])
            ->assertResponseOk();

        $foundInDB = !$tagOwner->userTagData
            ->where('target_user_id', (string) $tagTarget->id)->isEmpty();
        $this->assertTrue($foundInDB);
    }

    public function testDestroy()
    {
        $tagOwner = User::makeAndSave();
        $tagTarget = User::makeAndSave();
        $tag = str_random(10);
        UserTag::makeAndSave($tagOwner, $tagTarget, $tag);

        Session::start();
        $this->actingAs($tagOwner)
            ->delete('api/user/' . $tagTarget->key . '/tag/' . $tag, [
                '_token' => csrf_token(),
            ])
            ->assertResponseOk();

        $clearInDB = $tagOwner->userTagData
            ->where('target_user_id', (string) $tagTarget->id)->isEmpty();
        $this->assertTrue($clearInDB);
    }

    public function testAutocomplete()
    {
        $tagOwner = User::makeAndSave();
        $tagTarget = User::makeAndSave();
        $tag1 = 'aaa';
        $tag2 = 'aab';
        $tag3 = 'aba';
        $tag4 = 'baa';
        UserTag::makeAndSave($tagOwner, $tagTarget, $tag1);
        UserTag::makeAndSave($tagOwner, $tagTarget, $tag2);
        UserTag::makeAndSave($tagOwner, $tagTarget, $tag3);
        UserTag::makeAndSave($tagOwner, $tagTarget, $tag4);

        $response = $this->actingAs($tagOwner)
            ->call('GET', 'api/user/tagcomplete', ['prefix' => 'a']);

        $expect = [$tag1, $tag2, $tag3];
        $actual = json_decode($response->content());
        sort($expect);
        sort($actual);

        $this->assertEquals(json_encode($expect), json_encode($actual));
    }
}
