<?php

namespace Test;

use App\Entities\Text;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Session;

class ReplyOfReplyTest extends TestCase
{
    use DatabaseTransactions;

    public function testBasicIndex()
    {
        $postUser = Support\Create::regularUser();
        $replyUser = Support\Create::regularUser();
        $replyOfReplyUser = Support\Create::regularUser();
        $postBody = Support\Create::testTextBody();
        $replyBody = Support\Create::testTextBody();
        $replyOfReplyBody = Support\Create::testTextBody();

        $post = Text::makeAndSave($postBody, $postUser);
        $reply = Text::makeAndSave($replyBody, $replyUser, 1);
        $reply->parent()->associate($post);
        $reply->save();

        $replyOfReply = Text::makeAndSave($replyOfReplyBody, $replyOfReplyUser, 2);
        $replyOfReply->parent()->associate($reply);
        $replyOfReply->save();

        Session::start();
        $this->actingAs($replyOfReplyUser)
            ->get('api/reply/' . $reply->key . '/reply')
            ->seeJson([
                'ownerkey' => $replyOfReplyUser->key,
                'body' => $replyOfReplyBody,
                'level' => '2',
            ]);
    }

    public function testBasicStore()
    {
        $postUser = Support\Create::regularUser();
        $replyUser = Support\Create::regularUser();
        $replyOfReplyUser = Support\Create::regularUser();
        $postBody = Support\Create::testTextBody();
        $replyBody = Support\Create::testTextBody();
        $replyOfReplyBody = Support\Create::testTextBody();

        $post = Text::makeAndSave($postBody, $postUser);
        $reply = Text::makeAndSave($replyBody, $replyUser, 1);
        $reply->parent()->associate($post);
        $reply->save();

        Session::start();
        $this->actingAs($replyOfReplyUser)
            ->post('api/reply/' . $reply->key . '/reply', [
                '_token' => csrf_token(),
                'body' => $replyOfReplyBody,
            ])
            ->seeJson([
                'ownerkey' => $replyOfReplyUser->key,
                'body' => $replyOfReplyBody,
                'level' => '2',
            ]);
    }

    public function testBasicUpdate()
    {
        $postUser = Support\Create::regularUser();
        $replyUser = Support\Create::regularUser();
        $replyOfReplyUser = Support\Create::regularUser();
        $postBody = Support\Create::testTextBody();
        $replyBody = Support\Create::testTextBody();
        $oldReplyOfReplyBody = Support\Create::testTextBody();
        $newReplyOfReplyBody = Support\Create::testTextBody();

        $post = Text::makeAndSave($postBody, $postUser);
        $reply = Text::makeAndSave($replyBody, $replyUser, 1);
        $reply->parent()->associate($post);
        $reply->save();

        $replyOfReply = Text::makeAndSave($oldReplyOfReplyBody, $replyOfReplyUser, 2);
        $replyOfReply->parent()->associate($reply);
        $replyOfReply->save();

        Session::start();
        $this->actingAs($replyOfReplyUser)
            ->put('api/reply/' . $replyOfReply->key, [
                '_token' => csrf_token(),
                'body' => $newReplyOfReplyBody,
            ])
            ->seeJson([
                'ownerkey' => $replyOfReplyUser->key,
                'body' => $newReplyOfReplyBody,
                'level' => '2',
            ]);

    }

    public function testBasicDestroy()
    {
        $postUser = Support\Create::regularUser();
        $replyUser = Support\Create::regularUser();
        $replyOfReplyUser = Support\Create::regularUser();
        $postBody = Support\Create::testTextBody();
        $replyBody = Support\Create::testTextBody();
        $replyOfReplyBody = Support\Create::testTextBody();

        $post = Text::makeAndSave($postBody, $postUser);
        $reply = Text::makeAndSave($replyBody, $replyUser, 1);
        $reply->parent()->associate($post);
        $reply->save();

        $replyOfReply = Text::makeAndSave($replyOfReplyBody, $replyOfReplyUser, 2);
        $replyOfReply->parent()->associate($reply);
        $replyOfReply->save();

        Session::start();
        $this->actingAs($replyOfReplyUser)
            ->delete('api/reply/' . $replyOfReply->key, [
                '_token' => csrf_token(),
            ])
            ->assertResponseOk();

        $clearInDB = Text::where('key', $replyOfReply->key)->get()->isEmpty();
        $this->assertTrue($clearInDB);
    }
}
