<?php

namespace Test;

use App\Entities\Text;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Session;

class ReplyOfPostTest extends TestCase
{
    use DatabaseTransactions;

    public function testBasicIndex()
    {
        $postUser = Support\Create::regularUser();
        $replyUser = Support\Create::regularUser();
        $postBody = Support\Create::testTextBody();
        $replyBody = Support\Create::testTextBody();
        $post = Text::makeAndSave($postBody, $postUser);
        $reply = Text::makeAndSave($replyBody, $replyUser, 1);
        $reply->parent()->associate($post);
        $reply->save();

        Session::start();
        $this->actingAs($postUser)
            ->get('api/post/' . $post->key . '/reply')
            ->seeJson([
                'ownerkey' => $replyUser->key,
                'body' => $reply->body,
                'level' => '1',
            ]);
    }

    public function testBasicStore()
    {
        $postUser = Support\Create::regularUser();
        $replyUser = Support\Create::regularUser();
        $postBody = Support\Create::testTextBody();
        $replyBody = Support\Create::testTextBody();
        $post = Text::makeAndSave($postBody, $postUser);

        Session::start();
        $this->actingAs($replyUser)
            ->post('api/post/' . $post->key . '/reply', [
                '_token' => csrf_token(),
                'body' => $replyBody,
            ])
            ->seeJson([
                'ownerkey' => $replyUser->key,
                'body' => $replyBody,
                'level' => '1',
            ]);
    }

    public function testBasicUpdate()
    {
        $postUser = Support\Create::regularUser();
        $replyUser = Support\Create::regularUser();
        $postBody = Support\Create::testTextBody();
        $oldReplyBody = Support\Create::testTextBody();
        $newReplyBody = Support\Create::testTextBody();
        $post = Text::makeAndSave($postBody, $postUser);
        $reply = Text::makeAndSave($oldReplyBody, $replyUser, 1);
        $reply->parent()->associate($post);
        $reply->save();

        Session::start();
        $this->actingAs($replyUser)
            ->put('api/reply/' . $reply->key, [
                '_token' => csrf_token(),
                'body' => $newReplyBody,
            ])
            ->seeJson([
                'ownerkey' => $replyUser->key,
                'body' => $newReplyBody,
                'level' => '1',
            ]);
    }

    public function testBasicDestroy()
    {
        $postUser = Support\Create::regularUser();
        $replyUser = Support\Create::regularUser();
        $postBody = Support\Create::testTextBody();
        $replyBody = Support\Create::testTextBody();
        $post = Text::makeAndSave($postBody, $postUser);
        $reply = Text::makeAndSave($replyBody, $replyUser, 1);
        $reply->parent()->associate($post);
        $reply->save();

        Session::start();
        $this->actingAs($replyUser)
            ->delete('api/reply/' . $reply->key, [
                '_token' => csrf_token(),
            ])
            ->assertResponseOk();

        $clearInDB = Text::where('key', $reply->key)->get()->isEmpty();
        $this->assertTrue($clearInDB);
    }

}
