<?php

namespace Test;

use App\Entities\UserSubscribe;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Session;

class UserSubscribeTest extends TestCase
{
    use DatabaseTransactions;

    public function testBasicShow()
    {
        $owner = Support\Create::regularUser();
        $ownerId = $owner->id;
        $userSubscribe = UserSubscribe::makeAndSave($owner->id);

        Session::start();
        $this->actingAs($owner)
            ->get('api/user/' . $owner->key . '/subscribe')
            ->seeJson([
                'user_id' => (string) $ownerId,
                'classification_id' => "0",
            ]);
    }

    public function testBasicStore()
    {
        $owner = Support\Create::regularUser();
        $userSubscribe = UserSubscribe::make($owner->id);

        Session::start();
        $this->actingAs($owner)
            ->put('api/user/' . $owner->key . '/subscribe', [
                '_token' => csrf_token(),
                'classification_id' => $userSubscribe->classificationId,
            ])
            ->seeJson([
                'user_id' => (string) $owner->id,
                'classification_id' => "0",
            ]);
    }

    public function testBasicDestroy()
    {
        $owner = Support\Create::regularUser();
        $userSubscribe = UserSubscribe::makeAndSave($owner->id);

        Session::start();
        $this->actingAs($owner)
            ->put('api/user/' . $owner->key . '/subscribe/delete', [
                '_token' => csrf_token(),
                'classification_id' => $userSubscribe->classification_id,
            ])
            ->see('OK');
    }
}
