<?php

namespace Test\Support;

use App\Entities\ExternalId;
use App\Entities\Group;
use App\Entities\User;
use App\Entities\Shared as Shared;

class Create {
    /**
     * Create a plain user (with its default external ID) in the database.
     *
     * @return App\Entities\User
     */
    public static function regularUser()
    {
        $user = factory(User::class)->create();
        $user->externalIds()->save(factory(ExternalId::class)->make());

        $defaultGroup = Group::find(0);
        $defaultGroup->addMember($user);

        return $user;
    }

    /**
     * Create a plain user and its contact in the database.
     *
     * @return App\Entities\User
     */
    public static function regularUserWithContact()
    {
        $user = self::regularUser();

        $contact = factory(Shared\Contact::class)->make();
        $contact->id = $user->externalIds->first()->body;
        $contact->save();

        return $user;
    }

    public static function testTextBody()
    {
        return 'test body: ' . str_random(config('app.internal_key_length'));
    }

    public static function testName()
    {
        return 'test name: ' . str_random(config('app.internal_key_length'));
    }
}
