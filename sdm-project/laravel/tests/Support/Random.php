<?php

namespace Test\Support;

use App\Entities as Entities;

class Random {
    /**
     * Return a random picked user from the database.
     *
     * @return void
     */
    public static function user()
    {
        $min = Entities\User::all()->min('id');
        $max = Entities\User::all()->max('id');
        return Entities\User::find(rand($min, $max));
    }

    /**
     * Return a random picked text of the given level from the database.
     *
     * @param  int  $level
     * @return void
     */
    public static function textOfLevel($level)
    {
        $min = Entities\Text::ofCateg('level', $level)->min('id');
        $max = Entities\Text::ofCateg('level', $level)->max('id');
        return Entities\Text::find(rand($min, $max));
    }
}
