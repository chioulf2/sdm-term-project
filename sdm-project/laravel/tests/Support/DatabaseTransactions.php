<?php

namespace Test\Support;

trait DatabaseTransactions
{
    /**
     * @before
     */
    public function beginDatabaseTransaction()
    {
        $this->app->make('db')->beginTransaction();
        $this->app->make('db')->connection(config('database.shared'))->beginTransaction();

        $this->beforeApplicationDestroyed(function () {
            $this->app->make('db')->rollBack();
            $this->app->make('db')->connection(config('database.shared'))->rollBack();
        });
    }
}
