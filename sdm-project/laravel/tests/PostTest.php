<?php

namespace Test;

use App\Entities\Group;
use App\Entities\Text;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Session;

class PostTest extends TestCase
{
    use DatabaseTransactions;

    public function testIndexSimple()
    {
        $user = Support\Create::regularUser();
        $textBody = Support\Create::testTextBody();
        $post = Text::makeAndSave($textBody, $user);

        Session::start();
        $this->actingAs($user)
            ->get('api/post')
            ->seeJson([
                'ownerkey' => $user->key,
                'body' => $textBody,
                'level' => '0',
            ]);
    }

    public function testIndexEmpty()
    {
        $user = Support\Create::regularUser();

        Session::start();
        $this->actingAs($user)
            ->get('api/post')
            ->seeJson([]);
    }

    public function testIndexVisibilitySimple()
    {
        $user1 = Support\Create::regularUser();
        $user2 = Support\Create::regularUser();

        // user1 owns a private group and a public group
        $groupName = Support\Create::testName();
        $privateGroup = Group::makeAndSave($groupName, $user1, 'A private group', true);
        $groupName = Support\Create::testName();
        $publicGroup = Group::makeAndSave($groupName, $user1, 'A public group');

        // user1 owns postA in the default group
        $textBodyA = Support\Create::testTextBody();
        $postA = Text::makeAndSave($textBodyA, $user1);
        // user1 owns postB in the private group
        $textBodyB = Support\Create::testTextBody();
        $postB = Text::makeAndSave($textBodyB, $user1, 0, 0, $privateGroup);
        // user1 owns postC in the private group
        $textBodyC = Support\Create::testTextBody();
        $postC = Text::makeAndSave($textBodyC, $user1, 0, 0, $publicGroup);

        Session::start();
        // user1 can see:
        $this->actingAs($user1)
            ->get('api/post')
            ->see($textBodyA)
            ->see($textBodyB)
            ->see($textBodyC);
        // user2 (only as a member in the default group) can see:
        $this->actingAs($user2)
            ->get('api/post')
            ->see($textBodyA)
            ->see($textBodyC);
    }

    public function testStoreSimple()
    {
        $user = Support\Create::regularUser();
        $textBody = Support\Create::testTextBody();

        Session::start();
        $this->actingAs($user)
            ->post('api/post', [
                '_token' => csrf_token(),
                'body' => $textBody,
            ])
            ->seeJson([
                'ownerkey' => $user->key,
                'body' => $textBody,
                'level' => '0',
            ]);

        $inDefaultGroup = !Group::find(0)->parentTableRecord->texts
            ->where('body', $textBody)->isEmpty();
        $this->assertTrue($inDefaultGroup);
    }

    public function testStoreToGroupAsAdmin()
    {
        $user = Support\Create::regularUser();
        $textBody = Support\Create::testTextBody();

        // user is the creator and thus an admin of the group
        $groupName = Support\Create::testName();
        $group = Group::makeAndSave($groupName, $user);

        Session::start();
        $this->actingAs($user)
            ->post('api/post', [
                '_token' => csrf_token(),
                'body' => $textBody,
                'groupkey' => $group->key,
            ])
            ->seeJson([
                'ownerkey' => $user->key,
                'body' => $textBody,
                'level' => '0',
                'groupkey' => $group->key,
            ]);
    }

    public function testStoreToGroupAsMember()
    {
        $creator = Support\Create::regularUser();
        $member = Support\Create::regularUser();
        $textBody = Support\Create::testTextBody();

        $groupName = Support\Create::testName();
        $group = Group::makeAndSave($groupName, $creator);
        $group->addMember($member);

        Session::start();
        $this->actingAs($member)
            ->post('api/post', [
                '_token' => csrf_token(),
                'body' => $textBody,
                'groupkey' => $group->key,
            ])
            ->seeJson([
                'ownerkey' => $member->key,
                'body' => $textBody,
                'level' => '0',
                'groupkey' => $group->key,
            ]);
    }

    public function testStoreToGroupAlienNotAllowed()
    {
        $user = Support\Create::regularUser();
        $alien = Support\Create::regularUser();

        $groupName = Support\Create::testName();
        $group = Group::makeAndSave($groupName, $user);

        Session::start();
        $this->actingAs($alien)
            ->post('api/post', [
                '_token' => csrf_token(),
                'body' => 'some text ...',
                'groupkey' => $group->key,
            ])
            ->assertResponseStatus(403);
    }

    public function testUpdateSimple()
    {
        $user = Support\Create::regularUser();
        $textBodyOld = Support\Create::testTextBody();
        $textBodyNew = Support\Create::testTextBody();
        $postKey = Text::makeAndSave($textBodyOld, $user)->key;

        Session::start();
        $this->actingAs($user)
            ->put('api/post/' . $postKey, [
                '_token' => csrf_token(),
                'body' => $textBodyNew,
            ])
            ->seeJson([
                'ownerkey' => $user->key,
                'body' => $textBodyNew,
                'level' => '0',
            ]);
    }

    public function testUpdateNonownerNotAllowed()
    {
        $owner = Support\Create::regularUser();
        $others = Support\Create::regularUser();
        $textBodyOld = Support\Create::testTextBody();
        $textBodyNew = Support\Create::testTextBody();
        $postKey = Text::makeAndSave($textBodyOld, $owner)->key;

        Session::start();
        $this->actingAs($others)
            ->put('api/post/' . $postKey, [
                '_token' => csrf_token(),
                'body' => $textBodyNew,
            ])
            ->assertResponseStatus(403);
    }

    public function testUpdateInGroupNonownerNotAllowed()
    {
        $owner = Support\Create::regularUser();
        $others = Support\Create::regularUser();
        $textBodyOld = Support\Create::testTextBody();
        $textBodyNew = Support\Create::testTextBody();

        $group = Group::makeAndSave('groupName', $owner);
        $postKey = Text::makeAndSave($textBodyOld, $owner, 0, 0, $group)->key;
        $group->addMember($others);

        Session::start();
        $this->actingAs($others)
            ->put('api/post/' . $postKey, [
                '_token' => csrf_token(),
                'body' => $textBodyNew,
            ])
            ->assertResponseStatus(403);
    }

    public function testUpdateInGroupAlienNotAllowed()
    {
        $user = Support\Create::regularUser();
        $alien = Support\Create::regularUser();
        $textBodyOld = Support\Create::testTextBody();
        $textBodyNew = Support\Create::testTextBody();

        $group = Group::makeAndSave('groupName', $user);
        $postKey = Text::makeAndSave($textBodyOld, $user, 0, 0, $group)->key;

        Session::start();
        $this->actingAs($alien)
            ->put('api/post/' . $postKey, [
                '_token' => csrf_token(),
                'body' => $textBodyNew,
            ])
            ->assertResponseStatus(403);
    }

    public function testBasicShow()
    {
        $owner = Support\Create::regularUser();
        $textBody = Support\Create::testTextBody();
        $post = Text::makeAndSave($textBody, $owner);

        $this->actingAs($owner)
            ->get('api/post/' . $post->key)
            ->seeJson([
                'ownerkey' => $owner->key,
                'body' => $post->body,
                'level' => '0',
            ]);
    }

    public function testBasicDestroy()
    {
        $owner = Support\Create::regularUser();
        $textBody = Support\Create::testTextBody();
        $postKey = Text::makeAndSave($textBody, $owner)->key;

        Session::start();
        $this->actingAs($owner)
            ->delete('api/post/' . $postKey, [
                '_token' => csrf_token(),
            ])
            ->assertResponseOk();

        $clearInDB = Text::where('key', $postKey)->get()->isEmpty();
        $this->assertTrue($clearInDB);
    }
}
