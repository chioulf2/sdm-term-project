<?php
namespace Test;

use App\Entities\Shared\Contact;
use App\Entities\Shared\Education;
use App\Entities\Shared\Experience;
use Illuminate\Support\Facades\Session;
use Test\Support\DatabaseTransactions;

class ProfileTest extends TestCase
{
    use DatabaseTransactions;

    public function testBasicShow()
    {
        $user = Support\Create::regularUserWithContact();
        $education = factory(Education::class)->make();
        $education->id = $user->externalIds->first()->body;
        $education->save();
        $experience = factory(Experience::class)->make();
        $experience->id = $user->externalIds->first()->body;
        $experience->save();

        Session::start();
        $this->actingAs($user)
            ->get('api/user/' . $user->key . '/profile')
            ->seeJsonEquals([
                'contact' => $user->contact->toArray(),
                'educations' => [$education->toArray()],
                'experiences' => [$experience->toArray()],
            ]);
    }

    public function testBasicGetContact()
    {
        $user = Support\Create::regularUserWithContact();
        $contact = $user->contact;

        Session::start();
        $this->actingAs($user)
            ->get('api/user/' . $user->key . '/profile/contact')
            ->seeJson([
                'id' => $contact->id,
                'name' => $contact->name,
                'address' => $contact->address,
                'email' => $contact->email,
                'phone_mobile' => $contact->phone_mobile,
                'phone_work' => $contact->phone_work,
                'phone_home' => $contact->phone_home,
            ]);
    }

    public function testBasicPutContact()
    {
        $user = Support\Create::regularUser();

        $contact = factory(Contact::class)->make();
        $contact->id = $user->externalIds->first()->body;
        $contact->save();

        Session::start();
        $this->actingAs($user)
            ->put('api/user/' . $user->key . '/profile/contact', [
                '_token' => csrf_token(),

                'name' => 'testName',
            ])
            ->seeJson([
                'id' => $contact->id,
                'name' => 'testName',
                'address' => $contact->address,
                'email' => $contact->email,
                'phone_mobile' => $contact->phone_mobile,
                'phone_work' => $contact->phone_work,
                'phone_home' => $contact->phone_home,
            ]);
    }

    public function testBasicGetEducation()
    {
        $user = Support\Create::regularUser();

        $education = factory(Education::class)->make();
        $education->id = $user->externalIds->first()->body;
        $education->save();

        Session::start();
        $this->actingAs($user)
            ->get('api/user/' . $user->key . '/profile/education')
            ->seeJson([
                $education,
            ]);
    }

    public function testBasicPutEducation()
    {
        $user = Support\Create::regularUser();

        $education = factory(Education::class)->make();
        $education->id = $user->externalIds->first()->body;
        $education->save();

        Session::start();
        $this->actingAs($user)
            ->put('api/user/' . $user->key . '/profile/education', [
                '_token' => csrf_token(),
                'degree' => 'dd',
                'institute' => $education->institute,
                'department' => $education->department,
                'start_date' => $education->start_date,
                'end_date' => $education->end_date,
                'concentration' => $education->concentration,
                'obtained' => $education->obtained,
            ])
            ->seeJson([
                'degree' => 'dd',
            ]);
    }

    public function testBasicDeleteEducation()
    {
        $user = Support\Create::regularUser();
        $education = factory(Education::class)->make();
        $education->id = $user->externalIds->first()->body;
        $education->save();

        Session::start();
        $this->actingAs($user)
            ->get('api/user/' . $user->key . '/profile/education')
            ->seeJson([
                $education,
            ])
            ->put('api/user/' . $user->key . '/profile/education/delete', [
                '_token' => csrf_token(),
                'degree' => $education->degree,
                'institute' => $education->institute,
                'start_date' => $education->start_date,
            ])
            ->see('OK');
    }

    public function testBasicGetExperience()
    {
        $user = Support\Create::regularUser();

        $experience = factory(Experience::class)->make();
        $experience->id = $user->externalIds->first()->body;
        $experience->save();

        Session::start();
        $this->actingAs($user)
            ->get('api/user/' . $user->key . '/profile/experience')
            ->seeJson([
                $experience->toArray(),
            ]);
    }

    public function testBasicPutExperience()
    {
        $user = Support\Create::regularUser();

        $experience = factory(Experience::class)->make();
        $experience->id = $user->externalIds->first()->body;
        $experience->save();
        $newExperience = factory(Experience::class)->make();
        Session::start();
        $this->actingAs($user)
            ->put('api/user/' . $user->key . '/profile/experience', [
                '_token' => csrf_token(),
                'organization' => $experience->organization,
                'department' => $experience->department,
                'position' => $experience->position,
                'start_date' => $newExperience->start_date,
                'end_date' => $newExperience->end_date,
                'description' => $experience->description,
            ])
            ->seeJson([
                'department' => $experience->department,
            ]);
    }

    public function testBasicDeleteExperience()
    {
        $user = Support\Create::regularUser();
        $experience = factory(Experience::class)->make();
        $experience->id = $user->externalIds->first()->body;
        $experience->save();

        Session::start();
        $this->actingAs($user)
            ->get('api/user/' . $user->key . '/profile/experience')
            ->seeJson([
                $experience->toArray(),
            ])
            ->put('api/user/' . $user->key . '/profile/experience/delete', [
                '_token' => csrf_token(),
                'organization' => $experience->organization,
                'start_date' => $experience->start_date,
            ])
            ->see('OK');
    }

    public function testBasicPhoneQuery()
    {
        $user = Support\Create::regularUserWithContact();

        Session::start();
        $this->actingAs($user)
            ->post('api/phone_query/', [
                '_token' => csrf_token(),
                'name' => $user->name,
            ])
            ->seeJson([
                'id' => $user->externalIds->first()->body,
                'name' => $user->name,
                'phone_mobile' => $user->contact->phone_mobile,
            ]);

        $response = $this->actingAs($user)
            ->call('GET', 'api/phone_query', ['name' => $user->name]);

        $actual = $response->content();
        $expect = [[
            'id' => $user->externalIds->first()->body,
            'name' => $user->name,
            'phone_mobile' => $user->contact->phone_mobile,
        ]];
        $this->assertEquals(json_encode($expect), $actual);
    }
}
