<?php

namespace Test;

use App\Entities\Text;
use App\Entities\TextCategoryName;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Session;

class PostTagTest extends TestCase
{
    use DatabaseTransactions;

    public function testIndex() {
        $user = Support\Create::regularUser();
        $textBody = Support\Create::testTextBody();
        $post = Text::makeAndSave($textBody, $user);

        $tagName1 = strtolower(str_random(10));
        $tagName2 = strtolower(str_random(10));
        $tag1 = TextCategoryName::makeAndSave($tagName1, 'tag');
        $tag2 = TextCategoryName::makeAndSave($tagName2, 'tag');

        $post->classes()->attach($tag1);
        $post->classes()->attach($tag2);

        $this->actingAs($user)
            ->get('api/post/' . $post->key . '/tag')
            ->seeJsonEquals([$tagName1, $tagName2]);
    }

    public function testUpdate() {
        $user = Support\Create::regularUser();
        $textBody = Support\Create::testTextBody();
        $post = Text::makeAndSave($textBody, $user);
        $tagInput = str_random(10);

        Session::start();
        $this->actingAs($user)
            ->put('api/post/' . $post->key . '/tag/' . $tagInput, [
                '_token' => csrf_token(),
            ])
            ->assertResponseOk();

        $tag = strtolower($tagInput);
        $found = !Text::OfCateg('tag', $tag)->where('key', $post->key)->get()->isEmpty();
        $this->assertTrue($found);
    }

    public function testDestroy() {
        $user = Support\Create::regularUser();
        $textBody = Support\Create::testTextBody();
        $post = Text::makeAndSave($textBody, $user);

        $tagInput1 = str_random(10);
        $tagName1 = strtolower($tagInput1);
        $tagName2 = strtolower(str_random(10));
        $tag1 = TextCategoryName::makeAndSave($tagName1, 'tag');
        $tag2 = TextCategoryName::makeAndSave($tagName2, 'tag');

        $post->classes()->attach($tag1);
        $post->classes()->attach($tag2);

        Session::start();
        $this->actingAs($user)
            ->delete('api/post/' . $post->key . '/tag/' . $tagInput1, [
                '_token' => csrf_token(),
            ])
            ->assertResponseOk();

        $notFoundOf1 = Text::OfCateg('tag', $tagName1)->where('id', (string) $post->id)->get()->isEmpty();
        $this->assertTrue($notFoundOf1);

        $foundOf2 = !Text::OfCateg('tag', $tagName2)->where('id', (string) $post->id)->get()->isEmpty();
        $this->assertTrue($foundOf2);
    }

    public function testAutocomplete() {
        $tag1 = 'aaa';
        $tag2 = 'aab';
        $tag3 = 'aba';
        $tag4 = 'baa';
        TextCategoryName::makeAndSave($tag1, 'tag');
        TextCategoryName::makeAndSave($tag2, 'tag');
        TextCategoryName::makeAndSave($tag3, 'tag');
        TextCategoryName::makeAndSave($tag4, 'tag');
        $user = Support\Create::regularUser();

        $response = $this->actingAs($user)
            ->call('GET', 'api/post/tagcomplete', ['prefix' => 'a']);

        $expect = [$tag1, $tag2, $tag3];
        $actual = json_decode($response->content());
        sort($expect);
        sort($actual);

        $this->assertEquals(json_encode($expect), json_encode($actual));
    }
}
