<div class="secure">Upload form:</div>
<form method="POST" action="<?php echo action('User\UserController@saveAvatar', Auth::user()->hash);?>" accept-charset="UTF-8" enctype="multipart/form-data">
    <?php echo csrf_field(); ?>
    <div class="control-group">
        <div class="controls">
            <input name="image" type="file">
        </div>
    </div>
    <input class="send-btn" type="submit" value="Submit">
</form>
