FROM php:5.6-apache

VOLUME /var/www/html/

RUN usermod -u 1000 www-data
RUN a2enmod rewrite
RUN apt-get update && apt-get install -y \
      libfreetype6-dev \
      libjpeg62-turbo-dev \
      libmcrypt-dev \
      libpng12-dev \
      libgmp-dev \
      libmhash-dev \
      gettext \
      re2c \
      file \
      openssl \
 && docker-php-ext-install iconv mcrypt \
 && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
 && docker-php-ext-install gd \
 && docker-php-ext-install gettext mbstring
