Term Project for SDM
====================


The project backend uses [PHP Composer](https://getcomposer.org/) and is built
upon [Laravel](http://laravel.com/).  The SAML library we use this time is
[php-saml](https://github.com/onelogin/php-saml).

- - -

To run and develop the project, several preparations need to be done:

 - Run `$ bash prepare.sh` and verify that everything is done without an error.

 - Make sure that the stuffs under `storage` and `database` in folder
   `sdm-project/laravel` are writable to Httpd.

 - Enable the URL rewrite module of Httpd.

 - Register the app as a SAML SP at
   [SDM devconsole](http://sdm.im.ntu.edu.tw/devconsole).  The settings should
   be:

   + Service Provider Metadata URL:
   `{webapp-root}/auth/saml2/metadata`
   + Assertion Consumer Service URL:
   `{webapp-root}/auth/saml2/acs`
   + Single Logout Service URL:
   `{webapp-root}/auth/saml2/sls`

After finishing all the above settings, the app should now be able to run.
Please set the webapp root to `sdm-project`.

- - -

If you use Docker (Recommended), you can simply do:

```shell
$ bash prepare.sh
$ docker build -t laravel-env .
$ docker run -d --name sdm-run -p 8888:80 --dns=8.8.8.8 -v $(pwd):/var/www/html laravel-env
```

then the webapp should be accessible at
[http://localhost:8888/sdm-project/](http://localhost:8888/sdm-project/).  Note
that for the app to run properly, the registration at SDM devconsole
(mentioned above) is still required.
