#!/usr/bin/python

'''
Based on `http://addamhardy.com/blog/2013/06/05/good-commit-messages-and-enforcing-them-with-git-hooks`
and `http://karma-runner.github.io/0.12/dev/git-commit-msg.html`
'''

import sys, os, re
from subprocess import call

try:
    input = raw_input
except NameError:
    pass

editor = os.environ.get('EDITOR') or 'vim'
commit_type = ['feat', 'fix', 'docs', 'style', 'refactor', 'test', 'chore']
cross_second_line = False
in_long_block = False

def check_format_rules(line_no, line):
    global cross_second_line, in_long_block
    current_line = line_no + 1

    if re.match('^#.*', line):
        return False # skip the whole comment line

    if current_line == 1:
        e = list()
        summary_form = re.match(('^(?:' + '|'.join(commit_type) +
                                 ')(?:\((.+)\))?: (.+)'), line)
        if len(line) > 50:
            e.append('- First line should be less than 50 characters'
                     ' in length.')
        if not summary_form:
            e.append('- First line should have the form'
                     ' "<type>(<scope>): <subject>".')
            e.append('    <type>: ' + ', '.join(commit_type))
            e.append('    (<scope>): (optional) the scope of the affection')
            e.append('    <subject>: the summary of the work in this commit')
            return (['Error 1: ' + e[0]] +
                    map(lambda x: '         ' + x, e[1:]))
        else:
            the_scope = summary_form.group(1)
            the_subject = summary_form.group(2)
            if the_scope and not re.match('^[a-z0-9\-]+$', the_scope):
                e.append('- <scope> should be a single lowercased word.')
            if the_subject and not re.match('^[A-Z]', the_subject):
                e.append('- <subject> should start with a verb, first'
                         ' letter uppercased.')
            if e:
                return (['Error 1: ' + e[0]] +
                        map(lambda x: '         ' + x, e[1:]))
        return False # a good first line

    if not cross_second_line:
        cross_second_line = True
        if not re.match('^\s*$', line):
            return ['Error %d: - Second line should be empty.' %
                    (current_line,)]
        else:
            return False # a good second line

    if not in_long_block:
        long_line = re.match('^(.*)\[\[\]\](.*)', line)
        if long_line and \
            (long_line.group(1) or not re.match('^ .+', long_line.group(2))):
            return ['Error %d: - Malformed long message line symbol "[[]]".' %
                    (current_line,)]
        else:
            has_block_starter = re.match('^(.*)\[\[(.*)', line)
            if has_block_starter:
                in_long_block = True
                if has_block_starter.group(1) or has_block_starter.group(2):
                    return ['Error %d: - Malformed long message block'
                            ' symbol "[[".' % (current_line,)]
            else:
                if len(line) > 72:
                    line_with_comment = re.match('^(.*)#.*', line)
                    if line_with_comment and \
                        len(line_with_comment.group(1)) <= 72:
                        return False # a good line with comment longer than 72
                    return ['Error %d: - No line should be over 72 characters'
                            ' long.' % (current_line,)]
        return False # a good normal line or long line or a good block starter

    else: # in long block
        block_closer = re.match('^(.*)\]\](.*)', line)
        if block_closer:
            in_long_block = False
            if block_closer.group(1) or block_closer.group(2):
                return ['Error %d: - Malformed long message block symbol'
                        ' "]]".' % (current_line,)]
        return False # a good line in long block or a good block closer

def clean_error_message(message_file):
    global cross_second_line, in_long_block
    cross_second_line = False
    in_long_block = False
    with open(message_file, 'r') as commit_fd:
        texts = commit_fd.readlines()
        context = filter(lambda t: not t.startswith('# >> '), texts)
    with open(message_file, 'w') as commit_fd:
        for i in context:
            commit_fd.write(i)

if __name__ == '__main__':
    if len(sys.argv) == 2:
        message_file = sys.argv[1]
    else:
        print('No message input.')
        sys.exit(1)

    while True:
        commit_msg = list()
        errors = list()
        with open(message_file) as commit_fd:
            for lineno, line in enumerate(commit_fd):
                stripped_line = line.strip()
                commit_msg.append(line)
                e = check_format_rules(lineno, stripped_line)
                if e:
                    errors.extend(e)
        if in_long_block:
            errors.append('Error: Long message block isn\'t closed.')
        if errors:
            with open(message_file, 'w') as commit_fd:
                commit_fd.write('%s\n' %
                                '# >> GIT COMMIT MESSAGE FORMAT ERRORS:')
                for error in errors:
                    commit_fd.write('# >>   %s\n' % (error,))
                for line in commit_msg:
                    commit_fd.write(line)
            re_edit = input('Invalid git commit message format.'
                            '  Enter y to edit or n to cancel the commit.'
                            ' [y/n] ')
            if re_edit.lower() in ('n','no'):
                clean_error_message(message_file)
                sys.exit(1)
            call('%s %s' % (editor, message_file), shell=True)
            clean_error_message(message_file)
            continue
        break
