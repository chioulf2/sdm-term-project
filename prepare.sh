#!/bin/bash

# prepare git commit message checker
cp dev/commit-msg-checker/* .git/hooks/
chmod 775 .git/hooks/commit-msg .git/hooks/commit-msg.py
cp .git/hooks/pre-commit.sample .git/hooks/pre-commit

# prepare laravel dependencies
cd sdm-project/laravel
php -r "readfile('https://getcomposer.org/installer');" | php
php composer.phar install
php composer.phar update

# prepare laravel app key
cp .env.example .env
php artisan key:generate

# prepare database
rm -f database/vendor.sqlite
rm -f database/shared.sqlite
touch database/vendor.sqlite
touch database/shared.sqlite
php artisan migrate

# run unit tests
./vendor/bin/phpunit
